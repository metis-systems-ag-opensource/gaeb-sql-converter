Wie mache ich das mit der Generierung von INSERT-Statements?

Wie sieht meine DOM-Struktur überhaupt aus?

Können Mehrdeutigkeiten irgendwie aufgelöst werden?

Irgendwie muss ich, parallel zur Generierung des DB-Schemas, Daten
generieren (in irgendeiner Form, mit deren Hilfe der GAEB-Parser dann
die INSERT-Statements korrekt aus den GAEB-Daten heraus erzeugen und
ausführen kann.

Des weiteren wird – sowohl für den GAEBsd-Parser als auch den GAEB-Parser –
eine Konfigurationsdatei benötigt (besser eine für beide), in der zentrale
Daten abgelegt werden, die nicht jedesmal wieder übergeben werden sollen,
so z.B. der Name der generierten Datenbank(en), der Basispfad für die GAEB-
Schemadateien, ...


Folgende Schritte...

0. Erweiterung des Konfigurationsdatei-Managements um Datenbankzugriffsdaten
   (Server, Portnummer, Authentifikationsdaten).

Dazu muss ich mir überlegen, welche Konfigurationsdaten notwendig/sinnvoll
sind.

1. Analyse der aus einer (typischen) GAEB-Datei generierten Strukturen.

Dazu muss ich einen weiteren GAEB-Parser generieren, der die gesammelten
Daten in einer strukturierten Form ausgibt.

Evtl. erfordert das eine Umwandlung in Layout-strukturiertes JSON, damit ich
einen Überblick darüber gewinne, wie die generierten Strukturen tatsächlich
aussehen.

2. Entwicklung einer Datenstruktur, mit der die Arten der einzelnen Attribute
   bzw. Tabellen ohne komplexe Syntax beschrieben werden können.

So könnte die zusätzliche Datenstruktur aussehen:
    table: tblname: attr1, attr2, ... <EOL>
    attrX: name:type
    name: attribute name	(same as database table's column name)
    type: attribute type (b|i|k|f|r|s|x<tblname>|x[tblname]|X<tblname>)
	b = bool
	D = a date value
	d = a date/time value
	i = integer
	k = key value (integer with an additional key property)
	f = fixed point decimal number (probably with a fractional part)
	r = floating point or number
	s = string
	T = a time value
	x<tblname> = sequence (within a choice or sequence)
	    'tblname' is the name of the table generated for this grouping
	    element.
	x[tblname] = sequence with multiple values (maxOccurrence > 1).
	    'tblname' is the name of the table generated for this grouping
	    element.
	y<tblname> = choice
	    'tblname' is the name of the table generated for this grouping
	    element.
	y[tblname] = choice with multiple values (maxOccurrence > 1).
	    'tblname' is the name of the table generated for this grouping
	    element.
	X<tblname> = a <complexType> element. 'tblname' is the name of the
	    table generated for this element.
	X[tblname] = a <complexType> element with multiple values
	    (maxOccurrence > 1). 'tblname' is the name of the table genrated
	    for this element.

Beispiel:
    tbl1: gaebid:k, id:k, ix:k, xyz_index:x<tbl2>, val1:s, val2:r
    tbl2: gaebid:k, id:k, val1:f, val2:b, val3:f
    ...

Was der GAEB-Parser dann daraus macht, sollte dann durch diese Struktur
bestimmt werden.

Dieser Vorgang muss/sollte parallel zur Datenbank-Generierung erfolgen. Evtl.
muss letztere dafür angepasst werden (verzögerte Ausgabe?).

3. Entwicklung eines Algorithmus zur Generierung von INSERT-Statements aus
   tatsächlichen Daten (DOM-Baum) und der vom GAEBsd-Parser generierten
   Datenstruktur.

Das ist in der Tat der schwierigste Teil, und der, dessen Lösung mir bisher
am schwersten fällt.

2021-07-29:

Ich habe den Datentyp 'ElSpec' (hieß ursprünglich 'TblElement') um ein Feld
erweitert: 'seqx'. Dieses Feld ist ein Zähler, der mir die Möglichkeit gibt,
folgende Konfiguration zu identifizieren:

  <element name="xxx">
    <sequence maxOccurs="<wert größer 1>">
      <element name="abc" minOccurs="0" />
      <element name="def" />
      ...
      <element name="xyz" />
    </sequence>
  </element>

Bei dieser Konfiguration kann nämlich die Situation entstehen, dass
Anfang und Ende einer Sequenz wegen der optionalen Elemente nicht richtig
erkannt werden (z.B. in:

  <xxx>
    <def>Erster Wert</def>
    ...
    <xyz>Letzter Wert</xyz>
    <abc>Erster Wert 2</abc>
    ...
  </xxx>

In einer einfachen Existenz-Zuordnung würde das Element '<abc>' an der falschen
Stelle zugeordnet werden, nämlich der ersten Sequenz. Um solche Fälle zu
vermeiden, muss anhand einer Prüfung des Sequenz-Zählers ('seqx') ermittelt
werden, an welcher Position der aktuellen Sequenz das letzte zugeordnete
Element steht (im Beispiel: 'xyz'). Ist dessen Sequenz-Zähler nämlich größer
(oder gleich) als der des neu einzutragenden Elementes (im Beispiel 'abc'),
dann ist klar, dass es sich hierbei um den Beginn einer neuen Sequenz handelt.
In diesem Fall müsste also ein neues INSERT-Statement generiert werden.

Wie ist das mit dem Gruppierungselement <choice>?
Eine <choice>-Gruppe unterscheidet sich von einer '<sequence>' dadurch, dass
höchstens eines der Elemente, die in einer '<choice>'-Gruppe vorkommen,
tatsächlich erscheinen darf. Beispiel:

  <element name="chdemo">
    <choice>	<!-- 'minOccurs' und 'maxOccurs' sind beide '1'.
      <element name="Wahl_1" />
      <element name="Wahl_2" />
      ...
      <element name="Wahl_N" />
    </choice>
  </element>

In diesem Fall ist die Situation gegeben, dass erkannt werden muss, dass ein
beliebiges Element der <choice> bereits eingetragen wurde. Aus diesem Grund
müssen '<sequence>' und '<choice>' getrennt behandelt werden. Deshalb habe ich
den zusätzlichen "type" 'y' eingeführt.

Auch muss in beiden genannten Fällen für Gruppierungen der Bezug auf die
zugrundeliegende Tabelle hergestellt werden. Das bedeutet, dass

  - mein 'set<string> inserted;' in ein 'map<string, unsigned>' umgewandelt
    werden muss,
  - das mein Schlüsselelement für 'inserted' nicht der Name des Elementes,
    sondern der der zugrundeliegenden Tabelle sein muss.

Ich habe den Parser für die Insertion-Metadaten bereits erweitert, ebenso den
GAEB-Schemaparsr. Letzterer generiert als Typ ein 'y' für '<choice>'-Gruppen,
während ersterer die Ordnungszahl der Elemente in einer Gruppe in die gnerierte
Datenstruktur einfügt.
