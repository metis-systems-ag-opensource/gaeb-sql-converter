# Untersuchungen zum Thema *XML-Parsierung mit Validierung*

(Sorry ... this text is in german)

## Probleme bei der Verwendung eines beliebigen *validierenden* XML-Parsers

Leider sind bei meinen Untersuchungen zu dem Thema *XML-Parsierung mit
Validierung* einige Probleme aufgetreten.

### Problem Nr. 1:

Die GAEB-Dateien enthalten außer einem *Namespace-URI* im *Top-Level-Element*
keine weiteren Angaben zu irgendwelchen Schema-Definitionen (XSDs):

    <GAEB xmlns="http://www.gaeb.de/GAEB_DA_XML/DA81/3.2">
      ...
    </GAEB>

Leider führt dieser URI nirgendwo hin (hab's ausprobiert ... mehrfach), so
dass auch *Xerces-C++* nicht in der Lage sein dürfte, GAEB-Dateien zu
validieren – jedenfalls nicht ohne Hilfsmittel.

### Problem Nr. 2:

*Xerces-C++* verfügt sehr wohl über die Möglichkeit, Schema-Definitionen vorher
zu laden (vor dem Parsieren). Dazu muss aber bekannt sein, um welche XSDs es
sich handelt, bzw. welche benötigt werden.

Leider sind die Entwickler des GAEB-Standards offensichtlich zu *hirnverbrannt*
um zu Begreifen, dann man eine Parsierung mit Validierung erst dann machen
kann wenn man die passende Schema-Datei zunächst lädt. Leider befinden sich die
hierzu benötigten Informationen im GAEB-Dokument, und zwar so verklausuliert,
dass eine Entnahme erst durch ein Parsieren möglich wird.

### Problem Nr. 3:

Zwar verfügt Xerces-C++ über die Möglichkeit, XSDs vor dem Parsieren zu laden;
es ist jedoch nicht klar, was geschieht, wenn diese XSDs verweise auf weitere
XSDs enthalten (wie z.B. Verweise auf XSDs zu Grundsätzlichen Datentypen des
W3C).


## Wie gehe ich diese Probleme an?

### Lösung Nr. 1:

Die XSD-Dateien müssen irgendwo lokal abgelegt werden. Das kann z.B. auf einem
internen WEB-Server der Firma erfolgen, auf den der GAEB-Parser Zugriff hat.

Des weiteren muss eine Übersetzung des `xmlns`-*URI*s, z.B. mittels einer
einfachen Tabelle (in einer Datei enthalten) definiert werden, die es dem
GAEB-Parser ermöglicht, den URI der jeweils passenden Schema-Datei zu
bestimmen.

Außerdem wäre es u.U. nicht verkehrt, die einmal geladenen Schema-Dateien
lokal zwischenzuspeichern (zu *cachen*), damit zukünftige Parsierungsvorgänge
schneller stattfinden können.

### Lösung Nr. 2:

Dass die Informationen, welche XSDs benötigt werden, erst aus dem Dokument
entnommen werden müssen (es gibt keinerlei *META-Informationen* im Header, die
ein XML-Parser vorher auslesen könnte), ist es notwendig eine Parsierung in
zwei Parser-Läufen durchzuführen, und zwar mit

  - einem Parser-Lauf eines SAX-Parsers, um den Inhalt der Elemente

      <Version> ... </Version>

    und

      <DP> ... </DP>

    zu extrahieren, und

  - einem zweiten Parser-Lauf (eined DOM-Parsers), nachdem mit den aus dem
    ersten Lauf bestimmten Daten die passende Schema-Definition(en) geladen
    wurdei(n). Dieser Parser-Lauf kann dann "validierend" erfolgen.

Eine andere Möglichkeit bietet sich hier nicht, da eine Validierung im
Anschluss an eine vollständige *DOM-Parsierung* (also ein Zwei-Pass-Verfahren)
scheinbar nicht unterstützt wird.

#### Warum ein SAX-Parser-Lauf?

Die Frage ist recht einfach zu beantworten. Ein SAX-Parser ist ein Ereignis-
basierter Parser, d.h. er erlaubt es, bestimmte Routinen aufzurufen, sobald
(z.B.) das Start-Tag eines Elementes gefunden wurde, oder aber, sobald ein
Element vollständig eingelesen wurde. Er ist daher

  1. schneller, und
  2. platzsparender

als ein DOM-Parser, der erst einmal eine (unter Umständen sehr große) XML-Datei
erst als DOM-Struktur im Speicher ablegen müsste, nur um zwei Elemente zu
lesen, nämlich

  1. `<Version>`*Versionsnummer*`</Version>` im *GAEBInfo*-Element,
  2. `<DP>`*GAEB-Dateityp*`</DP>` im *Award*-Element.

#### Warum danach der DOM-Parser?

Die vollständigen Daten aus der GAEB-Datei sollen in eine Datenbank
(genauergesagt: eine *relationale `SQL`-Datenbank*) übertragen werden. Dazu ist
es tatsächlich besser, die gesamte GAEB-Datei als DOM-Struktur im Speicher
abzulegen, und aus diesen Daten dann die benötigten `INSERT`-Statements zu
generieren.

### Lösung Nr. 3:

Zu diesem Zweck muss einfach eine GAEB-Schema-Definition geladen werden, um
zu sehen, ob der XML-Parser (*Xerces-C++* in diesem Fall), die externen
Schema-Definitionen (des W3C) herunterlädt.

## Aus welchen Programmteilen muss denn der GAEB-Parser nun bestehen?

1. Die zwei XML-Parser (SAX zur Bestimmung der XSDs, DOM zur *Parsierung mit
   Validierung*).
2. Ein Modul, dass die Übersetzung der META-Daten (GAEB-Typ, Versionsnummer,
   usw.) in die URIs (bzw. Pfadnamen) der benötigten XSDs übernimmt.
3. Ein Modul, das das Zwischenspeichern der geladenen XSDs durchführt.
4. Ein Modul, das die Daten aus der DOM-Struktur der GAEB-Datei in eine
   Datenbank übernimmt.


## Erste Erkenntnisse

Der Versuch, eine GAEB-Datei vom Typ 'X83' mit Validierung zu parsieren,
führte zu einer riesigen Liste an Fehlermeldungen bzgl. nicht deklarierter
Elemente (allen voran das Toplevel-Element `<GAEB>`).

Nach vielem herumprobieren erkannte ich, dass ich das Ganze mit einfacheren
XSD- und XML-Dateien ausprobieren musste. Nachdem ich auf der Wikipedia-Seite
[XML Schema](https://de.wikipedia.org/wiki/XML_Schema) schließlich einige
einfache Beispiele gefunden und ausprobiert hatte, kam ich zu folgender
Erkenntnis:

Offensichtlich muss der Wert des Attributes `xmlns` im Toplevel-Element der
XML-Datei genau mit dem Wert des Attributes `targetNamespace` im Element
`<xs:schema>` der XSD-Datei (der Schema-Datei) übereinstimmen, damit die
Elemente des XML-Dokumentes korrekt erkannt werden.

Nun begreife ich auch, wozu die Attribute `xmlns` im Toplevel-Element der
XML-Datei und `targetNamespace` im `<xs:schema>`-Element der XSD-Datei da
sind. Auf diese Art werden die entsprechenden Elemente der XML-Datei wirklich
in einen eigenen Namensraum verlegt, der zu dem Namensraum der XSD-Datei genau
passen muss.
