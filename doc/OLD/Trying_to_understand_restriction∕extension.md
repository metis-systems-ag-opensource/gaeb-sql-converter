# Schema Component Constraint: Derivation Valid (Restriction, Complex)

If the derivation method is _restriction_, then **all** of the following
conditions must match:

1. The base type definition must be a complex type definition whose final
   does not contain _restriction_.
2. For each attribute use (call this **R**) in the attribute uses, the
   appropriate **case** among the following must be true:
 1. If there is an attribute use in the attribute uses of the base type
 definition (call this **B**) whose attribute declaration has the same
 name and target namespace, then **all** of the following must be true:
  1. **one** of the following must be true:
   1. **B**'s required is false
   2. **R**'s required is true
  2. **R**'s attribute declaration's type definition must be validly
  derived from **B**'s type definition
  3. 
