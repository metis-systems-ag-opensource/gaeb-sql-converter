# Plans ffor future releases of the _GAEB Schema Parser_ and the _GAEB Reader_



## No dissection/disassembly of certain elements

Because it doesn't make much sense to dissect elements which contain
(formatted) text, i plan to define such elements as a whole, meaning:

 - No ‘Schema => SQL/Insert Meta’ translation is done for such elements in the
   `GAEB Schema Parser`,
 - The content of such elements is re-assembled in the `GAEB Reader`.

Because it is not known (yet, and may remain as such) which elements are to be
kept undisassembled (are reassembled), a further (optional) configuration
item will be defined:
```
    NODISSECT = element-name ...
```
where `element-name` is a list of (either comma- or blank-separated list of
elements).

In the `GAEB Schema Parser` (`GAEBsd`) this means: I need to pass this list
through the resolver (or a pass before it). There, any element which is member
of such a list must be reconstructed with a simple text content attribute (of
arbitrary length) instead of the sub-structure.

In the `GAEB Reader` this means: The content of each element found in this list
must be reassembled into a text which will then be the content of this element
and so the `_content` column of the corresponding database entry.


### First Step: Configuration

I need a new (optional) configuration item `NODISSECT`. This element – if
existing – must contain a list of either comma or blank separated element
names.


### Second Step: GAEBsd

Either in the processing of the elements in the parsing pass (in
`sp-action.{cc,h}`), or in a following pass, the elements which are named
in `NODISSECT` must be reduced to simple type elements with their `content`
removed. A later step has the advantage that the configuration object, or
rather the `NODISSECT` configuration item can be stored in the local object
used for holding the working data used by the different routines of this
pass. This it not so easy in the routines of 'sp-action.{cc,h}'.

Additionally to the removal of the `content` of `NODISSECT` elements, these
elements should get a special marker (`bool nodissect`). This marker then
must be added to the output of the INSERT meta data (only there!).


### Third Step: GAEB Reader

The new flag `nodissect` must be added to the structures generated by
`readmeta()`. In the module `process.{cc,h}`, each time an element with the
flag `nodissect` set is found, this element must processed by a special (new)
function `reassemble()` which reassembles the content into a textual value,
which then becomes the `_content` value of the corresponding element.



## Completing the _GAEB Schema Parser_ and the _GAEB Reader_

Currently, the _GAEB Schema Parser_ doesn't support `<import>` elements and
more than the two namespaces for the _XML Schema_ and the (main) _GAEB Schema_
itself (the namespace with no prefix). These omissions make it impossible to
handle the '84P' phase (a form of a _fixed-term contract_.

This means that parsing and using additional _GAEB Schema_ definitions must be
added. The XML-Parser in the module 'schemaparser.{cc,h}' already has the
ability to parse more than one schema, because most of the _GAEB Schema_-files
are redefinitions of a central _Library Schema definition_.

What is required is:

 - the ability to store more than the two standard namespaces,
 - the Parsing of the `<import>` element(s),
 - the ability to store the parser output from the `<import>` element(s) and
   associating it with the corresponding namespace identifiers,
 - applying the following passes (save from the code generation) to the parser
   output of the `<import>` elements,
 - merging the structures of the `<import>` elements into the structures of the
   parsed _main schema_.
