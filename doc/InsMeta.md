# Beschreibung des InsMeta-Formates

Dies ist eine kurze Beschreibung des Formates der Daten, die der GAEB-Reader
benutzt, um INSERT-Statements aus der DOM-Struktur einer GAEB-Datei zu
generieren. Zuerst eine kurze EBNF-Notation der *.is*-Dateien:

## Syntax

```
InsMeta-File : Header-Line EOL InsMeta-Description { InsMeta-Description } .

Header-Line : "DATABASE" Database-Name .

DataBase-Name : IDENTIFIER .

InsMeta-Description : Header ":" Body EOL .

Header : Table-Name "," Element-Name .

Body : Column-Description { "," Column-Description } .

Column-Description : Column-Name ":" Column-Type .

Column-Name : IDENTIFIER .

Column-Type : Key-Type | Simple-Type | Complex-Type | Choice | Sequence .

Key-Type : "k" .

Simple-Type : "b" | "D" | "d" | "i" | "f" | "r" | "s" | "T" .

Complex-Type : "X" Reference-Specification .

Reference-Specification : One-Element-Reference-Speciffication
                        | Multi-Element-Reference-Specification .

One-Element-Reference-Specification : "<" Table-Reference ">" .

Table-Reference : IDENTIFIER .

Multi-Element-Reference-Specification : "[" Table-Reference "]" .

Choice : "y" Reference-Specification .

Sequence : "x" Reference-Specification .

EOL : <A UNIX or WINDOWS end of line specifier> .

IDENTIFIER : <A LETTER/DIGIT/UNDERSCORE sequence starting with a LETTER> .
```

**Anm.:** Leerzeilen und -zeichen zwischen einzelnen syntaktischen Einheiten
  sind (z.Zt.) nicht zugelassen, um den Parser für diese *InsMeta*-Spezifikationen
  so einfach wie möglich zu halten. Auch generiert der GAEB-Schemaparser
  (`gaebsd`) die betreffenden Spezifikationen ohne Leerzeilen und -zeichen.
  Das Zeilenende selbst ist ein syntaktisches Element. Es beebdet jeweils die
  Kopfzeile bzw. eine `InsMeta*-Beschreibung.

## Datentypen:

 `b` - BOOLEAN (Werte können `TRUE` oder `FALSE` sein)
 `D` - DATE (Ein Datumswert, d.h. z.B. `1980-01-02`)
 `d` - DATETIME (Ein Datums-Zeitwert, d.h. z.B. `2021-09-24 18:04:57`)
 `i` - Ein ganzzahliger Wert (z.B. `123`, `-13`, `17773485`, ...)
 `f` - Eine Festkommazahl (z.B. `3.14`, `-2.71828`, ...)
 `r` - Eine Gleitkommazahl (z.B. `3.14`, `1.5E-3`, `7.321E15`, ...)
 `T` - Ein Zeitwert (z.B. `03:40:17`)

Der Unterschied zwischen Festkomma- und Gleitkommazahlen ist nicht sofort
ersichtlich, Er wird jedoch klar, sobald mit monetären Werten gearbeitet
wird. Festkommazahlen haben üblicherweise eine feste Anzahl an
Nachkommastellen, werden in einer *MySQL*-Datenbank als
`DECIMAL(width,fraction-digits)` dafgestellt, und eine Notation wie `1.5E-3`
gibt es nicht. Der Unterschied wird vor allem darum gemacht, weil mit
Festkommazahlen exakte Berechnungen durchgeführt werden können, da mit
ihnen wie mit ganzen Zahlen gerechnet werden kann, während bei
Gleitkommazahlen üblicherweise selbst die Zahl `10.0` (Zehn) nicht mehr
exakt dargestellt werden kann. Dafür sind Festkommazahlen üblicherweise
im Wertebereich stark beschränkt, während Gleitkommazahlen größere
Wertebereiche umfassen können. Zusammenfassend kann man sagen:

- Festkommazahlen: Exakte Zahlendarstellung (innerhalb der vorgegebenen
    Genauigkeit, eingeschränkter Zahlenbereich.

- Gleitkommazahlen: Großer Zahlenbereich, eingeschränkte Genauigkeit.

## Nähere Beschreibung

Die *InsMeta*-Datei besteht aus einer Kopfzeile, die die Zieldatenbank
spezifiziert, und einer Anzahl an Beschreibungen, die die Zuordnungen
von Elementen zu Datenbank-Tabellen und -Spalten beschreiben. Hierbei
gibe es spezielle Namenskonvention:

### Schlüsselattribute

Die Namen ``gaebid``, ``id`` und ``ix`` sind grundsätzlich Schlüssel-Attribute.
Sie kommen üblicherweise in der GAEB-Datei nicht vor, sondern werden beim
Generieren der INSERT-Statements automatisch generiert (bzw. verwendet).
Hierbei ist

- ``gaebid`` die Datenbank-Identifikation der gesamten GAEB-Datei, d.h. alle
  Werte, die sich auf eine spezifische GAEB-Datei innerhalb der Datenbank
  beziehen, haben die selbe `gaebid`. Sie wird genau einmal, mit dem INSERT-
  Statement für die Meta-Daten der GAEB-Datei (Name, Datum, ... – eben das
  meißte, was im ``<GAEBInfo>``-Element steht) generiert, und in jedem weiteren
  INSERT-Statement nur noch verwendet,

- ``id`` ist die Identifikation eines Elementes innerhalbn der Datenbank. Sie
  wird für jedes Element von einem komplexen Typ, sowie für jede *Choice*
  oder *Sequence* einmal generiert. Ausnahme: Wiederholungen (Arrays).

- ``ix``. Manchmal werden Elemente auf der selben Ebene innerhalb der
  GAEB-Datei wiederholt. Da diese jedoch nur einmal innerhalb des DB-Eintrages
  des umgebenden Elementes referenziert werden können, werden diese zusätzlich
  zu ``gaebid`` und ``id`` auch noch mit diesem Schlüssel-Attribut versehen.
  Mit ``ix`` werden die mehrfach vorkommenden Elemente in genau der Reihenfolge,
  in der sie innerhalb der GAEB-Datei auftraten, nummeriert, beginnend mit
  ``1``.

### Namenskonventionen

Üblicherweise haben die Namen der Attribute in den jeweiligen
Spaltenneschreibungen die Namen der entsprechenden Elemente innerhalb der
GAEB-Datei. Es gibt jedoch folgende Ausnahmen:

- Namen, die mit einem ``a_`` beginnen sind Attribut-Werte, d.h. sie kommen
  in einem Element nur im Start-Tag vor, und haben immer Werte von einfachen
  Datentypen.

- Namen, die mit einem ``_index`` enden, sind Referenzattribute für Elemente
  von komplexen Datentypen, sowie *Choice*- und *Sequence*-Tabellen, bzw. die
  Tabellen, die diesen zugeordnet sind. Der Name innerhalb der spitzen bzw.
  eckigen Klammern bezieht sich dabei auf eine Tabelle.

- Die Namen von *Choice*- und *Sequence*-Tabellen lauten üblicherweise
  ``choice_``*Nummer* bzw. ``sequence_``*Nummer*. Die Nummern werden hierbei
  durch den GAEB-Schemaparser (``gaebsd``) sequentiell vergeben.

- Die Namen von Elementen vom komplexen Typ können mehrfach definiert sein, und
  zwar in unterschiedlichen Kontexten. Ein Beispiel ist das Text-Element
  ``<p>``, das z.B. in der GAEB-Phase ``81`` mit jeweils unterschiedlichen
  Definitionen existiert, einmal mit einem Inhalt, der nur aus ``<span>``- und
  ``<br/>``-Elementen besteht, an anderer Stelle jedoch mit zusätzlichen
  Verweisen auf Bilder (``<Image>``-Elemente), sowie Elementen vom Typ
  ``<TextComplement>``. (Es gibt insgesamt drei solcher Definitionen innerhalb
  des XML-Schemas für die GAEB-Phase ``81``.) Um diese Tatsache gerecht zu
  werden, werden für solche Elemente, die ja unterschiedlich definiert sind,
  Tabellen eingeführt, die als Namen *Element-Name*``_``*Nummer* haben. So wird
  durch den GAEB-Schemaparser (*gaebsd*) für die erste gefundene Definition
  von ``<p>`` der Tabellenname `p` vergeben, für die nächste ``p_1``, danach
  ``p_2`` usw. Die Referenz-Attribute (mit ``_index`` endend) sind von diesen
  geänderten Tabellennamen nicht betroffen, der Namen innerhalb einer
  ``Reference-Specification`` jedoch schon.

## Auszüge aus einer InsMeta-Datei (Beispiel)

```
DATABASE GAEB_X81
GAEBInfo,GAEBInfo:gaebid:k,id:k,Version:s,VersDate:DATE,Date:D,Time:T,ProgSystem:s,ProgName:s,Certific:s
span,span:gaebid:k,id:k,a_class:s,a_style:s,_content:s
image,image:gaebid:k,id:k,a_Encoding:s,a_Name:s,a_Type:s,a_align:s,a_border:i,a_hspace:s,a_vspace:s,a_width:s,_content:s
choice_96,choice_96:gaebid:k,id:k,ix:k,span_index:X<span>,br:s,image_index:X<image>
p,p:gaebid:k,id:k,choice_96_index:y[choice_96],a_class:s,a_style:s
choice_99,choice_99:gaebid:k,id:k,ix:k,span_index:X<span>,br:s
div,div:gaebid:k,id:k,choice_99_index:y[choice_99],a_class:s,a_style:s
ol,ol:gaebid:k,id:k,ix:k,li_index:X[li],a_class:s,a_start:s,a_style:s
choice_105,choice_105:gaebid:k,id:k,ix:k,p_index:X<p>,div_index:X<div>,span_index:X<span>,br:s,ul_index:X[ul],ol_index:X[ol]
li,li:gaebid:k,id:k,ix:k,choice_105_index:y[choice_105],a_class:s,a_style:s
ul,ul:gaebid:k,id:k,ix:k,li_index:X[li],a_class:s,a_style:s
choice_111,choice_111:gaebid:k,id:k,ix:k,p_index:X<p>,div_index:X<div>,span_index:X<span>,br:s,table_index:X[table],ul_index:X<ul>,ol_index:X<ol>
th,th:gaebid:k,id:k,ix:k,choice_111_index:y[choice_111],a_align:s,a_class:s,a_style:s,a_valign:s
choice_112,choice_112:gaebid:k,id:k,ix:k,p_index:X<p>,div_index:X<div>,span_index:X<span>,br:s,table_index:X[table],ol_index:X<ol>,ul_index:X<ul>
td,td:gaebid:k,id:k,ix:k,choice_112_index:y[choice_112],a_align:s,a_class:s,a_colspan:i,a_rowspan:i,a_style:s,a_valign:s
sequence_109,sequence_109:gaebid:k,id:k,ix:k,th_index:X[th],td_index:X[td]
tr,tr:gaebid:k,id:k,ix:k,sequence_109_index:x[sequence_109],a_align:s,a_class:s,a_style:s,a_valign:s
table,table:gaebid:k,id:k,ix:k,tr_index:X[tr],a_align:s,a_border:i,a_cellpadding:s,a_class:s,a_frame:s,a_height:s,a_org:s,a_style:s,a_width:s
Image,Image:gaebid:k,id:k,a_Encoding:s,a_Name:s,a_Type:s,a_align:s,a_border:i,a_hspace:s,a_vspace:s,a_width:s,_content:s
choice_94,choice_94:gaebid:k,id:k,ix:k,p_index:X<p>,div_index:X<div>,span_index:X<span>,ul_index:X[ul],ol_index:X[ol],table_index:X[table],br:s,page:s,Image_index:X<Image>
Descrip,Descrip:gaebid:k,id:k,choice_94_index:y[choice_94],a_style:s,a_textWidth:s
choice_98,choice_98:gaebid:k,id:k,ix:k,span_index:X<span>,br:s
p_1,p:gaebid:k,id:k,choice_98_index:y[choice_98],a_class:s,a_style:s
choice_95,choice_95:gaebid:k,id:k,ix:k,p_index:X<p_1>,div_index:X<div>,span_index:X<span>,br:s
OutlineAddText,OutlineAddText:gaebid:k,id:k,choice_95_index:y[choice_95],a_style:s,a_textWidth:s
DetailAddText,DetailAddText:gaebid:k,id:k,choice_94_index:y[choice_94],a_style:s,a_textWidth:s
AddText,AddText:gaebid:k,id:k,ix:k,OutlineAddText_index:X<OutlineAddText>,DetailAddText_index:X<DetailAddText>
PrjInfo,PrjInfo:gaebid:k,id:k,NamePrj:s,LblPrj:s,Descrip_index:X<Descrip>,Cur:s,CurLbl:s,BidCommPerm:s,AlterBidPerm:s,AddText_index:X[AddText],UPFracDig:i
COReas,COReas:gaebid:k,id:k,choice_94_index:y[choice_94],a_style:s,a_textWidth:s
COInfo,COInfo:gaebid:k,id:k,ix:k,CONo:i,COPhase:s,COStatus:s,COInit:s,COReas_index:X<COReas>,RefBoQCOInfo:s,CODate:D
MaintInfo,MaintInfo:gaebid:k,id:k,MaintType:s,ProcessType:s,ContrTransDate:D,ContrTransTime:T,ContrLaw:s,Deadline:s
MastAgrLbl,MastAgrLbl:gaebid:k,id:k,choice_95_index:y[choice_95],a_style:s,a_textWidth:s
choice_13,choice_13:gaebid:k,id:k,MastAgrDur:i,MastAgrEnd:D
ContrValCode,ContrValCode:gaebid:k,id:k,Percent:f,Number:i
CtlgAssign,CtlgAssign:gaebid:k,id:k,ix:k,CtlgID:s,CtlgCode:s
choice_19,choice_19:gaebid:k,id:k,ILN:s,CtlgAssign_index:X[CtlgAssign]
Bank,Bank:gaebid:k,id:k,ix:k,LblBank:s,BRNo:s,AcctNo:s,BIC:s,IBAN:s
```
...

```
BoQBody,BoQBody:gaebid:k,id:k,choice_34_index:y<choice_34>
RefLotNo,RefLotNo:gaebid:k,id:k,ix:k,a_IDRef:s
LotGrp,LotGrp:gaebid:k,id:k,ix:k,LotGrNo:s,RefLotNo_index:X[RefLotNo],Totals_index:X<Totals>,a_ID:s
BoQ,BoQ:gaebid:k,id:k,BoQInfo_index:X<BoQInfo>,BoQBody_index:X<BoQBody>,LotGrp_index:X[LotGrp],a_ID:s
RefBoQ,RefBoQ:gaebid:k,id:k,a_IDRef:s
RefLotGrNo,RefLotGrNo:gaebid:k,id:k,a_IDRef:s
choice_88,choice_88:gaebid:k,id:k,ix:k,RefBoQ_index:X<RefBoQ>,RefLotNo_index:X<RefLotNo>,RefLotGrNo_index:X<RefLotGrNo>,RefItem_index:X<RefItem>
sequence_87,sequence_87:gaebid:k,id:k,ix:k,choice_88_index:y[choice_88],WgChangeRate:f
WgChange,WgChange:gaebid:k,id:k,LblRefWage:s,RedPriceComp:f,sequence_87_index:x[sequence_87]
Award,Award:gaebid:k,id:k,DP:s,AwardInfo_index:X<AwardInfo>,IndivAgrInfo_index:X<IndivAgrInfo>,OWN_index:X<OWN>,Requester_index:X[Requester],CTR_index:X<CTR>,sequence_5_index:x[sequence_5],AddText_index:X[AddText],BoQ_index:X<BoQ>,WgChange_index:X<WgChange>
GAEB,GAEB:gaebid:k,id:k,GAEBInfo_index:X<GAEBInfo>,PrjInfo_index:X<PrjInfo>,Award_index:X<Award>,AddText_index:X[AddText]
```
