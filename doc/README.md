# Hinweise

## 1. Meine eigenen Texte

Dieses Verzeichnis enthält keine vollständige Dokumentation, sondern eher
Gedanken, die ich mir im Verlauf der ENtwicklung gemacht habe. Dementsprechend
sind diese Dokumenta auch nicht sauber formuliert. Ich habe sie trotzdem am
Ende mit ins GIT-Repository übertragen, weil sie später vielleicht Hinweise zur
Weiterentwicklung liefern könnten … auch wenn viele Überlegungen direkt in
meinem Kopf stattgefunden haben, und daher keine z.Zt. Schriftform haben.

## 2. Die BOOST-Lizenz

Ich verwende zumindest ein Modul, das ich nicht selbst entwickelt habe:
`pstream.h`. Dies ist eine _C++_-artige Umsetzung von `popen()` und `pclose()`
(PIPE-Handling) in _C_. Dieses Modul steht unter der Boost-Lizenz, und aus
diesem Grund ist diese Lizenz in diesem Verzeichnis enthalten.

## 3. GAEB-docs

Dieses Vrezeichnis enthält die Fachdokumentation zu den einzelnen
GAEB-Versionen als PDFs. Die Version *3.2\_2012-01* fällt hier etwas aus dem
Rahmen, da sich das betreffende PDF auf die Version *3.2\_2013-10* bezieht.
Man kann allerdings davon ausgehen, dass letztere eine neuere Version von
*GAEB DA XML 3.2* darstellt, so dass die Fachdokumentation genauso auf die
ältere Version *3.2\_2012-01* angewandt werden kann.

## 4. Online-Konverter

Es gibt einen Online-Konverter, mit dem veraltete GAEB-Dateien, also solche,
die nicht mindestens in der GAEB-Version *3.2\_2012-01* vorliegen, in eines
der neueren Formate verwandelt werden können. Dieser ist zu finden unter:

[WebGAEB - Der einfache Konverter](https://www.web-gaeb.de/)
