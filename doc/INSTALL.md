# Small installation manual


## Pre-requisites (external dependencies)

0. A C++ compiler (preferrably `g++`).

1. `Xerces-C`. I don't know qthe exact names of the required packeges in
   RedHat etc., but in Debian the package to be installed is
   `libxerces-c-dev`.

2. `mariadb`/`mysql` development packages: `libmariadb-dev` (and probably
   `libmariadb-dev-compat`).

3. `pkg-config`. This package is required for the exact detection of the
   header files and libraries of *MariaDB* and *Xerces-C*.

4. `unzip`, `wget` and/or `curl` are required for downloading and unpacking
   the ZIP archives which contain the *GAEB XML Schema* files (which are
   required by most of the programs generated from this source tree).

All in all, prior to an installation, the following commands must be executed
(with `root` (admin) access):
```
# apt install libxerces-c-dev libmariadb-dev pkg-config build-essential \
              unzip wget curl
```
and – probably –
```
# apt install libmariadb-dev-compat
```

If the corresponding packages are already installed, these steps can be
skipped.


## Installation

Most of the installation of this software package is done automatically, by
either invoking
```
# make
# make install
```
or by invoking
```
$ make
$ make local_install
```

*in the TOP-level directory of the source tree*.

The first invocation installs the program package (the program files plus a
"default" conficuration) in a position which can be specified by adding
`PREFIX=/some/install/base_path` (the default is: `PREFIX=/usr/local`).

The second invocation installs the program package below the user's *HOME*
dirtectory.

The `make install` invocation normally requires super user access, so it
it should be invoked as:
 - `sudo make install`.

There are two exceptions from this rule:
  1. In a *Debian* package building context, the `make`-variable *`DESTDIR`*
    is set to a directory where a normal user-mode installation can take
    place.
  2. If *`PREFIX`* is set to a directory where the user has write access (e.g.
    in `make PREFIX=~/local-apps install`).

Both `make` invocations also download the required *GAEB XML Schema* files
from the [GAEB main website][gaeb] (more exactly: from the same links as
found on [GAEB Datenaustausch][gaebda]) and install them in the same location
as the program package (in a subdirectory *`gaeb-xsd`*).

The installation uses the shell script `butils/install.sh`. For this script
bein able to work a newer *BASH* shell is **mandatory**.

`butils/install.sh` installs the complete program package in a directory
`$(PREFIX)/lib/gaeb-sql-converter` and creates symbolic links to a freshly
created wrapper script `$(PREFIX)/lib/gaeb-sql-converter/bin/run.sh` in
`$(PREFIX)/bin`. This script guarantees that all programs are invoked in
the correct context - the `$(PREFIX)/lib/gaeb-sql-converter` directory.

`butils/install.sh` is also able to download the current *GAEB XML Schema*
files from the [GAEB main website][gaeb].

Downloading and installing of the GAEB SML Schema files is done by one of the
invocations:
  - `butils/install.sh <some prefix> gaeb`, or
  - `butils/install.sh local gaeb`.

For `butils/install.sh` being able to install the GAEB SML Schema files,
two extra configuration files are required:
  - `conf/xsd-zipfiles`, and
  - `conf/xsd-schemafiles`.

Both files are required, because the GAEB stored the files partially in
ZIP archives, partially dirtectly.

Both file have the same format. They consist solely of lines of the format:
```
<target directory>|<GAEB XML Schema file/archive URL>
```

These configuration files must be adjusted from time to time, because the GAEB
sometimes removes old versions of the files and ZIP archives, and also makes
new versions available.

[gaeb]: https://www.gaeb.de/
[gaebda]: https://www.gaeb.de/de/service/downloads/gaeb-datenaustausch

## GAEB URL patterns

<table style="border-width: 0; border-collapse: collapse">
<tr>
<td>ZIP files:</td>
<td>https://www.gaeb.de/wp-content/uploads/YEAR/MONTH/ZIP-file</td>
</tr>
<tr>
<td>XSD files:</td>
<td>https://www.gaeb.de/wp-content/uploads/YEAR/MONTH/GAEB\_XSD-file</td>
</tr>
</table>
