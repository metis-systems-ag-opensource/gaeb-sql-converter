/* tostdstring.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Interface file for 'tostdstring.cc'
** (Transcode a Xerces-C++ 'XMLCh *' string into a 'std::string')
**
*/
#ifndef TOSTDSTRING_H
#define TOSTDSTRING_H

#include <string>

#include <xercesc/util/XMLString.hpp>

namespace std {

string to_string (const XMLCh *const xmlstring);

string to_string (const XMLCh *const xmlchunk, XMLSize_t size);

} /*namespace std*/

#endif /*TOSTDSTRING_H*/
