/* finit.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Functions for performing the initialisation and finalisation of the
** parts of the 'GAEB Reader' which need extra initialisation before and/or
** finalisation after use – such as 'Xerces-C' library.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <cstdlib>
#include <sysexits.h>

#include <iostream>
//#include <exception>

#include <xercesc/util/PlatformUtils.hpp>

#include "Common/sysutils.h"
#include "tostdstring.h"

#include "finit.h"

using std::cerr, std::endl;
using std::exception;
using std::string, std::to_string;
using xercesc::XMLPlatformUtils, xercesc::XMLException;

namespace GAEB {

static bool is_initialised = false;

void finalise();

/* Perform the initialisation of _all_ parts which need initialisation.
*/
void initialise()
{
    if (! is_initialised) {
	try {
	    XMLPlatformUtils::Initialize();
	    atexit (finalise);	// Automatic finalisation on termination
	    is_initialised = true;
	} catch (const XMLException &e) {
	    cerr << appname() << ": Initialisation failed; reason: " <<
		    to_string (e.getMessage()) << endl;
	    exit (EX_SOFTWARE);
	}
    }
}

void finalise()
{
    if (is_initialised) {
	XMLPlatformUtils::Terminate();
	is_initialised = false;
    }
}

} /*namespace GAEB*/
