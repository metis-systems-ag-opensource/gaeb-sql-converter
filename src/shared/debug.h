/* debug.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Enable or disable debugging ...
**
*/
#ifndef DEBUG_H
#define DEBUG_H

#define DEBUG 1

#if defined(DEBUG) && DEBUG > 0
#include <string>

std::string debug__genxtag (const std::string &n);

#define genxtag(v, n) ((v).xtag = debug__genxtag ((n)))

int debug_printf (const char *format, ...);

#define DBG(fmt, ...) (debug_printf (fmt, ##__VA_ARGS__))
//#define DBG(fmt, ...) (debug_printf (fmt __VA_OPT__(,) __VA_ARGS__))

#else

#define genxtag(v, n)

#define DBG(fmt, ...)

#endif /*DEBUG*/

#endif /*DEBUG_H*/
