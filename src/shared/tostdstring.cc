/* tostdstring.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Transcode a Xerces-C++ 'XMLCh *' string into a 'std::string'
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#include <cstdlib>
#include <iostream>
using std::cerr, std::endl, std::flush;
#endif /*DEBUG*/

#include "config.h"

#ifdef HAVE_ALLOCA
# include <alloca.h>
#endif

#include <stdexcept>

#include <xercesc/util/PlatformUtils.hpp>

#include "tostdstring.h"

using namespace xercesc;

namespace std {

string to_string (const XMLCh *const xmlstring)
{
    char *ires = XMLString::transcode (xmlstring);
    string res;
    if (ires) {
	res = string (ires);
	XMLString::release (&ires);
    }
    return res;
}

string to_string (const XMLCh *const xmlchunk, XMLSize_t size)
{
#ifdef HAVE_ALLOCA
    XMLCh *ires = (XMLCh *) alloca ((size + 1) * sizeof(XMLCh));
#else
    MemoryManager *const mm = XMLPlatformUtils::fgMemoryManager;
    XMLCh *ires = (XMLCh *) mm->allocate ((size + 1) * sizeof(XMLCh));
#endif
    if (! ires) {
	throw runtime_error ("Allocation of 'XMLCh *' string failed!");
    }
    XMLString::copyNString (ires, xmlchunk, size);
    ires[size] = (XMLCh) 0;
    auto res = to_string (ires);
#ifndef HAVE_ALLOCA
    mm->deallocate (ires);
#endif
    return res;
}

} /*namespace std*/
