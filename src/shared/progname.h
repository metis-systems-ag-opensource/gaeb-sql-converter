/* progname.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Interface part of the module 'progname'
** (Get a program name (either from the command line or via '/proc/self/exe').)
**
*/
#ifndef PROGNAME_H
#define PROGNAME_H

#include <string>

#include "pstream.h"

#define MAXPATHSZ 65536

struct progname {
    using string = std::string;
    progname() = delete;
    progname (const char *cpathname, const char *cpdefault = "c_prog");
    progname (const progname &x);
    progname &operator= (const progname &x);
    ~progname();
    string path() const;
    string name() const;
    operator string() const;
private:
    string _path, _name;
};

#endif /*PROGNAME_H*/
