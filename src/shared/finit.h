/* finit.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Interface part of 'finit.{cc,h}'
** (Functions for performing the initialisation and finalisation of the
**  parts of the 'GAEB Reader' which need extra initialisation before and/or
**  finalisation after use – such as 'Xerces-C' library.)
**
*/
#ifndef FINIT_H
#define FINIT_H

#include <string>

namespace GAEB {

void initialise();

void finalise();

} /*namespace GAEB*/

#endif /*FINIT_H*/
