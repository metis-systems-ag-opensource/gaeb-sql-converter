/* progname.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Get a program name (either from the command line or via '/proc/self/exe'
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <cstring>
#include <filesystem>
#include <string>
#include <utility>
#include <vector>

#include "pstream.h"

#include "progname.h"

#define MAXPATHSZ 65536

namespace fs = std::filesystem;

/**********************************************************\
* Helper functions for implementing the member function    *
* `progname (const char *, const char *)`.                 *
\**********************************************************/

using std::string;
using std::vector;

using hpresult = std::pair<fs::path, bool>;

// Result type of `split_line()`, a splitted version of the output
// lines from an invocation of the `mount` command
struct FsEntry {
    string node;	// The node name being mounted
    string path;	// The pathname of the mount point
    string type;	// The filesystem type
    string flags;	// The mount flags (in `(` and `)`).
};

// Split a single line of output from an invocation of the `mount` command
// into its components and return these components as an `FsEntry` object.
static FsEntry split_line (const string &line)
{
    string flags, type;
    FsEntry res { "", "", "", "()" };
    size_t ix, jx = string::npos;
    if (ix = line.find_last_of ("("); ix != string::npos) {
	res.flags = line.substr (ix); jx = ix;
	while (jx > 0 && line[jx - 1] == ' ') { --jx; }
    } else {
	res.flags = "()";
    }
    if (ix = line.find_last_of (" type ", jx); ix != string::npos) {
	size_t px = ix + 6;
	if (px < jx) {
	    res.type = line.substr (px, (jx - px));
	}
	jx = ix;
    }
    if (ix = line.substr (0, jx).find_first_of (" on "); ix != string::npos) {
	size_t px = ix + 4;
	res.path = line.substr (px, (jx - px));
	res.node = line.substr (0, jx);
    }
    return res;
}

// Search in the output of the `mount` command for an entry with the type
// `proc` or `procfs`.
static hpresult get_procfs()
{
    redi::ipstream mrx ("mount", vector<string> { "mount" });
    if (! mrx.is_open()) { return hpresult ("", false); }
    FsEntry mentry;
    bool proc_found = false;
    while (! mrx.eof()) {
	string line;
	getline (mrx, line);
	mentry = split_line (line);
	if (mentry.type == "proc" || mentry.type == "procfs") {
	    proc_found = true; break;
	}
    }
    while (! mrx.eof()) { string skip; getline (mrx, skip); }
    mrx.close();
    return hpresult (proc_found ? mentry.path : "", proc_found);
}

// If a process filesystem is mounted, read the content of the symbolic link
// with the pathname *procfs-path*`/self/exe`.
static hpresult read_procexe ()
{
    auto [procpath, found] = get_procfs();
    if (! found) { return hpresult ("", false); }
    fs::path procexe = procpath / "self" / "exe";
    if (! fs::exists (procexe) || ! is_symlink (procexe)) {
	return hpresult ("", false);
    }
    std::error_code ec;
    uintmax_t linksz = file_size (procexe, ec);
    if (linksz == (uintmax_t) -1) { return hpresult ("", false); }
    if (linksz > MAXPATHSZ) { return hpresult ("", false); }
    char *buf = new char[(size_t)linksz + 1];
    if (! buf) { return hpresult ("", false); }
    if (readlink (procexe.c_str(), buf, (size_t) linksz)) {
	delete[] buf; return hpresult ("", false);
    }
    string sres (buf, (size_t) linksz);
    delete[] buf;
    return hpresult (sres, true);
}

/**********************************************************\
* Implementations of the member functions of the struct    *
* class `progname`.                                        *
\**********************************************************/

// struct progname {

// Initialise a `progname` object with either the program name gotten through
// the `argv[]` argument vector, or, if this is the `nullptr`, from the
// program's *procfs-path*`/self/exe` symbolic link.
progname::progname (const char *cpathname, const char *cpdefault)
{
    fs::path progpath;
    if (cpathname == nullptr) {
	auto [ppath, found] = read_procexe();
	if (found) {
	    progpath = ppath;
	} else {
	    progpath = cpdefault;
	}
    } else {
	progpath = cpathname;
    }
    _name = progpath.filename().string();
    if (_name == "") { _name = cpdefault; }
    _path = (fs::absolute (progpath.parent_path())).string();
}

// Copy a `progname` object into another one
progname::progname (const progname &x) : _path(x._path), _name(x._name) { }

// Copy a `progname` object into another one
progname &progname::operator= (const progname &x)
{
    _path = x._path; _name = x._name;
    return *this;
}

progname::~progname() { }

// Return the pathname of the directory the program resides in.
string progname::path() const { return _path; }

// Return the program's filename.
string progname::name() const { return _name; }

progname::operator string() const { return _name; }

//private:
//    fs::path _path, _name;
//};
