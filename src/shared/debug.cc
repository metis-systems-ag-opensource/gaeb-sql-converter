/* debug.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Some definitions concerning debugging ...
**
*/

#include <cstdlib>
#include <cstdarg>
#include <iostream>
#include <string.h>

#include "debug.h"

#if defined(DEBUG) && DEBUG > 0

using std::cerr, std::endl;
using std::string, std::to_string;

static unsigned tagc = 0;

string debug__genxtag (const string &n)
{
    return n + "#" + to_string (tagc);
}

int debug_printf (const char *format, ...)
{
    char *out = nullptr;
    int written;
    va_list args;
    va_start (args, format);
    written = vasprintf (&out, format, args);
    va_end (args);
    cerr << out << endl;
    free (out);
    return written;
}

#endif /*DEBUG*/
