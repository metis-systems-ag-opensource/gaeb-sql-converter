/* config.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Small configuration file (manually generated)
**
*/
#ifndef CONFIG_H
#define CONFIG_H

#define HAVE_ALLOCA 1

#endif /*CONFIG_H*/
