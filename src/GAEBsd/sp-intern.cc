/* sp-intern.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** 'GAEBsdImpl' type and generator destroyer functions for it...
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#include <iostream>
using std::cerr, std::endl;
#endif /*DEBUG*/

#include "Common/fnutil.h"
#include "Common/tohex.h"

#include "sp-intern.h"

namespace GAEBsd {

// Sizes for pre-allocating some internal values (of `std::string` or
// `std::vector<...>` type). The reason is the avoidance of too many
// memory (re-)allocations; this helps to reduce heap fragmentation
// and can speed up the code using the corresponding (member) variables.
// With some experience, these values can be defined in a manner which
// reduces the number of memory reallocation to (near) 0.
//
#define PDATA_ITEMS_RESERVE 1024        // PData::items
#define PDATA_CONTEXT_RESERVE 64        // PData::context
#define IMPL_TEXTVAL_RESERVE 4096       // GAEBsdImpl::textval

using std::string;

GAEBsdImpl init_GAEBsdImpl (const char *filename)
{
    GAEBsdImpl res;
    res.filepath = (filename ? dirname (filename) : ".");
    PData &pdata = res.pdata;
    pdata.items.reserve (PDATA_ITEMS_RESERVE);
//    pdata.context.reserve (PDATA_CONTEXT_RESERVE);
    pdata.nc = 0;
    pdata.errors = 0;
    pdata.redefinable = false;
    pdata.skipContent = false;
//    res.textval.reserve (IMPL_TEXTVAL_RESERVE);
    res.errcount = 0;
    res.fatal = false;
    return res;
}

GAEBsdImpl *generate_GAEBsdImpl (const char *filename)
{
    GAEBsdImpl *res = new GAEBsdImpl;
    *res = init_GAEBsdImpl (filename);
    return res;
}

void destroy_GAEBsdImpl (GAEBsdImpl &p)
{
    p.filepath.clear();
    PData &pdata = p.pdata;
    pdata.items.clear();
//    pdata.context.clear();
    pdata.nc = 0;
    pdata.errors = 0;
    pdata.redefinable = false;
    pdata.skipContent = false;
    p.textval.clear();
    p.errcount = 0;
}

void destroy_GAEBsdImpl (GAEBsdImpl *&p)
{
    if (p) {
	destroy_GAEBsdImpl (*p);
	delete p; p = nullptr;
    }
}

} /*namespace GAEBsd*/
