/* schemaparser.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Interface part of 'schemaparser.{cc,h}'
** GAEB Schema (XSD) Parser.
**
*/
#ifndef SCHEMA_PARSER_H
#define SCHEMA_PARSER_H

#define REALPRIV 1

#include <map>
#include <string>
#include <memory>

#include <xercesc/sax2/Attributes.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/sax2/SAX2XMLReader.hpp>

/*#define REALPRIV*/

#ifndef REALPRIV
# include "sp-intern.h"
#endif

namespace GAEBsd {

XERCES_CPP_NAMESPACE_USE

struct ElemType;

#ifdef REALPRIV
struct GAEBsdImpl;
#endif

class GAEBsd : public DefaultHandler {
public:
    GAEBsd (const char *filename = nullptr);
    GAEBsd (const GAEBsd &x);
    GAEBsd (GAEBsd &&x);
    ~GAEBsd();
    GAEBsd &operator= (const GAEBsd &x);
    GAEBsd &operator= (GAEBsd &&x);

    void startElement (const XMLCh *const uri,
                       const XMLCh *const localname,
                       const XMLCh *const qname,
                       const Attributes &attrs);

    void endElement (const XMLCh *const uri,
                     const XMLCh *const localname,
                     const XMLCh *const qualname);
    void characters (const XMLCh *text, XMLSize_t textlen);
    void ignorableWhitespace (const XMLCh* const chars,
                              const XMLSize_t length);
    void startDocument();

    void startPrefixMapping (const XMLCh *const prefix,
			     const XMLCh *const uri);

    // SAX ErrorHandler interface functions.
    void warning(const SAXParseException& exc);
    void error(const SAXParseException& exc);
    void fatalError(const SAXParseException& exc);
    void resetErrors();
    void reset();

    void filepath (const char *p);

    PData &data();
    unsigned errCount() const;
    bool fatalError() const;

private:
#ifdef REALPRIV
    GAEBsdImpl *impl;
#else
    GAEBsdImpl impl;
#endif
}; /*GAEBsd*/

// A helper function which utilises the SAX2 parser generation
PData doparse (const char *filename);

} /*namespace GAEBsd*/

#endif /*SCHEMA_PARSER_H*/
