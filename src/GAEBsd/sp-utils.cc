/* sp-utils.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Some utility functions ...
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <cstdio>

#include "Common/flipflop.h"
#include "Common/sutil.h"
#include "Common/tohex.h"

#include "sp-utils.h"

namespace GAEBsd {

using std::set;
using std::ostream;
using std::string, std::to_string;

// Returns the XSD element name for a given PItem type tag (of type PType).
// Because of the names being constants, a 'const char *' is returned instead
// of a 'std::string'.
const char *ptype2s (PType ptype)
{
    switch (ptype) {
	case PType::attribute:      return "attribute";
	case PType::choice:         return "choice";
	case PType::complexContent: return "complexContent";
	case PType::complexType:    return "complexType";
	case PType::element:        return "element";
	case PType::extension:      return "extension";
	case PType::fractionDigits: return "fractionDigits";
	case PType::length:         return "maxLength";
	case PType::maxLength:      return "maxLength";
	case PType::restriction:    return "restriction";
	case PType::schema:         return "schema";
	case PType::sequence:       return "sequence";
	case PType::simpleContent:  return "simpleContent";
	case PType::simpleType:     return "simpleType";
	case PType::totalDigits:    return "totalDigits";
	default:                    return "***";
    }
}

string elTagName (const PItem &item, const string &elname, bool endtag)
{
    string res;
    auto elNameByType = ptype2s (item.itemtype);
    string name = (elname.empty() ? elNameByType : elname);
    res.reserve (60);
    res.append ("<");
    if (endtag) { res.append ("/"); }
    res.append (name);
    if (name != elNameByType) {
	res.append ("<!--"); res.append (elNameByType); res.append ("-->");
    }
    if (item.name) {
	res.append (" name=\""); res.append (*item.name); res.append ("\"");
    }
    res.append (">");
    return res;
}

string itemName (const PItem &item, bool with_addr)
{
    return (item.name ? *item.name : "(empty)") +
	   (with_addr ? " " + ptr2str (&item.name) : "");
}

string itemBasetype (const PItem &item)
{
    return (item.basetype ? *item.basetype : "(empty)");
}

string attr2s (const ItemAttr &attr)
{
    return "(n:\"" + attr.name + "\", t:\"" + attr.type + "\", d:\"" +
	   attr.defval + "\")";
}

string attrList (const ItemAttrs &attrs)
{
    string res = "(";
    flipflop<false> needcomma;
    for (auto &[an, av]: attrs) {
	if (needcomma) { res += ", "; }
	res += attr2s (av);
    }
    res += ")";
    return res;
}

string itemAttrs (const PItem &item)
{
    return attrList (item.attrs);
}

const char *rstype2s (Restriction::Type rst)
{
    switch (rst) {
	case Restriction::length: return "length";
	case Restriction::maxLength: return "maxLength";
	case Restriction::fractionDigits: return "fractionDigits";
	case Restriction::totalDigits: return "totalDigits";
	case Restriction::required: return "required";
        case Restriction::enumeration: return "enumeration";
	default: return "***";
    }
}

string itemRsList (const PItem &item)
{
    flipflop<false> needcomma;
    string res = "Restrictions(" + itemName (item) + "): {";
    res.reserve (4096);
    for (auto &rs: item.rs) {
	Restriction::Type rst = rs.type();
	if (needcomma) { res += ", "; }
	res += rstype2s (rst);
	switch (rst) {
	    case Restriction::enumeration: {
		flipflop<false> needcomma;
		const set<string> &evlist = rs.enumvalues();
		res += ": [";
		for (auto &ev: evlist) {
		    if (needcomma) { res += ", "; }
		    res += ev;
		}
		res += "]";
		break;
	    }
	    case Restriction::required:
		break;
	    default:
		res += ":" + to_string (rs.value());
	}
    }
    res += "}";
    {string _t(res); res.swap (_t);}
    return res;
}

static unsigned iw = 0;

ostream &iw_reset (ostream &s) { iw = 0; return s; }

void iw_back (unsigned w)
{
    iw = (w > iw ? 0 : iw - w);
}

ostream &indent (ostream &s, const string tag, int w)
{
    if (w > 0) {
	s << tag << (iw > 0 ? string (iw, ' ') : "");
	iw += (unsigned) w;
    } else if (w < 0) {
	unsigned uw = (unsigned) -w;
	if (uw > iw) { iw = 0; } else { iw -= uw; }
	s << tag << (iw > 0 ? string (iw, ' ') : "");
    } else {
	s << tag << (iw > 0 ? string (iw, ' ') : "");
    }
    return s;
}

using RsIndex = std::map<Restriction::Type, unsigned>;

static
void rs_insert (RestrList &rsl, const Restriction &rs, RsIndex &rsx)
{
    unsigned ix = rsl.size();
    auto [rsxp, inserted] = rsx.emplace (rs.type(), ix);
    if (inserted) {
	rsl.push_back (rs);
    } else {
	ix = rsxp->second;
	rsl[ix] = rs;
    }
}

RestrList mix_rs (const RestrList &other, const RestrList &preferred)
{
    // Combine the restrictions lists 'other' and 'preferred', preferring the
    // restrictions of 'preferred', and return the result of this mix.
    RestrList rsl;
    RsIndex rsx;
    for (auto &rs: preferred) { rs_insert (rsl, rs, rsx); }
    for (auto &rs: other) { rs_insert (rsl, rs, rsx); }
    return rsl;
}

RestrList mix_rs (const PItem &other, const PItem &preferred)
{
    // Same as above, but for the restriction lists of the 'Pitem' values
    // 'other' and 'preferred'.
    return mix_rs (other.rs, preferred.rs);
}

string DumpAddr (const void *addr)
{
    char buf[64];
    snprintf (buf, sizeof(buf), "%p", addr);
    return buf;
}

string DumpItem (const PItem &it, string indent, bool with_addrs)
{
    string res;
    res.reserve (16384);
    res = indent;
    if (with_addrs) { res += "[" + DumpAddr (&it.name) + "] "; }
    res += "("; res += ptype2s (it.itemtype);
    if (it.name) { res += " (name \"" + *it.name + "\")"; }
    if (it.basetype) { res += " (basetype \"" + *it.basetype + "\")"; }
    res += " (minOccurs " + to_string (it.min) + ") (maxOccurs " +
	   (it.max == (unsigned) -1 ? "unbounded" : to_string (it.max)) + ")";
    switch (it.modtype) {
	case ModificationType::restriction:
	    res += EOL + indent + "  ";
	    res += "(restricts \"" + (it.basetype ? *it.basetype : "") + "\")";
	    break;
	case ModificationType::extension:
	    res += EOL + indent + "  ";
	    res += "(extends \"" + (it.basetype ? *it.basetype : "") + "\")";
	    break;
	default:
	    break;
    }
    if (! it.attrs.empty()) {
	res += EOL + indent + "  (attrs";
	for (auto &[k, v]: it.attrs) {
	    res += " (" + k + " " + v.type;
	    if (! v.defval.empty()) { res += " \"" + v.defval + "\""; }
	    res += ")";
	}
	res += ")";
    }
    for (auto &sit: it.content) {
	res += EOL; res += DumpItem (sit, indent + "  ");
    }
    res += ")";
//    res += "[" + to_string (res.size()) + "]";
    res.shrink_to_fit();
//    { string t(res); res.swap (t); }
    return res;
}

string DumpAttr (const ItemAttr &a)
{
    string res;
    res.reserve (256);
    res = "(Attr " + a.name + "[" + DumpAddr (&a.name) + "] (";
    res += "(type " + a.type + ") (default \"" + a.defval + "\"))";
    res.shrink_to_fit();
//    { string t(res); res.swap (t); }
    return res;
}

string DumpStrMap (const std::map<string, string> &ssmap)
{
    string res;
    res.reserve (4096);
    res = "(";
    flipflop<false> need_dlm;
    for (auto &[k, v]: ssmap) {
	if (need_dlm) { res += " "; }
	res += "(" + k + " \"" + v + "\")";
    }
    res += ")";
    res.shrink_to_fit();
//    { string t(res); res.swap (t); }
    return res;
}

string DumpStrSet (const set<string> &sset)
{
    string res;
    res.reserve (4096);
    res = "(";
    flipflop<false> need_dlm;
    for (auto &el: sset) {
	if (need_dlm) { res += " "; }
	res += el;
    }
    res += ")";
    res.shrink_to_fit();
//    { string t(res); res.swap (t); }
    return res;
}

} /*namespace GAEBsd*/
