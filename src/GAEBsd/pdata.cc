/* pdata.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Small member function implementation part for the types of 'pdata.h'
**
*/

#include <stdexcept>

#include "pdata.h"

namespace GAEBsd {

using std::domain_error;
using std::set;
using std::string;

Restriction::Restriction (Type rst, size_t v)
  : _type(rst)
{
    if (_type == enumeration) {
	throw domain_error
	    ("Can't init an 'enumeration' restriction with an integer value");
    }
    _value = v;
}

Restriction::

Restriction::Restriction (const std::string &ev)
  : _value(set<string> { ev }), _type(enumeration)
{ }

size_t Restriction::value() const
{
    if (_type == enumeration) {
	throw std::domain_error
	    ("Restriction is of type 'enumeration'");
    }
    return std::get<size_t> (_value);
}

const std::set<string> &Restriction::enumvalues() const
{
    if (_type != enumeration) {
	throw std::domain_error
	    ("Restriction is not of type 'enumeration'");
    }
    return std::get<set<string>> (_value);
}

bool Restriction::addEnum (const string &ev)
{
    if (_type != enumeration) {
	throw domain_error ("Restriction is not of type 'enumeration'");
    }
    set<string> &_enumvalues = std::get<set<string>> (_value);
    auto [_it, done] = _enumvalues.insert (ev);
    return done;
}

unsigned Restriction::addEnums (const set<string> &evl)
{
    unsigned failures = 0;
    if (_type != enumeration) {
	throw domain_error ("Restriction is not of type 'enumeration'");
    }
    set<string> &_enumvalues = std::get<set<string>> (_value);
    for (const string &ev: evl) {
	auto [it, done] = _enumvalues.insert (ev);
	if (! done) { ++failures; }
    }
    return failures;
}

PItem::PItem (PType itemtype, bool simpleContent)
  : name(NoString), basetype(NoString), tname(), content(), attrs(), rs(),
    min(1), max(1), itemtype(itemtype), simpleContent(simpleContent),
    hasErrors(false), nodissect(false), typex(0)
{
    switch (itemtype) {
	case PType::restriction:
	case PType::enumeration:
	    modtype = ModificationType::restriction;
	    break;
	case PType::extension:
	    modtype = ModificationType::extension;
	    break;
	default:
	    modtype = ModificationType::none;
	    break;
    }
}

PItem::PItem (PType itemtype, bool simpleContent, const OptString &name,
	      const OptString &basetype)
  : name(name), basetype(basetype), tname(), content(), attrs(), rs(),
    min(1), max(1), itemtype(itemtype), simpleContent(simpleContent),
    hasErrors(false), nodissect(false), typex(0)
{    
    switch (itemtype) {
	case PType::restriction:
	case PType::enumeration:
	    modtype = ModificationType::restriction;
	    break;
	case PType::extension:
	    modtype = ModificationType::extension;
	    break;
	default:
	    modtype = ModificationType::none;
	    break;
    }
}

} /*namespace GAEBsd */
