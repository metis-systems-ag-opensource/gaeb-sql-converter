/* ogen.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Generator for the output (DB schema / INSERT creation data)
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <iostream>
#include <map>
#include <optional>
#include <set>
#include <stdexcept>
#include <utility>
#include <vector>

#include "Common/flipflop.h"
#include "Common/sutil.h"

#include "sp-utils.h"

#include "ogen.h"

namespace GAEBsd {

#ifdef DEBUG
using std::cerr, std::cout;
#endif /*DEBUG*/

using std::ostream;
using std::endl, std::flush;
using std::exception, std::runtime_error;
using std::string, std::operator""s, std::to_string;
using std::make_pair;
using std::vector;

using AltMap = std::map<string, string>;
struct AltNames {
    AltNames (const string &tname, const string &ename)
      : type2alt({ { tname, ename } }), altcc(0)
    { }
    std::pair<AltMap::iterator, bool> emplace (const string &tname, const string &ename) {
	    unsigned ncc = altcc + 1;
	    string nename = ename + "_" + to_string (ncc);
	    auto res = type2alt.emplace (tname, nename);
	    if (res.second) { altcc = ncc; }
	    return res;
    }
    AltMap::iterator begin() { return type2alt.begin(); }
    const AltMap::const_iterator begin() const { return type2alt.begin(); }
    AltMap::iterator end() { return type2alt.end(); }
    const AltMap::const_iterator end() const { return type2alt.end(); }
    const string &operator[] (const string &tname) const { return type2alt.at (tname); }
    bool has (const string &tname) const { return type2alt.count (tname) > 0; }
private:
    AltMap type2alt;
    unsigned altcc;
};
using CreatedMap = std::map<string, AltNames>;
using InsertedMap = std::map<string, bool>;

// Internal functions which don't require data supplied through the
// 'OutGenerator' object.

//
static inline
void check_name (const PItem &item)
{
    if (! item.name) {
        throw runtime_error
            ("INTERNAL ERROR! Attempt to use an anonymous <"s +
             ptype2s (item.itemtype) + ">.");
    }
}

static inline
void check_basetype (const PItem &item)
{
    if (! item.basetype) {
        throw runtime_error
            ("INTERNAL ERROR! <"s + elTagName (item) +
             "> requires a base type.");
    }
}

enum class Ctx : uint8_t { simpl, cmplx };
using FKey = std::pair<string, string>;
using FKeyOpt = std::optional<FKey>;
using FKMap = std::map<string, string>;

using ColFKey = std::pair<string, FKeyOpt>;
#define NoFKey (std::optional<FKey>())

static
bool add_foreign_key (const FKey &fk, FKMap &fkeys)
{
    auto [_fkp, done] = fkeys.insert (fk);
    return done;
}

class OutGenerator {
public:
    OutGenerator (PData &pdata, OStrings &ostr, ostream &outfile,
		  const string &dbname)
      : _ostr(&ostr), dbname(dbname), created(), _pdata(&pdata),
	_outfile(&outfile)
    {
	_schema = &pdata.items.front();
	check_name (*_schema);
	xsprefix = *_schema->name;
    }
    ostream &outfile() { return *_outfile; }
    PData &pdata() { return *_pdata; }
    void preamble();
    string gen_primary (PItem &pitem);
    PItem &schema() { return *_schema; }
protected:
    string ogtype (const string &xstype, const RestrList &rslist);
    string gen_attrcolumn (const string &name, const ItemAttr &val);
    ColFKey gen_tblcolumn (PItem &item, Ctx ctx);
    ColFKey gen_choice (PItem &item, Ctx ctx);
    ColFKey gen_sequence (PItem &item, Ctx ctx);
    ColFKey gen_simplelement (PItem &item, Ctx ctx);
    ColFKey gen_cmplxelement (PItem &item, Ctx ctx);
    string gen_textcolumn (PItem &item);
    std::pair<string, bool> altName (PItem &it, const string &xname = "");
    bool add_delayed_foreign_key (const string &tbl, const FKey &fk);
    bool need_delay_fkey (const FKey &fk);
    void process_delayed_foreign_keys();
    bool handle_ref_column (FKeyOpt &colfkey, const string &tblname,
			    FKMap &column_table_refs);
private:
    std::map<string, FKMap> delayed_foreign_keys;
    OStrings *_ostr;		  // Either 'DBStrings' or 'MDStrings'
    string xsprefix, dbname;	  // XSD-Prefix ('xs:'), Database name
    CreatedMap created;		  // used by 'altName()'
    InsertedMap inserted;
    std::set<string> tbl_created; // Set when a table definition is completed.
    PData *_pdata;		  // A pointer to the parser output.
    ostream *_outfile;		  // A pointer to the `ostream` object used.
    PItem *_schema;
    std::map<string, unsigned> itemcounts;
};

/* Sometimes the table which is referenced through FOREIGN KEYs doesn't exist
** at the point where the table referencing it is requested. In this case,
** this member function returns 'true'; otherwise, and if the given argument
** if empty, it returns 'false'.
*/
bool OutGenerator::need_delay_fkey (const FKey &fko)
{
    return tbl_created.find (fko.second) == tbl_created.end();
}

/* Additionally, because of this possibility, a member function for collecting
** foreign keys (in the member variable 'delayed_foreign_keys') is required.
*/
bool OutGenerator::add_delayed_foreign_key (const string &tbl, const FKey &fk)
{
    FKMap nfk { fk };
    auto [ifkp, done] = delayed_foreign_keys.emplace (tbl, nfk);
    if (! done) {
	auto &ifk = ifkp->second;
	auto [_it, done] = ifk.insert (fk);
	return done;
    }
    return done;
}

/* Last not least, a member function for handling table references (foreign
** keys) is required. Because of the repetition of the code required for the
** decision if foreign keys should handled either directly or delayed (in an
** 'ALTER TABLE' statement at the end of the generated SQL code), i put all
** of the corresponding code together in one member function
** 'handle_ref_column()'. Handling the foreign keys (save from generating the
** constraints) is done by one invocation of this function at the corresponding
** point.
*/
bool OutGenerator::handle_ref_column (FKeyOpt &colfkey,
				      const string &tblname,
				      FKMap &column_table_refs)
{
    if (_ostr->need_fkconstr() && colfkey) {
	if (need_delay_fkey (*colfkey)) {
	    return add_delayed_foreign_key (tblname, *colfkey);
	} else {
	    return add_foreign_key (*colfkey, column_table_refs);
	}
    }
    return true;
}

/* Creates a tablename from an item's `name` and `basetype`.
** @returns the "created" name and a flag, which is `false` if the name already existed, and
**          `true` if the "created" name is really new.
*/
std::pair<string, bool> OutGenerator::altName (PItem &it, const string &xname)
{
    string itname = (xname.empty() ? *it.name : xname);
    string ittype = it.tname; //(it.basetype ? *it.basetype : "");
    // This is a compromise. Principially, element definitions with the same
    // type can occur
    //   - exactly once (the standard case)
    //   - at least once, which is no problem, because the corresponding
    //     column(s) in the database tables are always NULLable,
    //   - more than once.
    // The latter case *must* lead to a different table definition, because
    // not every table has an additional index (`ix`) column. On the other
    // hand, it is unimportant how many elements of this type may occur,
    // because this is neither specified in the GAEB document, not is it
    // stored in the database, so an addition of a simple `_mv` to the type
    // name may be sufficient here fir distinguishing the corresponding
    // element(s).
    // This is a *compromise*, because elements may have the same name, but
    // differ in their attribute definitions, but this case must be handled
    // somewhere else (probably in one of the passes before the code
    // generation, which is the task of this module).
    if (it.max > 1) { ittype += "_mv"; }
    auto [ptr, is_new] = created.emplace (itname, AltNames (ittype, itname));
    if (! is_new) {
	AltNames &altNames = ptr->second;
	auto [ptr1, is_new] = altNames.emplace (ittype, itname);
	return std::make_pair (ptr1->second, is_new);
    }
    return std::make_pair (itname, is_new);
}

/*##EXPORT##*/
/* This function used the previously parsed XML schema definition and
** generates output for either a database schema (an SQL file) or an
** INSERTion specifications.
*/
void gen_output (OStrings &ostr, PData &pdata, ostream &outfile,
		 const string &dbname)
{
    OutGenerator ogen (pdata, ostr, outfile, dbname);
    auto &schema = ogen.schema();
    ogen.preamble();
    for (auto &si: schema.content) {
	if (si.itemtype != PType::element) { continue; }
	ogen.gen_primary (si);
    }
}
/*##END##*/

/* Write the code introducing the database schema SQL file or the file which
** contains the INSERT specifications.
*/
void OutGenerator::preamble()
{
    (*_outfile) << _ostr->preamblestr (dbname);
}

/* Tests if a given type is a valid XML Schema pre-defined type and generates
** a matching database or insertion specification for this type. The generated
** type is returned as string.
*/
string OutGenerator::ogtype (const string &xstype, const RestrList &rslist)
{
    string type;
    if (! is_prefix (xsprefix + ":", xstype)) {
	throw runtime_error
	    ("INTERNAL ERROR! Unresolved type '" + xstype + "'");
    }
    type = xstype.substr (xsprefix.size() + 1);
    return _ostr->typestr (type, rslist);
}

/* Generates code for a single attribute (always within the table representing
** the element which contains this attribute).
*/
string OutGenerator::gen_attrcolumn (const string &name, const ItemAttr &val)
{
    return _ostr->attrstr (name, ogtype (val.type, val.rs));
}

/* Generates the code for a table representing a <complexType> element.
*/
ColFKey OutGenerator::gen_cmplxelement (PItem &item, Ctx ctx)
{
    check_name (item);
    auto [tblname, is_new] = altName (item);
    string elname = *item.name;
    bool is_array = item.max > 1;

    if (is_new) {
	// Map holding the references to external tables ("FOREIGN KEY")
	FKMap column_table_refs;

	// Because there are elements which have more than one definition, the
	// element name isn't sufficient for the decision if a new table must
	// be generated or not. But the combination of `element name` and `type`
	// should be sufficient
	string tblspec = _ostr->tbl_prefix (tblname, elname, is_array,
					    item.nodissect);
#if 0
	if (item.nodissect) {
	    // Ignore 'item.content', use only the attributes, and then
	    // insert a '_content' element of type 'TEXT' artificially
	    for (auto &[an, av]: item.attrs) {
		tblspec += _ostr->itdel() + gen_attrcolumn (an, av);
	    }
	    tblspec += _ostr->itdel() + gen_textcolumn (item);
	    tblspec += _ostr->tbl_suffix (tblname, is_array);
	    (*_outfile) << tblspec << EOL;
	    _outfile->flush();
	    tbl_created.insert (tblname);
	    return ColFKey (_ostr->etbl_column (elname, tblname, is_array),
			    FKey (_ostr->tref_colname (elname), tblname));
	}
#endif
	for (auto &ci: item.content) {
	    auto [colstr, colfkey] = gen_tblcolumn (ci, Ctx::simpl);
	    handle_ref_column (colfkey, tblname, column_table_refs);
	    tblspec += _ostr->itdel() + colstr;
	}
	for (auto &[an, av]: item.attrs) {
	    tblspec += _ostr->itdel() + gen_attrcolumn (an, av);
	}
	// Some items consist only of their attributes and have _NO_ content!
	// An empty basetype seems to indicate this.
	if (item.content.size() == 0 && item.basetype) {
	    check_basetype (item);
	    tblspec += _ostr->itdel() + gen_textcolumn (item);
	}
	tblspec += _ostr->add_fkconstr (column_table_refs);
	tblspec += _ostr->tbl_suffix (tblname, is_array);
	(*_outfile) << tblspec << EOL;
	_outfile->flush();
	tbl_created.insert (tblname);
    }
    return ColFKey (_ostr->etbl_column (elname, tblname, is_array),
		    FKey (_ostr->tref_colname (elname), tblname));
}

/* Generates code for a table representing a '<choice>'.
*/
ColFKey OutGenerator::gen_choice (PItem &item, Ctx ctx)
{
    string tspec;
    check_name (item);
    auto [tblname, is_new] = altName (item);
    bool is_array = item.max > 1;

    if (is_new) {
	// Map holding the references to external tables ("FOREIGN KEY")
	FKMap column_table_refs;
	tspec.reserve (4096);
	tspec = _ostr->tbl_prefix (tblname, tblname, is_array);
	for (auto &ci: item.content) {
	    auto [colstr, colfkey] = gen_tblcolumn (ci, Ctx::cmplx);
	    handle_ref_column (colfkey, tblname, column_table_refs);
	    tspec += _ostr->itdel() + colstr;
	}
	tspec += _ostr->add_fkconstr (column_table_refs);
	tspec += _ostr->tbl_suffix (tblname, is_array);
	(*_outfile) << tspec << EOL;
	_outfile->flush();
	tbl_created.insert (tblname);
    }
    return ColFKey (_ostr->ctbl_column (tblname, is_array),
		    FKey (_ostr->tref_colname (tblname), tblname));
}

/* Generates code for a either table which represents a <sequence> (with the
** 'maxOccurs' attribute to set either to a value > 1 or the value
** "unbounded"), or a list of values (which is directly returned instead).
*/
ColFKey OutGenerator::gen_sequence (PItem &item, Ctx ctx)
{
	check_name (item);
	auto [tblname, is_new] = altName (item);
	bool is_array = item.max > 1;

	if (is_new) {
	    // Map holding the references to external tables ("FOREIGN KEY")
	    FKMap column_table_refs;
	    string tspec;
	    tspec.reserve (4096);
	    tspec = _ostr->tbl_prefix (tblname, tblname, is_array);
	    for (auto &ci: item.content) {
		auto [colstr, colfkey] = gen_tblcolumn (ci, Ctx::cmplx);
		handle_ref_column (colfkey, tblname, column_table_refs);
		tspec += _ostr->itdel() + colstr;
	    }
	    tspec += _ostr->add_fkconstr (column_table_refs);
	    tspec += _ostr->tbl_suffix (tblname, is_array);
	    (*_outfile) << tspec << EOL;
	    _outfile->flush();
	    tbl_created.insert (tblname);
	}
	return ColFKey (_ostr->stbl_column (tblname, is_array),
			FKey (_ostr->tref_colname (tblname), tblname));
}

/* Generates the code for the text content of a <complexType> element with
** simple (textual) content and extra attributes.
*/
string OutGenerator::gen_textcolumn (PItem &item)
{
    return _ostr->textcont (ogtype (*item.basetype, item.rs), "");
}

/* Generates a column for a <simpleType> element (an element consisting solely
** of its textual content, like (e.g.) '<Version>3.2</Version>' in the
** <GAEBInfo> element
*/
ColFKey OutGenerator::gen_simplelement (PItem &item, Ctx ctx)
{
    check_name (item);
    bool is_table = item.max > 1 || item.attrs.size() > 0;
    string elname = *item.name;

    if (is_table) {
	auto [tblname, is_new] = altName (item);
	bool is_array = item.max > 1;

	if (is_new) {
	    string tspec, forcedtext;
	    tspec.reserve (4096);
	    tspec = _ostr->tbl_prefix (tblname, elname, is_array,
				       item.nodissect);
	    for (auto &[an, av]: item.attrs) {
		tspec += _ostr->itdel() + gen_attrcolumn (an, av);
	    }
	    tspec += _ostr->itdel() + gen_textcolumn (item);
	    tspec += _ostr->tbl_suffix (tblname, is_array);
	    (*_outfile) << tspec << EOL;
	    _outfile->flush();
	    tbl_created.insert (tblname);
	}
	return ColFKey (_ostr->etbl_column (elname, tblname, is_array),
			FKey (_ostr->tref_colname (elname), tblname));
    } else {
	check_basetype (item);
	string basetype = (item.basetype ? *item.basetype : "");

	return ColFKey (_ostr->columnstr (elname, ogtype (basetype, item.rs)),
			NoFKey);
    }
}

/* Generates a single column for either the database schema or th insertion
** descriptions. Because such a table column probably refers to another table,
** this table has to be generated first, which leads to the recursive
** invocations of 'gen_listtable()', 'gen_simplelement()', 'gen_cmplxelement()',
** 'gen_choice()' or 'gen_sequence()'.
*/
ColFKey OutGenerator::gen_tblcolumn (PItem &item, Ctx ctx)
{
    ColFKey res;
    switch (item.itemtype) {
	case PType::element: {
	    check_name (item);
	    if (item.simpleContent) {
		res = gen_simplelement (item, ctx);
	    } else {
		res = gen_cmplxelement (item, ctx);
	    }
	    break;
	}
	case PType::choice: {
	    res = gen_choice (item, ctx);
	    break;
	}
	case PType::sequence: {
	    res = gen_sequence (item, ctx);
	    break;
	}
	default: {
	    throw runtime_error ("Unsupported element type: " +
				 elTagName (item));
	}
    }
    return res;
}

/* Generate code for the outmost element(s) in the (reduced) schema definition.
** This function is invoked from the 'gen_output()' function which is the only
** "object" exported from this module.
*/
string OutGenerator::gen_primary (PItem &item)
{
    if (! item.name) {
	throw runtime_error
	    ("INTERNAL ERROR! Attempt to create a table for an anonymous <"s +
	     ptype2s (item.itemtype) + ">.");
    }
    auto [itname, is_new] = altName (item);
    if (is_new) {
	// Map holding the references to external tables ("FOREIGN KEY")
	FKMap column_table_refs;
	string ddlst;
	ddlst.reserve (4096);
	ddlst = _ostr->tbl_prefix (itname, "", false, item.nodissect);
	for (auto &ci: item.content) {
	    auto [colstr, colfkey] = gen_tblcolumn (ci, Ctx::simpl);
	    handle_ref_column (colfkey, itname, column_table_refs);
	    ddlst += _ostr->itdel() + colstr;
	}
	for (auto &[an, av]: item.attrs) {
	    ddlst += _ostr->itdel() + gen_attrcolumn (an, av);
	}
	ddlst += _ostr->add_fkconstr (column_table_refs);
	ddlst += _ostr->tbl_suffix (itname, false);
	(*_outfile) << ddlst << EOL;
	_outfile->flush();
	tbl_created.insert (itname);
	if (! delayed_foreign_keys.empty()) {
	    for (auto &[tbl, foreign_keys]: delayed_foreign_keys) {
		(*_outfile) << EOL <<
			       _ostr->modify_table_addfkeys (tbl, foreign_keys);
	    }
	    (*_outfile) << EOL;
	}
	_outfile->flush();
    }
    return itname;
}

} /*namespace GAEBsd*/
