/* nodis.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Force elements whose names appear in a set of names (given ar argument)
** to be elements of simple content, but with a special flag 'nodissect' being
** set.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#include <iostream>
using std::cerr, std::endl;
#endif /*DEBUG*/

#include <stdexcept>
#include <utility>

#include "Common/tohex.h"

#include "sp-utils.h"

#include "nodis.h"

namespace GAEBsd {

#define STRINGTYPE "string"

using std::set;
using std::exception, std::runtime_error;
using std::string, std::operator""s, std::to_string;
using std::pair, std::make_pair, std::move;

class NoDissect {
public:
    NoDissect (const NDisSet &ndis_names);
    void nodis (PItem &it);
    void nodis_subs (PItem &it);
    void nodis_element (PItem &it);
    void nodis_simple (PItem &it);
    void set_xsdns (const string &xsdpfx, const string &xsdns);
    string stringtype() const { return xsdpfx + ":" + STRINGTYPE; }
private:
    string xsdpfx, xsdns;
    NDisSet ndis_names;
};

/*##EXPORT##*/
void set_nodissect (PData &pdata, const NDisSet &ndis_names)
{
    NoDissect nd (ndis_names);
    PItem &schema = pdata.items.front();
    PContent &cont = schema.content;

    nd.set_xsdns (schema.name ? *schema.name : "",
		  schema.basetype ? *schema.basetype : "");
    for (PItem &it: cont) {
	if (it.itemtype == PType::element) {
	    nd.nodis_element (it);
	}
    }
}
/*##END##*/

NoDissect::NoDissect (const NDisSet &_ndis_names)
  : ndis_names(_ndis_names)
{ }

void NoDissect::set_xsdns (const string &_xsdpfx, const string &_xsdns)
{
    xsdpfx = _xsdpfx;
    xsdns = _xsdns;
}

static
bool has (const NDisSet &ndis_names, const string &name)
{
    return ndis_names.count (name) > 0;
}

void NoDissect::nodis (PItem &it)
{
    switch (it.itemtype) {
	case PType::choice:
	    nodis_subs (it);
	    break;
	case PType::complexType:
	    nodis_subs (it);
	    break;
	case PType::element:
	    nodis_element (it);
	    break;
	case PType::sequence:
	    nodis_subs (it);
	    break;
	case PType::simpleType:
	    nodis_simple (it);
	    break;
	default:
	    // Do NOTHING here!
	    break;
    }
}

void NoDissect::nodis_subs (PItem &it)
{
    if (it.modtype == ModificationType::nd_processed) { return; }
    it.modtype = ModificationType::nd_processed;
    for (auto &subit: it.content) {
	nodis (subit);
    }
}

void NoDissect::nodis_element (PItem &it)
{
    if (it.modtype == ModificationType::nd_processed) { return; }
    it.modtype = ModificationType::nd_processed;
    if (! it.name) {
	throw runtime_error ("Element without a name found!");
    }
    string itname = *it.name;
    if (! it.simpleContent) {
	if (has (ndis_names, itname)) {
	    // Don't know if inspecting the sub-content is necessary or not,
	    // so i'm leaving the following line
	    for (auto &subit: it.content) { nodis (subit); }
	    // ATTENTION! An element (exactly: a complex type element) which
	    // should not be resolved, must be reduced to a simple type
	    // element, which _must_ have a basetype (of the XML Schema type
	    // `string`), no content, and its `nodissect` flag must be set to
	    // true.
	    it.content.clear();		// *No* content
	    it.basetype = stringtype();	// XML Schema type `string` (xs:string)
	    it.simpleContent = true;	// Force "simple type content"
	    it.nodissect = true;	// No dissolving!
	} else {
	    for (PItem &subit: it.content) {
		nodis (subit);
	    }
	}
    }
}

void NoDissect::nodis_simple (PItem &it)
{
    if (it.modtype == ModificationType::nd_processed) { return; }
    it.modtype = ModificationType::nd_processed;
}

} /*namespace GAEBsd*/
