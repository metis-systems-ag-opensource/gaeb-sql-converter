/* nodis.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Interface part of 'nodis.{cc,h}'
** (Force elements whose names appear in a set of names (given ar argument)
**  to be elements of simple content, but with a special flag 'nodissect' being
**  set.)
**
*/
#ifndef NODIS_H
#define NODIS_H

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <set>
#include <string>

#include "pdata.h"

namespace GAEBsd {

using NDisSet = std::set<std::string>;

void set_nodissect (PData &pdata, const NDisSet &ndis_names);

} /*namespace GAEBsd*/

#endif /*NODIS_H*/
