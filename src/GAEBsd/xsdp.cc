/* xsdp.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Hiding the ugly details (Xerces-C) of the schema parser from the
** main program.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <fstream>
#include <iostream>
#include <stdexcept>

#include <xercesc/util/PlatformUtils.hpp>
//#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>

#include <xercesc/util/OutOfMemoryException.hpp>

#include "tostdstring.h"
#include "pdata.h"
#include "schemaparser.h"

#include "xsdp.h"

namespace GAEBsd {

using std::ifstream, std::endl, std::flush;
using std::runtime_error;
using std::string, std::operator""s, std::to_string;

using std::cerr, std::cout, std::endl, std::ends, std::flush;

PData xsdparse (const char *filename)
{
    PData pdata = doparse (filename);
    return pdata;
}

} /*namespace GAEBsd*/
