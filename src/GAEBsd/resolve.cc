/* resolve.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Resolve <element> nodes (by inserting the data structures they refer to).
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#include <iostream>
using std::cerr, std::cout, std::endl, std::flush;
#endif /*DEBUG*/


#include <cctype>
#include <map>
#include <stdexcept>
#include <set>
#include <string>

#include "Common/flipflop.h"
#include "Common/sutil.h"
#include "sp-utils.h"
#include "resolve.h"

namespace GAEBsd {

using std::map;
using std::runtime_error;
using std::set;
using std::string, std::operator""s;

class Resolver {
public:
    Resolver (PData &pdata);
    void resolve (string &gaeb_phase,
		  string &gaeb_version,
		  string &gaeb_versdate);
protected:
    void resolve_element (PItem &el);
    void resolve_item (PItem &it);
    void resolve_attr (ItemAttr &attr);
    void resolve_itemattrs (PItem &it);
    void resolve_type (PItem &t);
    void resolve_simpletype (PItem &t);
    void resolve_complextype (PItem &t);
    void resolve_grouping (PItem &cs);
    void set_resolved (PItem &it);
    bool is_resolved (PItem &it);
    PItem *get_basetype (PItem &t);
    string xsprefix, phase_name, version, versdate, versdate2;
    TypeIndex *_tindex;
    PContent *_items;
};

static
const char *mtstring (ModificationType mt)
{
    switch (mt) {
	case ModificationType::none: return "[none]";
	case ModificationType::restriction: return "[restriction]";
	case ModificationType::extension: return "[extension]";
	case ModificationType::resolved: return "[resolved]";
	default: return "[]";
    }
}

bool Resolver::is_resolved (PItem &it)
{
    return it.modtype == ModificationType::resolved;
}

void Resolver::set_resolved (PItem &it)
{
    it.modtype = ModificationType::resolved;
}

void Resolver::resolve_attr (ItemAttr &attr)
{
    unsigned ax;
    if (is_prefix (xsprefix + ":", attr.type)) {
	// Already resolved!
	return;
    }
    if (! has (*_tindex, attr.type)) {
	throw runtime_error
	    ("INTERNAL ERROR! Type '" + attr.type + "' of attribute '" +
	     attr.name + "' isn't registered.");
    }
    ax = _tindex->at (attr.type);
    PItem &at = _items->at (ax);
    resolve_simpletype (at);
    if (! at.basetype) {
	throw runtime_error
	    ("INTERNAL ERROR! Resolved type of attribute '" + attr.name +
	     "' has no basetype!");
    }
    attr.type = *at.basetype;
    attr.rs.insert (attr.rs.end(), at.rs.begin(), at.rs.end());
}

void Resolver::resolve_itemattrs (PItem &it)
{
    for (auto &[an, av]: it.attrs) { resolve_attr (av); }
}

void Resolver::resolve_item (PItem &it)
{
    // Select the correct resolver function with the 'it.itemtype' member
    // variable.
    switch (it.itemtype) {
	case PType::all: case PType::annotation: case PType::attribute:
	case PType::complexContent: case PType::extension:
	case PType::fractionDigits: case PType::length: case PType::maxLength:
	case PType::restriction: case PType::schema: case PType::simpleContent:
	case PType::totalDigits: case PType::mvElement: case PType::altElement:
	    // These elements are either not passed to the resulting schema
	    // content, or are converted into something other than a 'PItem'
	    // or are generally not processed, so they are ignored here.
	    break;
	case PType::complexType: case PType::simpleType:
	    resolve_type (it);
	    break;
	case PType::choice:
	    // Need to resolve each element in the 'choice'
	    resolve_grouping (it);
	    break;
	case PType::element:
	    // Resolve an element
	    resolve_element (it);
	    break;
	case PType::group:
	    // `<group>` is currently not processed. Here, an (internal) error
	    // should be issued.
	    break;
	case PType::sequence:
	    // Need to resolve each element in a 'sequence'
	    resolve_grouping (it);
	    break;
	default:
	    throw runtime_error
		("INTERNAL ERROR! Unhandled structure type: " +
		 elTagName (it));
    }
}


// Resolve a '<sequence>' or a '<choice>'. Because both of these grouping
// elements consist only of '<element>'-elements andother grouping elements
// the resolving merely is  the resolving of each element in such a grouping
// element.
void Resolver::resolve_grouping (PItem &cs)
{
    bool resolved = is_resolved (cs);
    set_resolved (cs);
    if (resolved) { iw_back (2); return; }
    for (PItem &el: cs.content) { resolve_item (el); }
}

/******************************************************************************
* Local types and functions for setting the 'phase', 'version' and 'versdate' *
* member variables of the 'Resolver' object.                                  *
******************************************************************************/

// Associations between item names and member variables.
using I2VMap = std::map<string, string *>;

// Setting an indirectly referenced variable if the item `t` has an enumeration
// restriction with exactly one enumeration value...
//
static
bool set_special (string &var, PItem &t)
{
    const string &tName = itemName (t);
    for (Restriction &rs: t.rs) {
	if (rs.type() == Restriction::enumeration) {
	    const set<string> &evlist = rs.enumvalues();
	    size_t evlist_size = evlist.size();
	    if (evlist_size == 0) { continue; }
	    if (evlist_size >= 1) {
		auto evit = evlist.begin();
		var = *evit;
	    }
	    if (evlist_size == 1) { return true; }
	    cerr << "\nWARNING! Assuming the first entry (" << var <<
		    ") of a '" << tName <<
		    "' list with more than one element." << endl;
	    return false;
	}
    }
    cerr << "WARNING! Attempt to retrieve '" << tName <<
	    "' failed because this item contains no values." << endl;
    return false;
}

// Setting the value of an indirectly referenced variable which is selected
// from an 'Item2Var' element by the name of a given item.
//
static
void set_specials (const I2VMap &vItems, PItem &t)
{
    const string &tName = itemName (t);
    if (auto it = vItems.find (tName); it != vItems.end()) {
	string * var = it->second;
	set_special (*var, t);
    }
}

/******************************************************************************
******************************************************************************/

// Resolve a type
void Resolver::resolve_type (PItem &t)
{
    if (t.itemtype == PType::simpleType) {
	resolve_simpletype (t);
    } else {
	// An internal error always indicates a mistake done by the software
	// designer/developer. I'm raising an exception for identifying such
	// a problem.
	if (t.itemtype != PType::complexType) {
	    throw runtime_error
		("INTERNAL ERROR! '" + *t.name + "' is not a type!");
	}
	resolve_complextype (t);
    }

    // This is a BAD HACK! It is my solution for extracting the phase name,
    // the version and the version date from the schema file. Because the
    // resolver is no part of the original parser, i think i can integrate
    // this part here ...
    set_specials (I2VMap ({ { "tgDP", &phase_name },
			    { "tgVersion", &version },
			    { "tgGAEBVersDate", &versdate },
			    { "tgVersDate", &versdate2 }
			 }),
		  t);
}

// Returning either the pointer to the 'PItem' structure of the basetype of
// (the 'PItem' structure of) a given type, or 'nullptr', if the given type
// has no basetype.
PItem *Resolver::get_basetype (PItem &t)
{
    if (t.typex > 0) {
	return &_items->at (t.typex - 1);
    } else if (t.basetype) {
	if (auto txp = _tindex->find (*t.basetype); txp != _tindex->end()) {
	    return &_items->at (txp->second);
	}
    }
    return nullptr;
}

// Resolve a simple type
void Resolver::resolve_simpletype (PItem &t)
{
    bool resolved = is_resolved (t);
    set_resolved (t);
    // Return immediately if this type was previously resolved.
    if (resolved) { iw_back (2); return; }
    // '<simpleType>' 't' should always have a basetype.
    if (t.typex == 0 && ! t.basetype) {
	throw runtime_error
	    ("Attempt to resolve simple type" +
	     (t.name ? " (" + *t.name + ")" : ""s) +
	     " failed. The type has no base type.");
    }
    // Resolve the basetype first, and then, if this succeeded, the
    // type 't' by making the basetype of its current basetype the new
    // basetype of 't'. This removes the link to the original basetype
    // of 't', but this is OK, because 't' then refers directly to an
    // XSD type (which was the intention of this resolving process).
    string basetypename = *t.basetype;
    string btpfx = xsprefix + ":";
    bool elemental = is_prefix (btpfx, basetypename);
    if (! elemental) {
	// An internal error always indicates a mistake done by the
	// software designer/developer. I'm raising an exception for
	// identifying such a problem.
	if (! has (*_tindex, basetypename)) {
	    throw runtime_error
		("INTERNAL ERROR! No index found for '" +
		 basetypename + "'.");
	}
	PItem *pbt = get_basetype (t);
	auto &ot = *pbt;
	resolve_type (ot);
	if (! is_resolved (ot)) {
	    throw runtime_error
		("Attempt to resolve '" + basetypename +
		 "' failed. The deepest type found was: '" +
		 *ot.basetype + "'.");
	}
	//TODO! Merging the restrictions of the basetype 'ot' and 't'.
	// This is importent for getting the correct sizes of the
	// database attribute to be generated from 't' (or rather the
	// element referring to it).
	t.basetype = ot.basetype;
	t.typex = ot.typex;
    }
}

// Resolve a complex type
void Resolver::resolve_complextype (PItem &t)
{
    // Always resolve the attributes, because if some inner element refers
    // to this type (recursively), the attribute types of this inner element
    // remain unresolved, otherwise.
    resolve_itemattrs (t);
    ModificationType modtype = t.modtype;
    bool resolved = is_resolved (t);
    set_resolved (t);
    if (resolved) { iw_back (2); return; }
    PItem *pbt = get_basetype (t);
    if (! pbt) {
	// The '<complexType>' element has no base type. This means, that
	// it contains the structure directly.
	if (modtype != ModificationType::none) {
	    // It cannot be a restriction or an extension, because both of
	    // these require a basetype.
	    throw runtime_error
		("Type restriction or extension '" + *t.name +
		 "' without a basetype (" + mtstring (modtype) + ").");
	}
	// 1. Resolve the sub-item types
	for (auto &ci: t.content) { resolve_item (ci); }
	return;
    }

    // FACT: The <complexType> has a basetype!

    // Next actions:

    // 1. Finding the structure of this basetype (=> 'bt').
    // The member function 'at()' (both in 'std::vector<>' and in 'std::map<>')
    // is nearly the same as the index operator '[]', with one exception: The
    // element is _never_ extended if an unknown key or an index out of the
    // the capacity is given. The 'const' form of a 'std::map<>' works _only_
    // with 'at()', because of the potentially object extending property of
    // '[]' (which is clearly forbidden for a 'const' object).
    // (I'm missing the ternary operator '[]=' which is defined in more modern
    //  languages (like 'D', 'Nim', ...), because this operator would supersede
    //  the member function 'at()'.)
    PItem &bt = *pbt;

    // 2. Resolving 'bt'
    resolve_type (bt);

    // Here, we must distinguish between a '<complexType>' with
    // simple content (the 'simpleContent' flag should indicate this)
    // and one with a standard complex content.
    // The former leads to a resolvation similar to a '<simpleType>', but
    // additionally requires resolvation actions for its attributes.
    // The latter must be resolved in another routine which handles the
    // resolvation of either '<sequence>'-content or '<choice>'-content.
    // A complex type with complex content normally has no basetype, with
    // the exception for a type which is either a restriction or an
    // extension of some other type.
    if (t.simpleContent) {
	// For a 'complexType' with 'simpleContent', an 'extension' and a
	// 'restriction' are probably the same.
	//    ! bt.simpleContent => ERROR
	if (! bt.simpleContent) {
	    throw runtime_error
		("Base type of a 'simpleContent' type (" + *t.name +
		 ") must not be of 'complexContent'.");
	}
	t.basetype = bt.basetype;
	t.typex = bt.typex;
    } else { // FACT: 't' is of complex content!
	// The complex type can either be a restriction of an existing type,
	// or an extension of such a type, or it is completely new. As far as
	// i know (not completely sure), a restriction announces all elements
	// the complex type consists of, so it is likely something like a new
	// type. An extension on the other hand is a complex type which
	// contains either additionally content elements, or new attributes,
	// or both, which means that it consists of the content of the original
	// type (and its attributes) _plus_ the specified extra content and/or
	// attributes.
	PContent new_items;
	// FACT! Only a type extension introduces a new type, which may (or
	// may not) override an existing type, but previously inherits all of
	// its sub-structures and attributes.
	// A type restriction, otherwise, is a real replacement of an existing
	// type which clobbers all of the basetype's sub-structures, but
	// inherits the base type's attributes!
	// References:
	// [XML Schema Restriction for Complex types : Complete Redefinition?]
	// (https://stackoverflow.com/questions/14797543/xml-schema-restriction-for-complex-types-complete-redefinition)
	if (modtype == ModificationType::extension) {
	    // Get the content plus attributes of the original type ('bt') in
	    // the case of an extension.
	    new_items = bt.content;
	}
	// Now, add/insert the items from t ...
	for (auto &it: t.content) {
	    resolve_item (it);
	    new_items.push_back (it);
	}

	// Now complete the resolving by replacing the content, attributes,
	// and restrictions of 't' with the merged values
	t.content = new_items;
    }

    // Mix the attributes and restrictions of 't' and 'bt', storing the result
    // in 't'. This must be done in both cases of a complex type: the
    // complex type with simple content and the complex type with complex
    // content.
    ItemAttrs new_attrs;
    RestrList new_rs;
    if (t.modtype == ModificationType::extension) {
	new_attrs = bt.attrs;
	new_rs = bt.rs;
    }
    // (Probably) override existing elements
    for (auto &[n, v]: t.attrs) { new_attrs[n] = v; }
    t.attrs = new_attrs;
    t.rs = mix_rs (new_rs, t.rs);
}


static bool is_a_type (const PItem &t)
{
    return t.itemtype == PType::simpleType ||
	   t.itemtype == PType::complexType;
}

// Resolve an <element> structure
void Resolver::resolve_element (PItem &el)
{
    bool resolved = is_resolved (el);
    set_resolved (el);
    if (resolved) { iw_back (2); return; }
    if (el.basetype) {
	// Resolve the element ...
	string btname = *el.basetype;

	// Short cut for "elementary" types ... these are types which are
	// defined in XML-Schema and not the GAEB schema definition.
	if (is_prefix (xsprefix + ":", btname)) {
	    // Element's base type is already a simple type, meaning:
	    // the element's content is already "resolved".
	    // What remains is the resolving of the attributes.
	    resolve_itemattrs (el);
	    return;
	}

	// In the other case, the base type itself must exist...
	if (!has (*_tindex, btname)) {
	    throw runtime_error
		("INTERNAL ERROR! '" + btname + "' is unresolvable.");
	}
	PItem &t = _items->at (_tindex->at (btname));
	// ...be a type (otherwise it is an internal error)...
	if (! is_a_type (t)) {
	    throw runtime_error
		("INTERNAL ERROR! '" + *t.name + "' is not a type.");
	}
	// ...must not refer to itself...
	if (! t.name || *t.name != btname) {
	    throw runtime_error
		("INTERNAL ERROR! '" + btname +
		 "' can't be resolved to a valid type of the same name.");
	}
	// ...and must be resolvable.
	resolve_type (t);
	// Now, embed the content of `t` (it is a complex type) into the
	// element
	if (t.itemtype == PType::simpleType) {
	    el.simpleContent = true;
	    el.basetype = t.basetype;
	    el.rs = t.rs;
	} else if (t.itemtype == PType::complexType) {
	    if (! (el.content.empty() && el.attrs.empty())) {
		// An element with content and a reference to another type
		// should NEVER occur.
		throw runtime_error
		    ("INTERNAL ERROR! Attempt to embed content of a type into"
		     " the non-empty element '" + *el.name + "'.");
	    }
	    el.content = t.content; el.attrs = t.attrs;
	    el.rs = t.rs; el.simpleContent = t.simpleContent;
	    // In the case of a complex type with simple content, the basetype
	    // of the element must be kept, because it's the type of the
	    // element's textual content. In the case of a complex type with
	    // complex content, it should be removed instead.
	    if (el.simpleContent) {
		// *el.basetype is something like e.g. "tgNormalizedString",
		// so the basetype of el's basetype is the correct answer
		// (=> "xs:normalizedString").
		el.basetype = t.basetype;
		if (t.basetype) { el.tname = *t.basetype; }
	    } else {
		// Remove 'el.basetype', avoiding later problems in the
		// generator pass ...
		el.basetype = NoString;
		// but store the name of the basetype of 'el' in 'el.tname'
		// (which should be identical to '*el.basetype').
		el.tname = (t.name ? *t.name : *el.name); //if (t.name) { el.tname = *t.name; }
	    }
	}
	resolve_itemattrs (el);
    } else {
	// In an element with simple content, only the attributes are copied.
	resolve_itemattrs (el);
	// But in an element with complex content, the content must also be
	// resolved.
	if (! el.simpleContent) {
	    for (auto &it: el.content) {
		resolve_item (it);
	    }
	}
	el.tname = *el.name;
    }
}

Resolver::Resolver (PData &pdata)
{
    if (pdata.items.size() != 1) {
	// This is an ERROR!
	throw runtime_error ("INTERNAL ERROR! Parsing incomplete.");
    }
    PItem &schema = pdata.items.front();
    if (schema.itemtype != PType::schema) {
	throw runtime_error
	    ("INTERNAL ERROR! No 'schema' element found at parsing stack top.");
    }
    if (! schema.name) {
	throw runtime_error ("INTERNAL ERROR!: No XSD schema prefix found.");
    }
    xsprefix = *schema.name;
    _items = &schema.content;
    _tindex = &pdata.tindex;
};

static
string version_ident (const string &s)
{
    string res;
    res.reserve (s.size());
    for (unsigned ix = 0; ix < s.size(); ++ix) {
	if (char ch = s[ix]; isdigit (ch)) {
	    res.push_back (ch);
	} else if (ch == '.') {
	    res.push_back ('v');
	}
    }
    res.shrink_to_fit();
    return res;
}

static
string date2siso (const string &s)
{
    string res;
    res.reserve (s.size());
    size_t xp = s.find_first_of ("/-.");
    if (xp == string::npos) {
	res = s;
    } else {
	string y, m;
	switch (s[xp]) {
	    case '/': case '.': {
		// Assuming an anglo-saxian or german date
		m = s.substr (0, xp); y = s.substr (xp + 1);
		break;
	    }
	    default: /*case '-':*/ {
		// Assuming an ISO date
		y = s.substr (0, xp); m = s.substr (xp + 1);
		break;
	    }
	}
	if (m.size() < 2) { m = "0" + (m.size() < 1 ? "1" : m); }
	if (y.size() < 4) { y = "20"s + (y.size() < 2 ? "0" : "") + y; }
	res = y + m;
    }
    res.shrink_to_fit();
    return res;
}

void Resolver::resolve (string &gaeb_phase,
			string &gaeb_version,
			string &gaeb_versdate)
{
    for (auto &it: *_items) { resolve_item (it); }
    gaeb_phase = phase_name;
    gaeb_version = version_ident (version);
    gaeb_versdate = date2siso ((versdate.empty() ? versdate2 : versdate));
}

void resolve (PData &pdata,
	      string &phase_name,
	      string &version,
	      string &versdate)
{
    if (pdata.items.size() != 1) {
	// This is an ERROR!
	throw runtime_error ("INTERNAL ERROR! Parsing incomplete.");
    }
    Resolver resolver(pdata);
    resolver.resolve (phase_name, version, versdate);
    iw_reset (cerr);
    // Resolving of the '<element>' elements probably requires a second step,
    // because of recursive references in some of the data types which leaves
    // the corresponding elements unresolved.
}

} /*namespace GAEBsd*/
