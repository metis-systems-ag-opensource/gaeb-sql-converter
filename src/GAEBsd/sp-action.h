/* sp-action.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Interface part of 'spcation.{cc,h}'
** Parsing action routines for the different types of XSD-Elements
**
*/
#ifndef SP_ACTION_H
#define SP_ACTION_H

#include <string>

#include "pdata.h"

namespace GAEBsd {

void startAction (const std::string &uri,
		  const std::string &elname,
		  const PAttrs &attrs,
		  PData &pdata);

void endAction (const std::string &uri,
		const std::string &elname,
		PData &pdata);

void process_textcont (PData &pdata, const std::string &text);

} /*namespace GAEBsd*/

#endif /*SP_ACTION_H*/
