/* mdgen.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Generator for the structure data which is used by the GAEB parser for the
** construction of INSERT statements
**
*/

#include <ostream>
#include <string>

#include "pdata.h"

namespace GAEBsd {

void gen_metadata (PData &pdata, std::ostream &outfile,
		   const std::string &dbname);

} /*namespace GAEBsd*/
