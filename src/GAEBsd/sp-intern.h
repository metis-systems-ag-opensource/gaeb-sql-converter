/* sp-intern.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** 'GAEBsdImpl' type and generator destroyer functions for it...
**
*/
#ifndef SP_INTERN_H
#define SP_INTERN_H

#include <forward_list>
#include <string.h>

#include "pdata.h"

namespace GAEBsd {

using CtxStack = std::forward_list<std::string>;
struct GAEBsdImpl {
    PData pdata;
    std::string filepath, schema_prefix, schema_uri, gaeb_prefix, gaeb_uri;
    std::string textval;
    unsigned errcount;
    bool fatal;
};

GAEBsdImpl init_GAEBsdImpl (const char *filename);
GAEBsdImpl *generate_GAEBsdImpl (const char *filename);

void destroy_GAEBsdImpl (GAEBsdImpl &p);
void destroy_GAEBsdImpl (GAEBsdImpl *&p);

} /*namespace GAEBsd*/

#endif /*SP_INTERN_H*/
