/* mdgen.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Generator for the structure data which is used by the GAEB parser for the
** construction of INSERT statements
**
*/

#include <map>
#include <stdexcept>

#include "Common/sutil.h"

#include "ogen.h"

#include "mdgen.h"

namespace GAEBsd {

using std::ostream;
using std::map;
using std::runtime_error;
using std::string, std::to_string;

struct MDStrings : OStrings {
    string preamblestr (const std::string &dbname)
    {
	return "DATABASE " + dbname + EOL;
    }

    string typestr (const string &type, const RestrList &rslist)
    {
	if (type == "integer" || type == "positiveInteger"
	||  type == "nonNegativeInteger") {
	    return "i";
	}
	if (type == "date") { return "D"; }
	if (type == "time") { return "T"; }
	//if (type == "datetime") { return "d"; }
	//##maybe: if (type == "gYearMonth") { return "m"; }
	if (type == "gYearMonth") { return "s"; }
	if (type == "string") { return "s"; }
	if (type == "normalizedString") { return "s"; }
	if (type == "decimal") { return "f"; }
	//if (type == "real") { return "r"; }
	if (type == "token") { return "s"; }
	if (type == "ID" || type == "IDREF") { return "s"; }
	if (type == "NCName") { return "s"; }
	throw runtime_error
	    ("INTERNAL ERROR! Unknown schema type '" + type + "'");
    }

    string attrstr (const string &name, const string &type)
    {
	return "a_" + name + ":" + type;
    }

    string tbl_prefix (const string &tblname, const string &elname,
		       bool is_array)
    {
	return tbl_prefix (tblname, elname, is_array, false);
    }

    string tbl_prefix (const string &tblname, const string &elname,
		       bool is_array, bool nodis)
    {
	string res = tblname + "," + (elname.empty() ? tblname : elname) +
		     (nodis ? "!" : "") + ":gaebid:k,id:k";
	if (is_array) { res += ",ix:k"; }
	res.shrink_to_fit();
	return res;
    }

    string itdel() { return ","; }

    string add_fkconstr (const map<string, string> &fkconstr)
    {
	return "";
    }

    string tbl_suffix (const string &tblname, bool is_array)
    {
	return "";
    }

    string ctbl_column (const string &tblname, bool multivalue)
    {
	if (multivalue) {
	    return tref_colname (tblname) + ":y[" + tblname + "]";
	}
	return tref_colname (tblname) + ":y<" + tblname + ">";
    }

    string etbl_column (const string &colname, const string &tblname,
			bool multivalue)
    {
	if (multivalue) {
	    return tref_colname (colname) + ":X[" + tblname + "]";
	}
	return tref_colname (colname) + ":X<" + tblname + ">";
    }

    string stbl_column (const string &tblname, bool multivalue)
    {
	if (multivalue) {
	    return tref_colname (tblname) + ":x[" + tblname + "]";
	}
	return tref_colname (tblname) + ":x<" + tblname + ">";
    }

    string columnstr (const string &colname, const string &type)
    {
	return colname + ":" + type;
    }

    string textcont (const string &type, const string &name)
    {
	string colname = (name.empty() ? textcont_name : name);
	return colname + ":" + type;
    }

    bool need_fkconstr() const { return false; }

    string modify_table_addfkeys (const string &tblname,
				  const map<string, string> &fkconstr)
    {
	return "";
    }
};

void gen_metadata (PData &pdata, ostream &outfile, const string &dbname)
{
    MDStrings ostr;
    gen_output (ostr, pdata, outfile, dbname);
}

} /*namespace GAEBsd*/
