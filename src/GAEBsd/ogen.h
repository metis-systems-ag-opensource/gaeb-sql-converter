/* ogen.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Output generator for the database schema _and_ the insert generation
** metadata.
**
*/

#include <ostream>
#include <string>

#include "pdata.h"
#include "ostrings.h"

namespace GAEBsd {

void gen_output (OStrings &ostr, PData &pdata, std::ostream &outfile,
                 const std::string &name);

} /*namespace GAEBsd*/
