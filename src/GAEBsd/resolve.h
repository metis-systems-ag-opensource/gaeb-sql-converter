/* resolve.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Interface part of 'resolve.{cc,h}'
** Resolve all remaining element's of a GAEB schema definition after parsing
** (All `<element>`, `<simpleType>` and `<complexType>` elements), meaning:
**   - Resolve all `<simpleType>` elements to their XSD base types,
**   - Complete all `<complexType>` extensions and restrictions with the
**     items of their base types (resolving these base types beforehand),
**   - Complete
** Resolve <element> nodes (by inserting the data structures they refer to).
**
*/

#include <string>

#include "pdata.h"

namespace GAEBsd {

void resolve (PData &pdata,
	      std::string &phase_name,
	      std::string &version,
	      std::string &versdate);

} /*namespace GAEBsd*/
