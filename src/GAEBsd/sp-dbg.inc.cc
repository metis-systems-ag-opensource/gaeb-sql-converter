/* sp-dbg.inc.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** The debugging functions used in 'sp-action.cc'
**
*/
#ifndef SP_DBG_INC
#define SP_DBG_INC

#ifdef DEBUG

#include <cstdlib>

#include "Common/flipflop.h"
#include "Common/sutil.h"

static unsigned DBGindent = 0;

static __attribute__((unused))
void DBGstartTag (const char *dbgtag,
                  const string &elname,
                  const PAttrs &attrs,
                  const string &attrname,
                  const PData *pdata = nullptr)
{
    size_t dtlen = strlen (dbgtag);
    cerr << dbgtag << (dtlen < 5 ? ":  " : ": ") <<
            string (DBGindent, ' ') << "<" << elname;
    if (! attrname.empty() && has (attrs, attrname)) {
        cerr << " " << attrname << "=\"" << attrs.at (attrname) << "\"";
    }
    cerr << ">";
    if (pdata) {
        auto &items = pdata->items;
        int ix = (items.size() < 1 ? -1 : (int) lastx (pdata->items));
        cerr << ", ix = " << ix;
    }
    cerr << endl;
    DBGindent += 2;
}

static __attribute__((unused))
void DBGendTag (const char *dbgtag,
                const string &elname,
                const OptString &valOpt)
{
    size_t dtlen = strlen (dbgtag);
    DBGindent -= 2;
    cerr << dbgtag << (dtlen < 5 ? ":  " : ": ") <<
            string (DBGindent, ' ') << "</" << elname << ">";
    if (valOpt) { cerr << " <!-- " << *valOpt << " -->"; }
    cerr << endl;
}

static __attribute__((unused))
void DBGmessage (const char *dbgtag, initializer_list<string> l)
{
    size_t dtlen = strlen (dbgtag);
    cerr << dbgtag << (dtlen < 5 ? ":  " : ": ") <<
            string (DBGindent, ' ');
    bool first = true;
    for (auto &x: l) {
        if (first) { first = false; } else { cerr << ' '; }
        cerr << x;
    }
    cerr << endl;
}

static __attribute__((unused))
void DBGmessage (const char *dbgtag, const string &x)
{
    DBGmessage (dbgtag, { x });
}

static __attribute__((unused))
void DBGixAndOuter (const char *dbgtag,
                    unsigned ix,
                    const PItem *outer)
{
    DBGmessage (dbgtag,
                "ix = " + to_string (ix) + ", outer is a '" +
                (outer ? ", outer is a '" + elTagName (*outer) + "'" : ""s) +
                "'.");
}

#define MAXWIDTH 80;
static __attribute__((unused))
string dump_attrs (const PAttrs &attrs, unsigned indent = 4)
{
    size_t maxwidth = MAXWIDTH;
    char *columns, *p = nullptr;
    string res, line, el, comma;
    if (attrs.empty()) {
	res = "{ }";
    } else {
	res.reserve (1024);

	// Get the current terminal width. If the environment variable
	// 'COLUMNS' exists and contains an unsigned integer value between
	// 20 and 4096 (both bounds inclusive), 'maxval' gets this value.
	// Otherwise, the value of 'maxwidth' remains the same ('MAXWIDTH').
	if ((columns = getenv ("COLUMNS"))) {
	    unsigned long x = strtoul (columns, &p, 10);
	    if (p > columns && *p == '\0' && x >= 20 && x <= 4096) {
		maxwidth = x;
	    }
	}
	line.reserve (maxwidth + 1);
	el.reserve (maxwidth);

	flipflop<true> first;
	flipflop<false> not_first;
	res.push_back ('{');
	line.append (indent, ' ');
	for (auto &[k, v]: attrs) {
	    el.append ("{ \""); el.append (k); el.append ("\", \"");
	    el.append (v); el.append ("\" }");
	    if (first) { res.append (EOL); } else { line.push_back (','); }
	    if (line.size() + el.size() + 1 >= maxwidth) {
		res.append (line); res.append (EOL);
		line.erase(); line.append (indent, ' ');
	    } else {
		if (not_first) { line.push_back (' '); }
	    }
	    line.append (el);
	    el.erase();
	}
	if (line.size() > 0) { res.append (line); res.append (EOL); }
	res.push_back ('}');
    }
    return res;
}


#else /*!DEBUG*/

/* Default definitions if 'DEBUG' is unset */
#define DBGstartTag(dbgtag, elname, attrs, attrname, ...)
#define DBGendTag(dbgtag, elname, valOpt)

/* Both 'DBGmessage()' functions collapse into one macro... */
#define DBGmessage(dbgTag, ...)
#define DBGixAndOuter(dbgTag, ix, outer)

#endif /*DEBUG*/

#endif /*SP_DBG_INC*/
