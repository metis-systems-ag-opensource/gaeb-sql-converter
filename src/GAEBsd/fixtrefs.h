/* fixtrefs.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Interface part of 'fixtrefs.{cc,h}'.
** (Fix the type references for the complex types which are restrictions or
**  extensions. This step must be done at the end of the parsing of a single
**  XSD file.)
**
*/
#ifndef FIXTREFS_H
#define FIXTREFS_H

#include "pdata.h"

namespace GAEBsd {

/* Fixing the type references of all elements of the schema which are
** either an '<element>' or a '<simpleType>' or a '<complexType>'.
*/
void fixtrefs (PData &pdata);

} /*namespace GAEBsd*/

#endif /*FIXTREFS_H*/
