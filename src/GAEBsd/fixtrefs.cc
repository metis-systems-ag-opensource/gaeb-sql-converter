/* fixtrefs.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Fix the type references for the complex types which are restrictions or
** extensions. This step must be done at the end of the parsing of a single
** XSD file.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#include <iostream>
using std::cerr, std::cout, std::endl, std::flush;
#endif /*DEBUG*/

#include <stdexcept>
#include <string>

#include "Common/sutil.h"

#include "sp-utils.h"

#include "fixtrefs.h"

namespace GAEBsd {

using std::runtime_error;
using std::string;

// Fixing a single type reference (setting the 'typex' member variable of
// a given item 'it' to the index of it's basetype in the 'TypeIndex' map
// 'tindex' plus one. (The "plus one" is necessary, because the indices in
// the content of the schema start with '0', but '0' is here also the
// indicator of an unresolved type/basetype. (The 'typex' member variable
// remains unset if 'it' has no type/basetype.)
static
void fixtref (PItem &it, TypeIndex &tindex, const string &xsdprefix)
{
    // (REMEMBER (for me)! Always exclude types which are part of XML-Schema
    // and *not* the local schema definitions.
    if (it.typex == 0	// Using the fastest condition first,
    && it.basetype	// then the second fastest, and then ...
    && ! is_prefix (xsdprefix, *it.basetype)) {	// ... the slowest.
	if (auto txp = tindex.find (*it.basetype); txp != tindex.end()) {
	    it.typex = txp->second + 1;
	} else {
	    throw runtime_error
		("Failed to resolve type/basetype of " + itemName (it));
	}
    }
}

// Fixing the type references of all elements of the schema which are
// either an '<element>' or a '<simpleType>' or a '<complexType>'.
void fixtrefs (PData &pdata)
{
    auto &[items, tindex, _nc, _errors, _redefinable, _skipContent] = pdata;

    PItem &schema = items.front();
    string prefix = *schema.name + ":";
    for (auto &it: schema.content) {
	switch (it.itemtype) {
	    case PType::complexType:
	    case PType::element:
	    case PType::simpleType:
		fixtref (it, tindex, prefix);
		break;
	    default:
		// This should never occur, but for avoiding compiler warnings
		// about "not all cases exhausted" (or something like that), i
		// added this 'switch' sink.
		break;
	}
    }
}

} /*namespace GAEBsd*/
