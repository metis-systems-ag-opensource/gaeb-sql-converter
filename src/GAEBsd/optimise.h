/* optimise.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Interface part of 'optimise.{cc,h}'
** (Embed sequences into the structures containing them iff
**    - these sequences occur exactly once ('minOccurs == maxOccurs == 1'), and
**    - if the structures surrounding them are either sequences or elements.)
**
*/
#ifndef OPTIMISE_H
#define OPTIMISE_H

#include "pdata.h"

namespace GAEBsd {

void optimise (PData &pdata);

} /*namespace GAEBsd*/

#endif /*OPTIMISE_H*/
