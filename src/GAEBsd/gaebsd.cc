/* gaebsd.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Main program of the GAEB XSD-parser.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <cerrno>
#include <cctype>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <ostream>
#include <set>
#include <stdexcept>
#include <string>
#include <system_error>
#include <utility>

#include "Common/pathcat.h"
#include "Common/sutil.h"
#include "Common/sysutils.h"

#include "dbgen.h"
#include "finit.h"
#include "mdgen.h"
#include "nodis.h"
#include "optimise.h"
#include "pdata.h"
#include "readconf.h"
#include "resolve.h"
#include "xsdp.h"

namespace fs = std::filesystem;
using std::ofstream;
using std::cerr, std::cout, std::endl, std::flush;
using std::ostream;
using std::exception, std::runtime_error, std::error_code;
using std::string, std::operator""s, std::to_string;
using std::error_condition, std::make_error_condition, std::errc;
using std::system_error;
using std::pair;

#include "sp-utils.h"

using GAEBsd::NDisSet, GAEBsd::set_nodissect;

[[noreturn]] static void usage();
[[noreturn]] static void missingargs();
[[noreturn]] static void configerr (exception &e);
static void check_file (const string &file);
static NDisSet noDissects (const Config &cfg);

int main (int argc, char *argv[])
{
    string prog = appname();

    // I want 'bool' values to be written as 'true' / 'false' on 'cerr'!
    cerr.setf (std::ios::boolalpha);

    Config cfg;
    try {
	cfg = readconf ("gaebsd");
    } catch (exception &e) {
	configerr (e);
    }

    NDisSet nodissects (noDissects (cfg));

    if (argc < 2) { usage(); }

    const char *gaebsdfile = argv[1];
    const char *outfile = (argc > 2 ? argv[2] : "-");
    string dbname = (argc > 3 ? argv[3] : "");
    GAEBsd::PData pdata;

    try {
	check_file (gaebsdfile);
    } catch (system_error &e) {
	cerr << appname() << ": " << e.what() << endl;
	return 1;
    }

    GAEB::initialise();

    bool is_stdout = false;
    ofstream ofsql, ofsi;
    ostream *ofsqlp = nullptr, *ofsip = nullptr;
    string outsql, outsi;
    if (outfile == "-"s) {
	ofsqlp = ofsip = &cout; is_stdout = true;
    } else {
	if (is_suffix (".is", outfile, true)) {
	    outsi = outfile;
	    outsql = outsi.substr (0, outsi.size() - 3) + ".sql";
	} else if (is_suffix (".sql", outfile, true)) {
	    outsql = outfile;
	    outsi = outsql.substr (0, outsql.size() - 4) + ".is";
	} else  {
	    outsql = outfile + ".sql"s; outsi = outfile + ".is"s;
	}
	if (fs::exists (outsql)) {
	    cerr << appname() << ": File \"" << outsql <<
		    "\" already exists." << endl;
	    return 1;
	}
	if (fs::exists (outsi)) {
	    cerr << appname() << ": File \"" << outsi <<
		    "\" already exists." << endl;
	    return 1;
	}
	ofsql.open (outsql);
	if (! ofsql.is_open()) {
	    cerr << appname() << ": Attempt to open \"" << outsql <<
		    "\" failed." << endl;
	    return 1;
	}
	ofsqlp = &ofsql;
	ofsi.open (outsi);
	if (! ofsi.is_open()) {
	    cerr << appname() << ": Attempt to open \"" << outsi <<
		    "\" failed." << endl;
	    ofsql.close(); fs::remove (outsql);
	    return 1;
	}
	ofsip = &ofsi;
    }

    try {
	cerr << "Parsing XSD file '" << gaebsdfile << "' ..." << flush;
	pdata = GAEBsd::xsdparse (gaebsdfile);
	if (pdata.items.size() < 1) {
	    throw runtime_error
		("INTERNAL ERROR! No '<schema>' data structure generated.");
	}
	GAEBsd::PItem &schema = pdata.items.front();
	cerr << " complete. " << schema.content.size() <<
		" elements/types found." << endl;
	cerr << "Resolving elements/types ..." << flush;
	// Resolving the elements *and* getting the name of the GAEB phase,
	// version, and version date.
	string gaeb_phase, gaeb_version, gaeb_versdate;
	GAEBsd::resolve (pdata, gaeb_phase, gaeb_version, gaeb_versdate);

	// Constructing the database name (if not explicitely specified as
	// 'GAEB<something>').
	if (! dbname.empty() && ! is_prefix ("GAEB", dbname)) {
	    gaeb_phase = dbname; dbname.clear();
	}
	if (dbname.empty()) {
	    dbname = "GAEB" +
		     (gaeb_version.empty() ? "" : "_" + gaeb_version) +
		     (gaeb_versdate.empty() ? "" : "_" + gaeb_versdate) +
		     "_X" + gaeb_phase;
	}
	cerr << " complete." << endl;
	cerr << "Optimising ..." << flush;
	GAEBsd::optimise (pdata);
	cerr << " done." << endl;
	cerr << "Prohibiting the dissection of certain elements ..." << flush;
	GAEBsd::set_nodissect (pdata, nodissects);
	cerr << " done." << endl;
	cerr << "Generating DB-Schema ..." << flush;
	GAEBsd::gen_dbschema (pdata, *ofsqlp, dbname);
	cerr << " done." << endl;
	cerr << "Generating INSERT metadata ..." << flush;
	GAEBsd::gen_metadata (pdata, *ofsip, dbname);
	cerr << " done." << endl;
	if (! is_stdout) {
	    cerr << "Closing \"" << outsql << "\" and \"" << outsi << "\"." <<
		    endl;
	    ofsql.close(); ofsi.close();
	}
    } catch (exception &e) {
	cerr << "[" << appname() << "] " << e.what() << endl;
	GAEB::finalise();
	return 1;
    }

    GAEB::finalise();

    // At this point the XML parser structure is no longer present. This is
    // good, because it's no longer needed. Instead, we have the Result from
    // the parsing (in 'pdata'), which is now further processed.

    // Processing the data collected during the parsing process. This data
    // is 'pdata', the result of 'xsdparse()'.

    return 0;
}

[[noreturn]] static void usage()
{
    string prog = appname();
    cerr << "Usage: " << prog <<
	    " (GAEB-)XSD-file [outfile[.is|.sql] [dbname|phase name hint]]"
	    EOL "       " << prog << "  # (no arguments) for help" EOL EOL
	    "Arguments:" EOL
	    "  (GAEB-)XSD-file" EOL
	    "     The pathname of the XML-Schema file of a GAEB-schema. If"
	    " this schema file" EOL
	    "     refers another (GAEB-specific) schema file, this other"
	    " schema must be" EOL
	    "     placed in the same directory as '(GAEB-)XSD-file'." EOL
	    "  dbname" EOL
	    "     The name of the database which is created with the"
	    " database schema. This" EOL
	    "     name must begin with 'GAEB'." EOL
	    "  phase name hint" EOL
	    "     A phase name hint to be used if the phase name couldn't"
	    " be determined by" EOL
	    "     reading the GAEB schema file. This phase name must not"
	    " begin with 'GAEB'." EOL
	    "     If the phase name couldn't be determined by reading the"
	    " GAEB schema file," EOL
	    "     nor the phase name hint was given, the phase name part"
	    " of the constructed" EOL
	    "     database name is 'UNKNOWN'." EOL
	    "  outfile[.is|.sql]" EOL
	    "     The pathname of the file the database schema and INSERT"
	    " metadata files" EOL
	    "     are written to. A suffix '.is' or '.sql' is"
	    " automatically truncated from" EOL
	    "     this name, because two files are constructed, the"
	    " database schema (with" EOL
	    "     a suffix '.sql' appended to 'outname') and the INSERT"
	    " metadata (with a" EOL
	    "     suffix '.is'). If not given (or set as \"-\"), database"
	    " schema and INSERT" EOL
	    "     metadata are both sent to the standard output." EOL;
    cerr.flush();
    exit (0);
}

[[noreturn]] static void missingargs()
{
    cerr << appname() << ": Missing argument(s)." << endl;
    exit (64);
}

[[noreturn]] static void configerr (exception &e)
{
    cerr << appname() << ": Reading configuration failed - " <<
	    e.what() << endl;
    exit (1);
}

static void check_file (const string &file)
{
    fs::path filename (file);
    fs::file_status fstat = status (filename);
    if (! fs::exists (fstat)) {
	auto ec = make_error_code (errc::no_such_file_or_directory);
	throw system_error (ec, "'" + file + "'");
    }
    if (! is_regular_file (fstat)) {
	auto ec = make_error_code (errc::invalid_argument);
	throw system_error (ec, "'" + file + "'");
    }
}

static NDisSet noDissects (const Config &cfg)
{
    NDisSet res;
    if (cfg.has ("NODISSECT")) {
	string nodissect = cfg["NODISSECT"];
	string el;
	el.reserve (nodissect.size());
	bool get_el = false;
	for (char ch: nodissect) {
	    if (! isalnum (ch) && ch != '_' && ! isblank (ch) && ch != ',') {
		throw runtime_error ("Config(NODISSECT): Invalid value");
	    }
	    if (get_el) {
		if (isalnum (ch) || ch == '_') {
		    el.push_back (ch);
		} else {
		    if (! el.empty()) { res.insert (el); el.clear(); }
		    get_el = false;
		}
	    } else {
		if (isalnum (ch) || ch == '_') {
		    el.push_back (ch); get_el = true;
		}
	    }
	}
	if (! el.empty()) { res.insert (el); el.clear(); }
    }
    return res;
}
