/* ./src/GAEBsd/optimise.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Embed sequences into the structures containing them iff
**   - these sequences occur exactly once ('minOccurs == maxOccurs == 1'), and
**   - if the structures surrounding them are either sequences or elements.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#include <iostream>
using std::cerr, std::endl, std::flush;
#endif /*DEBUG*/

#include <set>

#include "sp-utils.h"

#include "optimise.h"

namespace GAEBsd {

class Optimiser {
public:
    Optimiser() { }
    Optimiser (const Optimiser &x) = delete;
    Optimiser (Optimiser &&x) = delete;

    void optimise (PData &pdata);

protected:
    void optimise_sequence (PItem &seq);
    void optimise_choice (PItem &choice);
    void optimise_element (PItem &elem);
    bool is_processed (unsigned item_index) {
	auto itp = processed.find (item_index);
	return itp != processed.end();
    }
    bool set_processed (unsigned item_index) {
	auto [itp, done] = processed.insert (item_index);
	return done;
    }
private:
    // For checking if a top-level element was processed yet, or not.
    std::set<unsigned> processed;
};

void optimise (PData &pdata)
{
    Optimiser opt;

    opt.optimise (pdata);
}

void Optimiser::optimise (PData &pdata)
{
    PContent &items = pdata.items;
    if (items.size() >= 1) {
	auto &schema = pdata.items.front();
	auto &cont = schema.content;
	unsigned contsz = (unsigned) cont.size();
	for (unsigned ix = 0; ix < contsz; ++ix) {
	    PItem &item = cont[ix];
	    if (item.itemtype != PType::element) { continue; }
	    if (! is_processed (ix)) {
		optimise_element (item);
		set_processed (ix);
	    }
	}
    }
}

void Optimiser::optimise_sequence (PItem &seq)
{
    PContent &content = seq.content;
    size_t contentsz = content.size();
    if (contentsz == 0) { return; }
    PContent newcont;
    newcont.reserve (contentsz * 2);
    for (PItem &item: content) {
	switch (item.itemtype) {
	    case PType::sequence: {
		optimise_sequence (item);
		if (item.min == 1 && item.max == 1) {
		    PContent &icont = item.content;
		    newcont.insert (newcont.end(), icont.begin(), icont.end());
		} else {
		    newcont.push_back (std::move (item));
		}
		break;
	    }
	    case PType::choice: {
		optimise_choice (item);
		newcont.push_back (item);
		break;
	    }
	    case PType::element: {
		optimise_element (item);
		newcont.push_back (item);
		break;
	    }
	    default:
		newcont.push_back (item);
		break;
	}
    }
//    { PContent t(newcont); newcont.swap (t); }
    newcont.shrink_to_fit();
    content = std::move (newcont);
}

void Optimiser::optimise_choice (PItem &choice)
{
    PContent &content = choice.content;
    size_t contentsz = content.size();
    if (contentsz == 0) { return; }
    for (PItem &item: content) {
	switch (item.itemtype) {
	    case PType::sequence:
		optimise_sequence (item); break;
	    case PType::choice:
		optimise_choice (item); break;
	    case PType::element:
		optimise_element (item); break;
	    default:
		break;
	}
    }
}

void Optimiser::optimise_element (PItem &elem)
{
    // NOTE! The optimisation step takes place after resolving the elements,
    // meaning that an element is either of a simple type, has no content,
    // and so doesn't need further optimisation, or that this element is of
    // a complex type and has all its sub-structures already embedded.
    PContent &content = elem.content;
    size_t contentsz = content.size();
    if (elem.simpleContent || contentsz == 0) { return; }
    PContent newcont;
    newcont.reserve (contentsz * 2);
    for (PItem &item: content) {
	switch (item.itemtype) {
	    case PType::sequence: {
		optimise_sequence (item);
		if (item.min == 1 && item.max == 1) {
		    PContent &icont = item.content;
		    newcont.insert (newcont.end(), icont.begin(), icont.end());
		} else {
		    newcont.push_back (std::move (item));
		}
		break;
	    }
	    case PType::choice: {
		optimise_choice (item);
		newcont.push_back (item);
		break;
	    }
	    case PType::element: {
		optimise_element (item);
		newcont.push_back (item);
		break;
	    }
	    default:
		newcont.push_back (item);
		break;
	}
    }
//    { PContent t(newcont); newcont.swap (t); }
    newcont.shrink_to_fit();
    content = std::move (newcont);
}

} /*namespace GAEBsd*/
