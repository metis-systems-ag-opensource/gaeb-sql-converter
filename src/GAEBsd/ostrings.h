/* ostrings.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Definition of the helper base class which generates the strings for the
** output generator ('ogen.{cc,h}').
**
*/
#ifndef OSTRINGS_H
#define OSTRINGS_H

#include <map>
#include <string>

#include "pdata.h"

namespace GAEBsd {

struct OStrings {
    OStrings() : textcont_name("_content") { }
    virtual ~OStrings() { }
    virtual std::string preamblestr (const std::string &dbname) = 0;
    virtual std::string typestr (const std::string &type,
				 const RestrList &rslist) = 0;
    virtual std::string attrstr (const std::string &name,
				 const std::string &type) = 0;
    virtual std::string tbl_prefix (const std::string &tblname,
				    const std::string &elname,
				    bool is_array) = 0;
    virtual std::string tbl_prefix (const std::string &tblname,
				    const std::string &elname,
				    bool is_array, bool nodis) = 0;
    virtual std::string itdel() = 0;
    virtual std::string add_fkconstr (
	const std::map<std::string, std::string> &fkeys
    ) = 0;
    virtual std::string tbl_suffix (const std::string &tblname,
				    bool is_array) = 0;
    virtual std::string ctbl_column (const std::string &tblname,
				     bool multivalue) = 0;
    virtual std::string etbl_column (const std::string &colname,
				     const std::string &tblname,
				     bool multivalue) = 0;
    virtual std::string stbl_column (const std::string &tblname,
				     bool multivalue) = 0;
    virtual std::string columnstr (const std::string &colname,
				   const std::string &type) = 0;
    virtual std::string textcont (const std::string &type,
				  const std::string &name) = 0;
    virtual bool need_fkconstr() const = 0;

    virtual std::string modify_table_addfkeys (
	const std::string &tblname,
	const std::map<std::string, std::string> &fkconstr
    ) = 0;

    std::string tref_colname (const std::string &tblname) const {
	return tblname + "_index";
    }
protected:
    std::string textcont_name;
};

} /*namespace GAEBsd*/

#endif /*OSTRINGS_H*/
