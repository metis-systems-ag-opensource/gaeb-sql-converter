/* pdata.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Data collected during parsing an XSD structure.
**
*/
#ifndef PDATA_H
#define PDATA_H

#include "debug.h"

#include <cstdint>

#include <map>
#include <optional>
#include <set>
#include <string>
#include <utility>
#include <variant>
#include <vector>

namespace GAEBsd {

using PAttrs = std::map<std::string, std::string>;

enum class PType : uint8_t {
    all, annotation, attribute, choice, complexContent, complexType, element,
    extension, fractionDigits, group, length, maxLength, restriction, schema,
    sequence, simpleContent, simpleType, totalDigits, mvElement, altElement,
    enumeration, xUnion
};

struct PItem;

using PContent = std::vector<PItem>;

using OptString = std::optional<std::string>;

#define NoString (OptString())

struct Restriction {
    enum Type { length, maxLength, fractionDigits, totalDigits, required,
		enumeration };
    Restriction (Type rst, size_t v);
    Restriction (const std::string &ev);
//    Restriction (const Restriction &x) = default;
//    Restriction &operator= (const Restriction &x) = default;
    Type type() const { return _type; }
    size_t value() const;
    const std::set<std::string> &enumvalues() const;
    bool addEnum (const std::string &ev);
    unsigned addEnums (const std::set<std::string> &evl);
private:
    std::variant<size_t, std::set<std::string>> _value;
    Type _type;
};

using RestrList = std::vector<Restriction>;

struct ItemAttr {
    std::string name, type, defval;
    RestrList rs;
    bool required, fixed, isRef;
};

using ItemAttrs = std::map<std::string, ItemAttr>;

enum class ModificationType:uint8_t {
    none, restriction, extension, resolved, nd_processed
};

struct PItem {
    PItem() = delete;
    PItem (PType itemtype, bool simpleContent = false);
    PItem (PType itemtype, bool simpleContent, const OptString &name,
	   const OptString &basetype = NoString);

    OptString name, basetype;
    std::string tname;
    PContent content;
    ItemAttrs attrs;
    RestrList rs;
    unsigned min, max;
    PType itemtype;
    ModificationType modtype;
    bool simpleContent, hasErrors, nodissect;
    unsigned typex;
#ifdef DEBUG
    std::string xtag;
#endif
};

/*! `TypeIndex` is used as a `name` => `item(simpleType|complexType)`
**  association, to be used for selecting a `PItem` of the type
**  `PType::simpleType` or `PType::complexType`, when referenced in an
**  `<xs:element>`. This index can be represented by a simple `std::map<>`,
**  because type names must be unique (or are overwritten when this is not
**  the case).
*/
using TypeIndex = std::map<std::string, unsigned>;

/*! PData contains a 'std::map<>' and a 'std::vector', because the order of
**  the elements must be kept intact.
*/
struct PData {
    PData()
      : items(), tindex(), nc(0), errors(0), redefinable(false),
	skipContent(false)
    { }
    ~PData() {
	tindex.clear(); tindex.clear(); nc = errors = 0;
	redefinable = skipContent = false;
    }
    PData (const PData &x)
      : items(x.items), tindex(x.tindex), nc(x.nc), errors(x.errors),
	redefinable(x.redefinable), skipContent(x.skipContent)
    { }
    /* Storing all items found in their correct order. */
    PContent items;
    /* Storing the references to type names! Only type names need to be
    ** unique (except when re-defined, but then the original definition
    ** nonetheless must be overwritten with the definition in the
    ** <xs:redefine> element).
    */
    TypeIndex tindex;
    // Helper variable for creating names for artificial elements
    unsigned nc, errors;
    bool redefinable, skipContent;
};

using Name2Type = std::map<std::string, PType>;

} /*namespace GAEBsd*/

#endif /*PDATA_H*/
