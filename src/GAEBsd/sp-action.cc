/* sp-action.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Parsing action routines for the different types of XSD-Elements
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <cstring>
#include <iostream>
#include <initializer_list>
#include <set>
#include <stdexcept>
#include <utility>

#include "sp-utils.h"

#include "sp-action.h"

namespace GAEBsd {

using std::cerr, std::cout, std::endl, std::flush;
using std::initializer_list;
using std::set;
using std::pair, std::make_pair, std::move;
using std::map;
using std::exception, std::runtime_error, std::invalid_argument,
      std::out_of_range;
using std::string, std::operator""s, std::to_string;
using std::vector;
using std::tie;

#define bottomitem (items.front())	// (items[0])

#include "sp-dbg.inc.cc"

static
void wrongContext (const PItem &inner, const PItem &outer,
		   const string &innerElName = "")
{
    cerr << "ERROR! '" << elTagName (inner, innerElName) <<
	    "' is wrong in this context ('" << elTagName (outer) <<
	    "')." << endl;
}

using ULongOpt = std::optional<unsigned long>;
using UnsignedOpt = std::optional<unsigned>;

static
ULongOpt longValAt (const string &elname,
		    const PAttrs &attrs,
		    const string &attrname)
{
    string sval = attrs.at (attrname);
    try {
	size_t xx;
	unsigned long val = stoul (sval, &xx);
	return val;
    } catch (exception &e) {
	cerr << "ERROR! Invalid value of '<" << elname << ">' attribute '" <<
		attrname << "': " << sval << endl;
	return ULongOpt();
    }
}

UnsignedOpt intValAt (const string &elname,
		      const PAttrs &attrs,
		      const string &attrname)
{
    ULongOpt tmp = longValAt (elname, attrs, attrname);
    unsigned long max = (unsigned) -1;
    if (! tmp || *tmp > max) { return UnsignedOpt(); }
    return UnsignedOpt((unsigned) *tmp);
}

/* <xs:annotation...> or <xs:annotation.../> */
static
void startAnnotation (const string &uri,
		      const string &elname,
		      const PAttrs &attrs,
		      PData &pdata)
{
    pdata.skipContent = true;
}

/* </xs:annotation> or <xs;annotation.../> */
static
void endAnnotation (const string &uri,
		    const string &elname,
		    PData &pdata)
{
    pdata.skipContent = false;
}

/* An attribute declaration is always part of either an element or a
** 'complexType' (often combined with a 'simpleContent'). It may
** contain '<xs:restriction>' declarations and '<xs:simpleType>'
** elements with restrictions (the 'simpleType''s 'base' attribute will then
** become the rype of the attribute); or it contains a reference to an
** externally defined type (using the 'type' attribute directly). Additionally,
** it may contain a 'use' attribute (which has one of the values 'optional' or
** 'required', where the former is the default. When a '<xs:simpleType>' is
** directly embedded in the attribute, this 'simpleType''s base type must
** then the attribute's type. Additional restrictions should be collected
** within the "open" attribute (an attribute declaration without a 'type'
** attribute). The
*/
static
void startAttribute (const string &uri,
		     const string &elname,
		     const PAttrs &attrs,
		     PData &pdata)
{
    // Get references to all elements of 'pdata'
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    PItem item(PType::attribute, true);

    ItemAttr attr { "", "", "", RestrList(), false, false };

    if (has (attrs, "ref")) {
	if (has (attrs, "name") || has (attrs, "type") || has (attrs, "form")) {
	    cerr << "ERROR! '<attribute ref=...>' not possible with 'name',"
		    " 'type' or 'form'" << endl <<
		    "  attributes." << endl;
	    ++errc; item.hasErrors = true;
	} else {
	    attr.type = attrs.at ("ref"); attr.isRef = true;
	}
    }
    if (has (attrs, "type")) { attr.type = attrs.at ("type"); }
    if (! has (attrs, "name")) {
	cerr << "ERROR! nameless '<attribute...>' elements are currently"
		" unsupported." << endl;
	++errc; item.hasErrors = true;
    } else {
	attr.name = attrs.at ("name");
	if (has (attrs, "default")) {
	    if (has (attrs, "fixed")) {
		cerr << "ERROR! 'fixed' and 'default' cannot be together in an"
			" '<attribute>' spec." << endl;
		++errc; item.hasErrors = true;
	    } else {
		attr.defval = attrs.at ("default");
	    }
	} else if (has (attrs, "fixed")) {
	    attr.defval = attrs.at ("fixed"); attr.fixed = true;
	}
	item.attrs[attr.name] = attr;
	item.name = attr.name;
    }
    items.push_back (item);
}

/* </xs::attribute> or <xs:attribute.../> */
static
void endAttribute (const string &uri,
		   const string &elname,
		   PData &pdata)
{
    static set<PType> valid_outer {
	PType::schema, PType::complexType, PType::restriction,
	PType::extension, PType::element
    };
    static set<PType> valid_context {
	PType::schema, PType::complexType, PType::element
    };
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    unsigned ix = lastx (items);
    PItem &attrEl = items[ix];
    PItem &outer = items[ix - 1];
    PType outerType = outer.itemtype;

    if (attrEl.hasErrors) { outer.hasErrors = true; goto POP_TOPITEM; }

    if (!(is_in (outerType, valid_context) ||
	  is_in (outerType, valid_outer))) {
	wrongContext (attrEl, outer, elname);
	outer.hasErrors = true; ++errc;
    } else {
    //    auto ait = attr.attrs.end(); --ait; auto &[attrname, attr] = *ait;
	string attrname = *attrEl.name;
	if (attrname.empty()) {
	    cerr << "ERROR! '<attribute> requires a 'name'." << endl;
	    outer.hasErrors = true; ++errc;
	} else {
	    ItemAttr &attr = attrEl.attrs[attrname];
	    if (attr.type.empty()) {
		if (! attrEl.basetype) {
		    cerr << "ERROR! Couldn't determine the type of " <<
			    elTagName (attrEl) << endl;
		    goto POP_TOPITEM;
		}
		attr.type = *attrEl.basetype;
	    }
	    if (has (outer.attrs, attrname)) {
		cerr << "ERROR! '<attribute name=\"" << attrname <<
			"\" ...>' is ambiguous" << endl <<
			" in '<" << ptype2s (outerType) << ">'." << endl;
		outer.hasErrors = true; ++errc;
	    } else {
		outer.attrs[attrname] = attr;
	    }
	}
    }
POP_TOPITEM:
    items.pop_back();
}

/* `<xs:complexContent>...</xs:complexContent>` is only some "bracket pair"
** surrounding `<xs:extension>` or `<xs:restriction>` elements. It has
** (probably) no special meaning (except as a syntactical construct).
*/
static
void startComplexContent (const string &uri,
			  const string &elname,
			  const PAttrs &attrs,
			  PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;
    PItem cplCont (PType::complexContent);

    items.push_back (cplCont);
}

static
void endComplexContent (const string &uri,
			const string &elname,
			PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    unsigned ix = lastx (items);
    PItem &cplCont = items[ix];

    PItem &outer = items[ix - 1];

    if (cplCont.hasErrors) { outer.hasErrors = true; goto POP_TOPITEM; }

    if (outer.itemtype != PType::complexType) {
	wrongContext (cplCont, outer, elname);
	outer.hasErrors = true; ++errc; goto POP_TOPITEM;
    }

    // '<complexContent>' must not have less than or more than one child
    // element.
    if (cplCont.content.size() > 1) {
	cerr << "ERROR! '<complexContent>' must contain exactly one"
		" child-element." << endl;
	outer.hasErrors = true; ++errc; goto POP_TOPITEM;
    }
    if (outer.basetype || ! outer.content.empty() || ! outer.attrs.empty()) {
	cerr << "ERROR! Surrounding '<complexType>' element must not contain"
		" more than this" << endl <<
		"'<" << elname << ">' element." << endl;
	outer.hasErrors = true; ++errc; goto POP_TOPITEM;
    }
    // Move all the relevant data to the outer element...
    outer.basetype = /*move*/ (cplCont.basetype);
    outer.content = /*move*/ (cplCont.content);
    outer.attrs = /*move*/ (cplCont.attrs);
    outer.modtype = cplCont.modtype;
    outer.simpleContent = cplCont.simpleContent;

POP_TOPITEM:
    // The item is no longer required, because either it had errors, or its
    // content was already embedded into its parent ('<complexType>') element.
    items.pop_back();
}

/* A complexType with a 'name' attribute is the external definition of an
**
** of the definition of an element's content.
** An element either has an attribute 'type' referring to such a complexType
** or a simpleType
**   <xs:complexContent>
**     <xs:restriction base="name_of_basetype/typename">
**       <xs:sequence> | <xs:choice>
**         sequence | alternative of elements or further
**         <xs:sequence> | <xs:choice>
*/
static
void startComplexType (const string &uri,
		       const string &elname,
		       const PAttrs &attrs,
		       PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    // Create a new `PItem` value for the `<xs:complexType>` element
    PItem cplType (PType::complexType);

    if (has (attrs, "name")) {
	// The `complexType` exists on its own, but any further decision
	// concerning it cannot be done until its content was collected
	// completely. Only its name can be set here.
	// NOTE! The use of the member function 'std::map<T>::at()' is
	// mandatory here, because 'std::map<T>::operator[]' does not work
	// on constant references ('const &') of this type. This is the
	// problem if a ternary operator (like '[]=' (=> 'a[b] = c')) doesn't
	// exist (is unspecifiable) in the programming language.
	cplType.name = attrs.at ("name");
    }
    if (has (attrs, "type")) {
	// The definition of the <complexType> is given somewhere else...
	cplType.basetype = attrs.at ("type");
    }
    // Open a new "context" for this `complexType`
    // Append the new item (`cplType`) to the items list of `pdata`
    items.push_back (cplType);
}

static
bool newComplexType (PContent &items, PItem &cplType,
		     unsigned ix, TypeIndex &tindex)
{
    if (cplType.name) {
	// The element 'cplType' has a name, and so must be inserted as a new
	// '<complexType>' element into the bottom element (the '<schema>'
	// element. Additionally
	PItem &schema = bottomitem;
	// "Allocate" a new position ('bx') for this '<complexType>' element.
	unsigned bx = schema.content.size();
	// Now, insert this position into the search index for the types.
	auto [bxp, done] = tindex.emplace (*cplType.name, bx);
	// It is an error if there is already a type with the same name within
	// the search index.
	if (! done) {
	    // This is an ERROR! In this case, issue a message ...
	    cerr << "ERROR! '" << *cplType.name << "' already exists." <<
		    endl;
	    // Remove defective item
	    return false;
	}
	// The insertion into the search index was successful. Now the element
	// itself must be embedded into the '<schema>' element.
	genxtag (cplType, *cplType.name);
	schema.content.push_back (cplType);
    } else {
	// The '<complexType>' element does not have a name. In this case,
	// its content must be embedded into the element directly below it
	// in the context stack, but _only_ if this "outer" element is not
	// the '<schema>' element at the bottom of the context step.
	// 'ix' is the position of this element on the parsing context stack.
	if (ix <= 1) {
	    // ERROR! A '<complexType>' element cannot be member of the
	    // bottom ('<schema>') element.
	    cerr << "ERROR! A nameless '" << elTagName (cplType) <<
		    "' cannot be embedded directly into a '<schema>'." << endl;
	    return false;
	}
	// This means that the outer element is at the position directly below
	// this element's one.
	PItem &outer = items[ix - 1];
	outer.content = /*move*/ (cplType.content);
	outer.attrs = /*move*/ (cplType.attrs);
	outer.rs = /*move*/ (cplType.rs);
	// Because a '<complexType>' element doesn't have attributes like
	// 'minOccurs' or 'maxOccurs', 'min' and 'max' are not copied.
	// 'itemtype' must _never_ be copied!
	if (cplType.modtype != ModificationType::none) {
	    outer.basetype = /*move*/ (cplType.basetype);
	    outer.modtype = cplType.modtype;
	}
	outer.hasErrors = cplType.hasErrors;
	genxtag (cplType, "anonComplexType");
    }
    // Last not least, remove the element from the 'items' list.
    return true;
}

struct vr4ct_result {
    string redeftypename, basetypename;
    PItem *redeftype, *basetype;
    bool err;
};

static
vr4ct_result validRestriction4ComplexType (PContent &items, PItem &cplType,
					   TypeIndex &tindex, bool redefinable)
{
    unsigned ix;
    if (! cplType.basetype) {
	// It is an error if there is no base type!
	cerr << "ERROR! '" << *cplType.name << "' is a restriction but has"
		" no basetype." << endl;
	return vr4ct_result { "", "", nullptr, nullptr, false };
    }
    if (! redefinable) {
	// It is also an error if this restriction of the base type
	// was not not defined within a 'redefine' context.
	cerr << "ERROR! '" << *cplType.name << "' is not defined in a"
		" redefinition context." << endl;
	return vr4ct_result { "", "", nullptr, nullptr, false };
    }
    // 'cplType' is a redefinition of some complex type. This means, that
    // the complex type to be redefined _must_ exist!
    string redeftypename = *cplType.name;
    if (! has (tindex, redeftypename)) {
	// One cannot redefine a 'complexType' if this 'complexType' were
	// not defined somewhere.
	cerr << "ERROR! '" << redeftypename << "' was nowhere defined."
	    << endl;
	return vr4ct_result { "", "", nullptr, nullptr, false };
    }
    // Define a reference (an ALIAS) of the type which must be redefined
    ix = tindex.at (redeftypename);
    PItem &redeftype = items[ix];

    // Because 'cplType' is a re-definition, its basetype _must_ already
    // be defined somewhere
    string basetypename = *cplType.basetype;
    if (! has (tindex, basetypename)) {
	// The 'base' type for the redefined type must exist, too!
	cerr << "ERROR! The basetype of '" << redeftypename <<
		"' was nowhere defined." << endl;
	return vr4ct_result { "", "", nullptr, nullptr, false };
    }
    // Define a reference (an ALIAS) of the type which is used as the
    // source of the redefinition. (This is not necessarily the same
    // type as the type to be redefined ('redeftype').
    ix = tindex.at (basetypename);
    PItem &basetype = items[ix];
    return vr4ct_result { redeftypename, basetypename, &redeftype, &basetype,
			  true };
}

static
bool complexRestrictionSimple (PItem &cplType, PItem &redeftype,
			       PItem &basetype)
{
    // What must be done?
    // 1. Getting the content restrictions from 'basetype'.
    // 2. Merging the restrictions from 'cplType' into the restrictions gotten
    //    from 'baseType'.
    // 3. Getting all attributes from 'basetype'.
    // 4. Merging all restrictions from the attributes of 'cplType' with the
    //    the restrictions of the corresponding attributes gotten from
    //    'basetype'.
    // 5. Merging all these things together to form 'redeftype'.
    // RESTRICTIONS: 'cplType' must not have attributes which are not already
    // part of 'basetype', because this would 'cplType' make an extension (and
    // not a restriction) of 'basetype'.

    // Mixing the restrictions from 'cplType' with the restrictions from
    // 'baseType', storing the result in 'redefType'
    redeftype.rs = mix_rs (basetype, cplType);

    // Getting the attribute list from 'basetype' (3)
    ItemAttrs rattrs = basetype.attrs;

    // Merging the attributes and restrictions. (4) (5)
    for (auto &[name, value]: cplType.attrs) {
	auto attrp = rattrs.find (name);
	if (attrp == rattrs.end()) {
	    // An attribute in 'cplType' which doesn't exist in 'basetype'
	    // is an ERROR! (Not yet handling this)
	    continue;
	}
	//rsx.clear();
	attrp->second = value;
    }
//    redeftype.typex = 0;

    return true;
}

static
bool complexRestrictionComplex (PItem &cplType, PItem &redeftype,
				PItem &basetype)
{
    // In a restriction of complex content of a complex type, all sub-elements
    // are (as far as i know) issued a second time (together with their
    // restrictions).
    if (basetype.name != redeftype.name) {
	cerr << "ERROR! Redefinition with a base type other than the type to"
		" be redefined" << endl <<
		" is currently not supported." << endl;
	return false;
    }
    // I'm currently not checking structural equivalence between 'redeftype'
    // and 'cplType', only replacing the list of the elements.
    if (! cplType.content.empty()) {
	redeftype.content = /*move*/ (cplType.content);
    }

    // Same for the attributes ...
    if  (! cplType.attrs.empty()) {
	redeftype.attrs = /*move*/ (cplType.attrs);
    }

    // ... and the restrictions.
    if (! cplType.rs.empty()) {
	redeftype.rs = cplType.rs;
    }
    return true;
}

static
void endComplexType (const string &uri,
		     const string &elname,
		     PData &pdata)
{
    static set<PType> valid_outer { PType::element, PType::schema };

    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    unsigned ix = lastx (items);
    PItem &cplType = items[ix];
    PItem &outer = items[ix - 1];
    PType outerType = outer.itemtype;

    if (! is_in (outerType, valid_outer)) {
	wrongContext (cplType, outer, elname);
	// Increase the error count and remove the defective item, before
	// returning.
	++errc; items.pop_back();
	return;
    }

    // Three cases:
    switch (cplType.modtype) {
	case ModificationType::none: {
	    // 1. the type is a new one. This should be the standard in most
	    //    cases.
	    if (! newComplexType (items, cplType, ix, tindex)) {
		// Increase the error count because of the defective element.
		++errc;
	    }
	    break;
	}
	case ModificationType::restriction: {
	    auto &schema = bottomitem;
	    // The index in 'tindex' refers an entry in the schema's item list,
	    // not 'items', because 'items' is only a parsing stack, whereas
	    // the schema's item list consists of the types collected so far.
	    auto [redeftypename, basetypename, predeftype, pbasetype, ok] =
		validRestriction4ComplexType
		    (schema.content, cplType, tindex, redefinable);

	    if (! ok) {
		++errc;
	    } else {
		// For a complex type with simple content, only the existing
		// restrictions of the content and of the attributes must be
		// transferred. But because the basetype of the redefinition
		// needs not be the same as the type being redefined, the
		// restriction list and attribute map must both be copied,
		// modified, and then be transferred to the type being
		// redefined. Copy the restrictions on the content type.
		// Currently, the only way to specify restrictions on a complex
		// type is to repeat all elements and attribute and adding or
		// replacing elements which occur (or don't occur) in the
		// redefined type.
//		if (cplType.simpleContent) {
//		    ok = complexRestrictionSimple (cplType, *predeftype, *pbasetype);
//		} else {
//		    ok = complexRestrictionComplex (cplType, *predeftype, *pbasetype);
//		}
		ok = (cplType.simpleContent ? complexRestrictionSimple
					    : complexRestrictionComplex)
			 (cplType, *predeftype, *pbasetype);
		if (! ok) { ++errc; }
		predeftype->basetype.reset();
		// Remove the top item, because its content was already
		// embedded in the redefined type.
	    }
	    break;
	}
	case ModificationType::extension: {
	    // A '<complexType>' containing an '<extension>' defines a new type
	    // which shares some declarations with an existing one, and adds
	    // some new ones. Thus, and because the referenced type is not
	    // known yet, it must be treated like a new type. This also means
	    // that an existing type with the same name should not be
	    // overwritten.
	    if (! newComplexType (items, cplType, ix, tindex)) {
		++errc;
	    }
	    break;
	}
	default:
	    // Not used here, because this value is solely used in the
	    // resolver ...
	    break;
    }
//POP_ITEM:
    items.pop_back();
}


/* 'startElement()' pushes a new 'PItem' value on 'items'. This value is either
** removed later, because it is a replacement of another - already existing
** element, or
** description of a <xs:simpleType> or <xs:complexType>, or contains a
** <xs:simpleType> of <xs:complexType> itself.
*/
static
void startElement (const string &uri,
		   const string &elname,
		   const PAttrs &attrs,
		   PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;
    const string &name = attrs.at ("name");
    PItem el (PType::element, false, name);

    // Remembering a reference to the element is important, because it may
    // contain sub-elements, and filling it requires knowledge of the position
    // of its data.
    // Element NOT yet inserted, because it must be filled with content first.
    genxtag (el, name);
    if (has (attrs, "type")) {
	const string &tname = attrs.at ("type");
	el.basetype = tname; el.typex = 0;
	if (auto txp = tindex.find (tname); txp != tindex.end()) {
	    el.typex = txp->second + 1;
	}
    }
    UnsignedOpt valOpt;
    if (has (attrs, "minOccurs")) {
	valOpt = intValAt (elname, attrs, "minOccurs");
	if (valOpt) { el.min = *valOpt; } else { el.hasErrors = true; ++errc; }
    }
    if (has (attrs, "maxOccurs")) {
	auto &av = attrs.at ("maxOccurs");
	valOpt = (av == "unbounded" ? (unsigned) -1
				  : intValAt (elname, attrs, "maxOccurs"));
	if (valOpt) { el.max = *valOpt; } else { el.hasErrors = true; ++errc; }
    }

    // Now, that the new '<xs:element>'-structure is filled as far as possible,
    // it can be inserted into my result structure.
    items.push_back (el);
}

/* Helper finction for generating a unique name ... */
static
string gen_name (PData &pdata, string own_name = "")
{
    static set<PType> valid_elements {
	PType::complexType, PType::simpleType, PType::element
    };

    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    // Search in the outer elements for an element which has a name and then
    // construct the name of this sub-structure from this name
    if (! own_name.empty()) {
	return own_name + "_" + to_string (nc++);
    }
    for (unsigned ix = lastx (items); ix > 0; --ix) {
	auto &item = items[ix];
	if (item.name && is_in (item.itemtype, valid_elements)) {
	    string &npfx = *item.name;
	    return npfx + "_" + to_string (nc++);
	}
    }
    // Default name if nothing matching could be found.
    return "_UNKNOWN__" + to_string (nc++);
}


static
void endElement (const string &uri,
		 const string &elname,
		 PData &pdata)
{
    static set<PType> valid_outer {
	PType::choice, PType::group, PType::schema, PType::sequence,
    };
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    unsigned ix = lastx (items);
    PItem &el = items[ix];
    PItem &outer = items[ix - 1];
    PType outerType = outer.itemtype;

    if (! is_in (outerType, valid_outer)) {
	wrongContext (el, outer, elname);
	++errc;
    } else {
	if (outerType == PType::schema && ! el.name) {
	    // Empty '<element>' name in a '<schema>' context. This is an
	    // error!
	    cerr << "ERROR! '" << elTagName (el) <<
		    "' requires a name in the '" << elTagName (outer) <<
		    "' context." << endl;
	    outer.hasErrors = true; ++errc;
	} else {
	    // Always embed the '<element>' into the surrounding '<schema>',
	    // '<choice>', ...
//	    if (! el.basetype) { el.basetype = gen_name (pdata, "anontype"); }
	    outer.content.push_back (el);
	}
    }
    items.pop_back();
}

/* '<xs:extension base="T">...</xs:extension>' is simply some kind of bracket
** around a number of '<xs:element>' / <xs:attribute>' elements, which extend
** the given basetype 'T'. This means that, short of extracting the 'base'
** attribute, nothing needs to be done here.
*/
static
void startExtension (const string &uri,
		     const string &elname,
		     const PAttrs &attrs,
		     PData &pdata)
{
    // Decomposing `pdata`
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    // All needed here is the base type of a type/element which is extended
    // by '<xs:extension>'. The sub-elements '<xs:extension>' contains are
    // handled by the corresponding '<xs:element>' / '<xs:attribute>'
    // XSD-elements
    const string &basetype = attrs.at ("base");
    PItem ext(PType::extension, false, NoString, basetype);

    ext.typex = 0;
    if (auto txp = tindex.find (basetype); txp != tindex.end()) {
	ext.typex = txp->second + 1;
    }

    items.push_back (ext);
}

static
void endExtension (const string &uri,
		   const string &elname,
		   PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    // Pop the '<xs:extension>' element from 'items'
    unsigned ix = lastx (items);
    PItem &ext = items[ix];

    // and move its content into the surreounding '<xs:simpleContent>' or
    // '<xs:complexContent>' element.
    PItem &outer = items[ix - 1];
    if (outer.itemtype == PType::simpleContent) {
	if (! ext.content.empty()) {
	    cerr << "ERROR! complex content extension within simple content"
		    " complex type." << endl;
	    ++errc; goto POP_TOPITEM;
	}
	outer.simpleContent = true;
    } else {
	if (outer.itemtype != PType::complexContent) {
	    wrongContext (ext, outer, elname);
	    ++errc; goto POP_TOPITEM;
	}
	outer.content = /*move*/ (ext.content);
	outer.simpleContent = false;
    }
    outer.basetype = /*move*/ (ext.basetype);
    outer.attrs = /*move*/ (ext.attrs);
    outer.modtype = ModificationType::extension;
    outer.typex = ext.typex;

POP_TOPITEM:
    items.pop_back();
}


static
bool setRestr (RestrList &restrList, Restriction &newRestr)
{
    static set<Restriction::Type> supported {
	Restriction::length, Restriction::maxLength,
	Restriction::fractionDigits, Restriction::totalDigits,
	Restriction::enumeration
    };

    Restriction::Type newRestrType = newRestr.type();
    if (is_in (newRestrType, supported)) {

	for (auto it = restrList.begin(); it != restrList.end();) {
	    if (newRestr.type() == it->type()) {
		it = restrList.erase (it);
	    } else {
		++it;
	    }
	}
	restrList.push_back (newRestr);
    }
    return true;
}

/* startSizeRestr() handles all restrictions which contain a 'value' attribute
** concerning a length/size of the attribute/content it is defined for.
** These types of restrictions can be used for database declarations, so they
** are processed here.
*/
static
void startSizeRestr (const string &uri,
		     const string &elname,
		     const PAttrs &attrs,
		     PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    std::map<std::string, Restriction::Type> supported {
	{ "length", Restriction::length },
	{ "maxLength", Restriction::maxLength },
	{ "fractionDigits", Restriction::fractionDigits },
	{ "totalDigits", Restriction::totalDigits }
    };

    PItem restr(PType::restriction, true);

    if (has (supported, elname)) {
	auto valOpt = intValAt (elname, attrs, "value");
	if (valOpt) {
	    Restriction::Type rsType = supported.at (elname);
	    restr.rs.push_back (Restriction (rsType, *valOpt));
	} else {
	    restr.hasErrors = true; ++errc;
	}
    }
    // 'restr' must always pushed on the stack, because 'endSizeRestr()'
    // always pops it ...
    items.push_back (restr);
}

static
void endSizeRestr (const string &uri,
			  const string &elname,
			  PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;
    unsigned ix = lastx (items);
    PItem &restr = items[ix];
    PItem &outer = items[ix - 1];

    if (restr.hasErrors) {
	outer.hasErrors = true;
    } else {
	if (outer.itemtype != PType::restriction) {
	    wrongContext (restr, outer, elname);
	    ++errc;
	} else {
	    for (auto rs: restr.rs) { setRestr (outer.rs, rs); }
	}
    }

    items.pop_back();
}

/* Like '<xs:extension base="T"> ... </xs:extension>', '<xs:restriction
** base="T"> ... </xs:restriction>' is merely a ptype of brackets, only that
** it surrounds the restrictions defined for an '<xs:element>' or a
** '<xs:simpleType>', so no action function for '</xs:endRestriction> is
** required, and only the 'base' attribute is extracted.
*/
static
void startRestriction (const string &uri,
		       const string &elname,
		       const PAttrs &attrs,
		       PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;
    string basetype = attrs.at ("base");

    PItem restr(PType::restriction, false, NoString, basetype);

    restr.typex = 0;
    if (auto txp = tindex.find (basetype); txp != tindex.end()) {
	restr.typex = txp->second + 1;
    }

    items.push_back (restr);
}

static
bool validRestrCont (const set<PType> &valids,
		     const PItem &restr,
		     const PItem &outer)
{
    size_t restrContSize = restr.content.size();
    if (restrContSize > 1) {
	cerr << "ERROR! '<" << elTagName (restr) <<
		">' contains more than one sub-element." << endl;
	return false;
    }
    if (restrContSize > 0) {
	unsigned ix = lastx (restr.content);
	const PItem &restrCont = restr.content[ix];
	if (! is_in (restrCont.itemtype, valids)) {
	    cerr << "ERROR! '<" << elTagName (restrCont) <<
		    ">' is invalid in a '<" << elTagName (restr) <<
		    ">' if the latter is part of a '<" << elTagName (outer) <<
		    ">' element." << endl;
	    return false;
	}
    }
    return true;
}

static
void endRestriction (const string &uri,
		     const string &elname,
		     PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    unsigned ix = lastx (items);
    PItem &restr = items[ix];

    PItem &outer = items[ix - 1];

    if (restr.hasErrors) {
	outer.hasErrors = true; goto POP_TOPITEM;
    }

    if (outer.basetype || ! outer.content.empty()
    || ! outer.attrs.empty() || ! outer.rs.empty()) {
	cerr << "ERROR! '" << elTagName (outer) << "' surrounding '" <<
		elTagName (restr, elname) << "' isn't empty!" << endl;
	++errc; outer.hasErrors = true; goto POP_TOPITEM;
    }
    switch (outer.itemtype) {
	case PType::simpleType: {
	    set<PType> valid_content { PType::simpleType };
	    if (! validRestrCont (valid_content, restr, outer)) {
		++errc; outer.hasErrors = true; break;
	    }
	    outer.basetype = /*move*/ (restr.basetype);
	    outer.content = /*move*/ (restr.content);
	    outer.rs = /*move*/ (restr.rs);
	    outer.modtype = ModificationType::restriction;
	    outer.typex = restr.typex;
	    break;
	}
	case PType::simpleContent: {
	    set<PType> valid_content { PType::simpleType };
	    // restr.content may have one element at most, and this element
	    // must be of '<simpleType>'.
	    if (! validRestrCont (valid_content, restr, outer)) {
		++errc; outer.hasErrors = true; break;
	    }
	    outer.basetype = /*move*/ (restr.basetype);
	    outer.content = /*move*/ (restr.content);
	    outer.attrs = /*move*/ (restr.attrs);
	    outer.rs = /*move*/ (restr.rs);
	    outer.modtype = ModificationType::restriction;
	    outer.typex = restr.typex;
	    break;
	}
	case PType::complexContent: {
	    set<PType> valid_content {
		PType::group, PType::all, PType::choice, PType::sequence
	    };
	    // restr.content may have one element at most, and this element
	    // must be one of '<group>', '<all>', '<choice>' or '<sequence>'.
	    if (! validRestrCont (valid_content, restr, outer)) {
		++errc; outer.hasErrors = true; break;
	    }
	    // Also, restrictions make no sense, so they are not copied along.
	    outer.basetype = /*move*/ (restr.basetype);
	    outer.content = /*move*/ (restr.content);
	    outer.attrs = /*move*/ (restr.attrs);
	    outer.modtype = ModificationType::restriction;
	    outer.typex = restr.typex;
	    break;
	}
	default: {
	    wrongContext (restr, outer, elname); ++errc;
	    break;
	}
    }

POP_TOPITEM:
    items.pop_back();
}

static
void startEnumeration (const string &uri,
		       const string &elname,
		       const PAttrs &attrs,
		       PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    PItem enumr(PType::enumeration, false);

    if (! has (attrs, "value")) {
	cerr << "ERROR! '<" << elname << ">' requires a 'value' attribute." <<
		endl;
	++errc;
    } else {
	Restriction rs (attrs.at ("value"));
	enumr.rs.push_back (rs);
    }
    items.push_back (enumr);
}

// Helper function for inserting a list of '<enumeration>' values into
// another enumeration.
static
bool insert_enum (Restriction &ors, const Restriction &irs)
{
    unsigned ec = 0;
    const set<string> &evset = irs.enumvalues();
    for (const string &ev: evset) {
	if (! ors.addEnum (ev)) {
	    cerr << "ERROR! <enumeration value=\"" << ev <<
		    "\"/> occurs more than once." << endl;
	    ++ec;
	}
    }
    return ec == 0;
}

// Helper function for inserting a list of '<enumeration>' values into
// a <restriction>'s restriction list.
static
bool insert_enum (RestrList &orsl, const Restriction &irs)
{
    bool found = false, doubles = false;
    for (Restriction &ors: orsl) {
	if (ors.type() == Restriction::enumeration) {
	    if (! insert_enum (ors, irs)) { doubles = true; }
	    found = true;
	    break;
	}
    }
    if (! found) { orsl.push_back (irs); }
    return ! doubles;
}

// The '<enumeration>' element is empty in normal cases, but operations on
// it should not be performed until is was completely scanned, meaning: until
// the handler function for the end tag of this element was found.
static
void endEnumeration (const string &uri,
		     const string &elname,
		     PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    PItem &enumr = items.back();
    if (enumr.rs.empty()) {
	// No value error from 'startEnumeration()'
	items.pop_back();
    } else {
	unsigned ix = lastx (items);
	PItem &outer = items[ix - 1];
	// The outer element MUST be a '<restriction>' element!
	if (outer.itemtype != PType::restriction) {
	    cerr << "ERROR! '<enumeration>' element not in a <restriction>"
		    " context" << endl;
	    ++errc;
	} else {
	    Restriction &irs = enumr.rs.front();
	    if (! insert_enum (outer.rs, irs)) { ++errc; }
	}
    }
    items.pop_back();
}

static
void startGroupLike (const string &uri,
		     const string &elname,
		     const PAttrs &attrs,
		     PData &pdata)
{
    static Name2Type supported {
	{ "choice", PType::choice },
	{ "sequence", PType::sequence },
    };
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    if (! has (supported, elname)) {
	cerr << "ERROR! '<" << elname << ">' unsupported by this function." <<
		endl;
	++errc; return;
    }

    UnsignedOpt intOpt;
    PType elType = supported.at (elname);
    PItem groupLike(elType);

    if (has (attrs, "minOccurs")) {
	intOpt = intValAt (elname, attrs, "minOccurs");
	if (! intOpt) { ++errc; groupLike.hasErrors = true; }
	else { groupLike.min = (unsigned) *intOpt; }
    }
    if (has (attrs, "maxOccurs")) {
	const string &av = attrs.at ("maxOccurs");
	intOpt = (av == "unbounded" ? (unsigned) -1
				    : intValAt (elname, attrs, "maxOccurs"));
	if (! intOpt) { ++errc; groupLike.hasErrors = true; }
	else { groupLike.max = (unsigned) *intOpt; }
    }
    groupLike.name = gen_name (pdata, elname);
    genxtag (groupLike, (elType == PType::choice ? "choice" : "sequence"));

    items.push_back (groupLike);
}

static
void endGroupLike (const string &uri,
		   const string &elname,
		   PData &pdata)
{
    static Name2Type supported {
	{ "choice", PType::choice },
	{ "sequence", PType::sequence },
    };
    static set<PType> valid_context {
	PType::group, PType::choice, PType::sequence, PType::complexType,
	PType::restriction, PType::extension, PType::element
    };

    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    if (! has (supported, elname)) {
	cerr << "ERROR! '<" << elname << ">' unsupported by this function." <<
		endl;
	++errc; items.pop_back(); return;
    }

    unsigned ix = lastx (items);
    PItem &groupLike = items[ix];

    // Throwing away empty groupLike ... just don't know any other way (yet).
    if (! groupLike.content.empty()) {
	PItem &outer = items[ix - 1];
	PType outerType = outer.itemtype;

	if (is_in (outerType, valid_context)) {
	    outer.content.push_back (groupLike);
	} else {
	    wrongContext (groupLike, outer, elname);
	    ++errc;
	}
    }

    items.pop_back();
}

static
void startSimpleContent (const string &uri,
			 const string &elname,
			 const PAttrs &attrs,
			 PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;
    PItem simCont(PType::simpleContent, true);

    if (has (attrs, "base")) {
	simCont.basetype = attrs.at ("base");
    }

    items.push_back (simCont);
}

static
void endSimpleContent (const string &uri,
		       const string &elname,
		       PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    unsigned ix = lastx (items);
    PItem &simCont = items[ix];
    PItem &outer = items[ix - 1];
    if (simCont.hasErrors) { outer.hasErrors = true; goto POP_TOPITEM; }
    if (outer.itemtype != PType::complexType) {
	wrongContext (simCont, outer, elname);
	outer.hasErrors = true; ++errc; goto POP_TOPITEM;
    }
    if (outer.basetype || ! outer.content.empty() || ! outer.attrs.empty()) {
	cerr << "ERROR! Surrounding '<complexType>' element must not contain"
		" more than this" << endl <<
		"'<simpleContent>' element." << endl;
	outer.hasErrors = true; ++errc; goto POP_TOPITEM;
    }
    // Move all the relevant data to the outer element...
    outer.basetype = /*move*/ (simCont.basetype);
    outer.content = /*move*/ (simCont.content);
    outer.attrs = /*move*/ (simCont.attrs);
    outer.modtype = simCont.modtype;
    outer.simpleContent = simCont.simpleContent;

POP_TOPITEM:
    items.pop_back();
}

static
void startSimpleType (const string &uri,
		      const string &elname,
		      const PAttrs &attrs,
		      PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    OptString nameOpt (has (attrs, "name") ? OptString (attrs.at ("name"))
					    : NoString);
    PItem simType(PType::simpleType, true, nameOpt);

    genxtag (simType, (nameOpt ? *nameOpt : "anonSimpleType"));
    items.push_back (simType);
}

static
bool operator!= (const OptString &l, const OptString &r)
{
    if (! l || ! r) { return true; }
    return *l != *r;
}

static
void endSimpleType (const string &uri,
		    const string &elname,
		    PData &pdata)
{
    static set<PType> valid_outer {
	PType::attribute, PType::element, PType::restriction, PType::schema
    };
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    // Index to the last 'PItem' entry in 'items[]'.
    unsigned ix = lastx (items);
    PItem &simType = items[ix];

    PItem &outer = items[ix - 1];
    PType outerType = outer.itemtype;

    if (simType.hasErrors) {
	outer.hasErrors = true; ++errc; goto POP_TOPITEM;
    }

    // Checking if my parent is my "real" parent!
    if (! is_in (outerType, valid_outer)) {
	wrongContext (simType, outer, elname);
	outer.hasErrors = true; ++errc; goto POP_TOPITEM;
    }

    // '<simpleType>' is a direct child of either '<schema>' or
    // '<redefine>'
    if (! simType.name) {
	// I have no name, and so must be embedded in the element next to
	// my position on the stack.
	if (outerType == PType::schema) {
	    cerr << "ERROR! '<simType>' requires a name when '<" <<
		    elTagName (outer) << ">' is its parent." << endl;
	    outer.hasErrors = true; ++errc; goto POP_TOPITEM;
	}
	if (! outer.content.empty()) {
	    cerr << "ERROR! '<" << ptype2s (outerType) <<
		    ">' already has content." << endl;
	    outer.hasErrors = true; ++errc; goto POP_TOPITEM;
	}
	if (! outer.basetype) { outer.basetype = simType.basetype; }
	outer.rs = mix_rs (simType, outer);
    } else {
	// The '<simpleType>' must be embedded in the top-level element
	//
	string name = *simType.name;
	PItem &schema = bottomitem;
	unsigned bx = schema.content.size();
	auto [bxp, done] = tindex.emplace (name, bx);
	if (done) {
	    // The '<simType name="...">' element is a new element!
	    schema.content.push_back (simType);
	} else {
	    // The '<simType name="...">' element already exists. If it is
	    // not a restriction, it _must_ _not_ replace an existing item
	    if (!(redefinable ||
		  simType.modtype == ModificationType::restriction)) {
		cerr << "ERROR! '" << elTagName (simType) <<
			"' cannot replace an already existing element." <<
			endl;
		schema.hasErrors = true; ++errc;
	    } else {
		// The simple type is a redefinition with additional
		// restrictions.
		bx = bxp->second;
		PItem &baseType = schema.content[bx];
		if (! baseType.simpleContent) {
		    // For a simple type restriction to be valid, the base
		    // type must itself be a simple type.
		    cerr << "ERROR! '" << elTagName (baseType) <<
			    "' cannot be redefined with simple content." <<
			    endl;
		    schema.hasErrors = true; ++errc;
		} else if (simType.name != baseType.name) {
		    cerr << "ERROR! The base type name of '" <<
			    elTagName (simType) <<
			    "' doesn't match the redefinition's name." << endl;
		    schema.hasErrors = true; ++errc;
		} else {
		    // A simple type has neither content nor attribute
		    // definitions. But is has a base type and restrictions.
		    // It is easier here to replace the relevant content of the
		    // base type instead to rewrite the existing base type
		    // element with a complete new one.
//		    baseType.content = /*move*/ (simType.content);
//		    for (auto &[n, a]: simType.attrs) {
//			baseType.attrs[n] = /*move*/ (a);
//		    }
		    // Transfer the restrictions from 'simType' to 'baseType'.
		    for (auto &rs: simType.rs) {
			setRestr (baseType.rs, rs);
		    }
		}
	    }
	}
    }
POP_TOPITEM:
    items.pop_back();
}

static
void startUnion (const string &uri,
		 const string &elname,
		 const PAttrs &attrs,
		 PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    PItem xUnion(PType::xUnion, true, NoString, attrs.at ("memberTypes"));

    items.push_back (xUnion);
}

static
void endUnion (const string &uri,
		const string &elname,
		PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    unsigned ix = lastx (items);
    PItem &xUnion = items[ix];
    PItem &schema = items[0];
    PItem &outer = items[ix - 1];

    if (outer.itemtype != PType::simpleType) {
	wrongContext (xUnion, outer, elname);
	outer.hasErrors = true; ++errc; goto POP_TOPITEM;
    }

    // Because a union can be anything "simple" i'm reducing it here to
    // the 'xs:string' type
    outer.basetype = *schema.name + ":string";

    // and set the maximum length to 255 (avoiding the creation of a 'TEXT'
    // database attribute. (This is something like a 'tgNormalizesString255'.)
    outer.rs.push_back (Restriction (Restriction::maxLength, 255));

POP_TOPITEM:
    items.pop_back();
}

static
void startSchema (const string &uri,
		  const string &elname,
		  const PAttrs &attrs,
		  PData &pdata)
{
    auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    // These attributes are explicitely set before 'startAction()' (and thus
    // this routine) is invoked, so they can be accessed without a prior
    // check on their existance...
    string prefix = attrs.at ("schemaPrefix");
    string sUri = attrs.at ("schemaURI");

    // The "bottom" element should not created if the context stack is not
    // empty, because then, the (only) on this context stack is already a
    // '<schema>' from an outer '<schema>' (loaded as a part of a
    // redefinition).
    if (items.empty()) {
	PItem schema(PType::schema, false,
		     (prefix.empty() ? NoString : prefix),
		     (sUri.empty() ? NoString : sUri));
	items.push_back (schema);
    } else {
	// There is already an element on the stack. This element _must_ be the
	// only element, and it must be a '<schema>' element.
	unsigned ix = lastx (items);
	if (ix != 0 || items[ix].itemtype != PType::schema) {
	    cerr << "ERROR! '<" << elTagName (items[ix], elname) <<
		    ">' must be the top-level element." << endl;
	    items[ix].hasErrors = true;
	    ++errc;
	}
	PItem &schema = items[ix];
	if (! schema.name && ! prefix.empty()) { schema.name = prefix; }
	if (! schema.basetype && ! sUri.empty()) { schema.basetype = sUri; }
    }
}

static
void endSchema (const string &uri,
		const string &elname,
		PData &pdata)
{
    //auto &[items, tindex, nc, errc, redefinable, sc] = pdata;

    // Nothing to be done here (currently!)
}


/* Typedefs for the function types in the 'struct ActionTab' declaration */
typedef void (*StartAction) (const string &uri,
			     const string &elname,
			     const PAttrs &attrs,
			     PData &pdata);

typedef void (*EndAction) (const string &uri,
			   const string &elname,
			   PData &pdata);

struct ActionTab { const char *name; StartAction sact; EndAction eact;
		   bool forceEndAction; };

/* The action selector tab. I inserted all elements found in the GAEB-XSD
** files, even if most of the entries are (currently) not used. The reason
** is the fact that i currently don't know if these entries are required or
** not.
*/
static ActionTab xsd_element_actions[] = {
    { "annotation", startAnnotation, endAnnotation, true },
    { "attribute", startAttribute, endAttribute, false },
    { "choice", startGroupLike, endGroupLike, false },
    { "complexContent", startComplexContent, endComplexContent, false },
    { "complexType", startComplexType, endComplexType, false },
//    { "documentation", startDocumentation, endDocumentation, false },
    { "element", startElement, endElement, false },
    { "enumeration", startEnumeration, endEnumeration, false },
    { "extension", startExtension, endExtension, false },
    { "fractionDigits", startSizeRestr, endSizeRestr /*nullptr*/, false },
    { "length", startSizeRestr, endSizeRestr /*nullptr*/, false },
//    { "maxExclusive", startMaxExclusive, endMaxExclusive, false },
//    { "maxInclusive", startMaxInclusive, endMaxInclusive, false },
    { "maxLength", startSizeRestr, endSizeRestr /*nullptr*/, false },

//    { "minExclusive", startMinExclusive, endMinExclusive, false },
//    { "minInclusive", startMinInclusive, endMinInclusive, false },
//    { "pattern", startPattern, endPattern, false },
//    { "redefine", startRedefine, endRedefine, false },
    { "restriction", startRestriction, endRestriction, false },
    { "schema", startSchema, endSchema, false },
    { "sequence", startGroupLike, endGroupLike, false },
    { "simpleContent", startSimpleContent, endSimpleContent, false },
    { "simpleType", startSimpleType, endSimpleType, false },
    { "totalDigits", startSizeRestr, endSizeRestr /*nullptr*/, false },
    { "union", startUnion, endUnion, false },
    { nullptr, 0, 0, false }
};

/* Small helper function for determining an action from the XSD-element's
** name.
*/
static ActionTab *actionentry (const char *element)
{
    ActionTab *ae = nullptr;
    for (unsigned ix = 0; (ae = &xsd_element_actions[ix], ae->name); ++ix) {
	if (strcmp (ae->name, element) == 0) { return ae; }
    }
    return nullptr;
}

/* This function is invoked from the 'GAEBsd::startElement()' function in
** 'schemaparser.cc'
*/
void startAction (const string &uri,
		  const string &elname,
		  const PAttrs &attrs,
		  PData &pdata)
{
    ActionTab *ae = actionentry (elname.c_str());
    if (ae && ! pdata.skipContent) {
	ae->sact (uri, elname, attrs, pdata);
    } else {
//	cerr << "Ignoring element '" + elname + "'" << endl;
    }
}

/* This function is invoked from the 'GAEBsd::endElement()' function in
** 'schemaparser.cc'
*/
void endAction (const string &uri,
		const string &elname,
		PData &pdata)
{
    ActionTab *ae = actionentry (elname.c_str());
    if (ae && ae->eact && (! pdata.skipContent || ae->forceEndAction)) {
	ae->eact (uri, elname, pdata);
    }
}

void process_textcont (PData &pdata, const std::string &text)
{
    // NEVER process the text content if 'skipContent' (of 'pdata') is set.
    if (! pdata.skipContent) {
	// Processing text only if there is something to process ...
    }
}

} /*namespace GAEBsd*/
