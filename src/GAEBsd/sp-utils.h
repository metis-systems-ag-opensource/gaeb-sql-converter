/* sp-utils.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Interface part of 'sp-utils.{cc,h}'
** (Some utility functions ...)
**
*/
#ifndef SP_UTILS_H
#define SP_UTILS_H

#include <map>
#include <stdexcept>
#include <set>
#include <string>
#include <vector>

#include "pdata.h"

namespace GAEBsd {

// has (map, key) is far more clear than 'map.count (key) > 0', so i'm
// using this template function in most places instead.
//
template<typename V, typename K>
static bool has (const std::map<K, V> &m, K k)
{
    return m.count (k) > 0;
}

// This is a specialisation, because an automatic conversion from
// 'const char *' to string would never work with the template function
// above.
//
template<typename V>
static bool has (const std::map<std::string, V> &m, const char *k)
{
    return m.count (std::string (k)) > 0;
}

// The same (as explained above) holds for 'std::set<T>', so matching template
// function(s) are introduced for this, too.
template<typename T>
static bool has (const std::set<T> &s, const T &k)
{
    return s.count (k) > 0;
}

// The second variant is sometimes nearer to a natural spoken sentence, so
// i introduced this one, too. (I'm missing free operator declarations in C++,
// sigh...)
template<typename T>
static bool is_in (const T &k, const std::set<T> &s)
{
    return s.count (k) > 0;
}

// Returns the index of the last element of a 'std::vector<T>'. Because only
// the index (an 'unsigned int') is returned, no two versions of this template
// function are required.
template<typename T>
static inline unsigned lastx (const std::vector<T> &v)
{
    size_t vsize = v.size();
    if (vsize == 0) {
	throw std::out_of_range
	    ("lastx(): Can't return the index of an empty array.");
    }
    return vsize - 1;
}

// Returns the XSD element name for a given PItem type tag (of type PType).
// Because of the names being constants, a 'const char *' is returned instead
// of a 'std::string'.
const char *ptype2s (PType ptype);

std::string elTagName (const PItem &item,
		       const std::string &elname = "",
		       bool endtag = false);

std::string itemName (const PItem &item, bool with_addr = false);
std::string itemBasetype (const PItem &item);
std::string attr2s (const ItemAttr &attr);
std::string attrList (const ItemAttrs &attrs);
std::string itemAttrs (const PItem &item);
const char *rstype2s (Restriction::Type rst);
std::string itemRsList (const PItem &item);

std::ostream &iw_reset (std::ostream &s);
void iw_back (unsigned w);
std::ostream &indent (std::ostream &s, const std::string tag, int w = 0);

RestrList mix_rs (const RestrList &other, const RestrList &preferred);
RestrList mix_rs (const PItem &other, const PItem &preferred);

std::string DumpAddr (const void *addr);
std::string DumpItem (const PItem &it,
		      std::string indent = "",
		      bool with_addrs = false);
std::string DumpAttr (const ItemAttr &a);
std::string DumpStrMap (const std::map<std::string, std::string> &map);
std::string DumpStrSet (const std::set<std::string> &set);

} /*namespace GAEBsd*/

#endif /*SP_UTILS_H*/
