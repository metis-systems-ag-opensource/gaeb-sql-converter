/* xsdp.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Interface part of 'xsdp.{cc,h}'
** (Hiding the ugly details (Xerces-C) of the schema parser from the
**  main program.)
**
*/
#ifndef XSDP_H
#define XSDP_H

#include "pdata.h"

namespace GAEBsd {

PData xsdparse (const char *filename);

} /*namespace GAEBsd*/

#endif /*XSDP_H*/
