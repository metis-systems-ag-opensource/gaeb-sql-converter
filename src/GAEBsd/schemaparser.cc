/* schemaparser.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Parse a GAEB-Schema and create a tree/dag structure from it. This parser
** is necessary for constructing the database (SQL) schema required for
** creating a database suitable for storing GAEB data.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <iostream>
#include <map>
#include <optional>
#include <stdexcept>
#include <string>

#include <xercesc/sax2/Attributes.hpp>
#include <xercesc/sax/SAXParseException.hpp>
#include <xercesc/sax/SAXException.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>

#include <xercesc/util/OutOfMemoryException.hpp>

#include "Common/fnutil.h"
#include "Common/pathcat.h"
#include "Common/sutil.h"
#include "Common/sysutils.h"

#include "sp-action.h"
#include "tostdstring.h"

#include "sp-intern.h"
#include "sp-utils.h"
#include "fixtrefs.h"

#include "schemaparser.h"

namespace std {
template<typename T> T* allocate (size_t n = 1)
{
    if (n == 0) { return nullptr; }
    if (n == 1) { return new T; }
    return new T[n];
}
} /*namespace std*/

#ifdef REALPRIV
# define ref(x) (*(x))
# define priv_set(x, y) (*(x) = *(y))
# define priv_init(f) (generate_GAEBsdImpl ((f)))
# define priv_move(v) (v)
# define priv_resetptr(p) ((p) = nullptr)
#else
# define ref(x) (x)
# define priv_set(x, y) ((x) = (y))
# define priv_init(f) (init_GAEBsdImpl ((f)))
# define priv_move(v) (std::move (v))
# define priv_resetptr(p)
#endif

namespace GAEBsd {

// Sizes for pre-allocating some internal values (of `std::string` or
// `std::vector<...>` type). The reason is the avoidance of too many
// memory (re-)allocations; this helps to reduce heap fragmentation
// and can speed up the code using the corresponding (member) variables.
// With some experience, these values can be defined in a manner which
// reduces the number of memory reallocation to (near) 0.
//
#define PDATA_ITEMS_RESERVE 1024	// PData::items
#define PDATA_CONTEXT_RESERVE 64	// PData::context
#define IMPL_TEXTVAL_RESERVE 4096	// GAEBsdImpl::textval

using std::cerr, std::endl, std::flush;
using std::map;
using std::optional;
using std::exception, std::runtime_error;
using std::string, std::operator""s, std::to_string;

GAEBsd::GAEBsd (const char *filename)
  : impl(priv_init (filename))
{ }

GAEBsd::GAEBsd (const GAEBsd &x)
  : impl (priv_init (nullptr))
{
    priv_set (impl, x.impl);
}

GAEBsd::GAEBsd (GAEBsd &&x)
    : impl(priv_move (x.impl))
{
    priv_resetptr (x.impl);
}

GAEBsd::~GAEBsd()
{
    destroy_GAEBsdImpl (impl);
}

GAEBsd &GAEBsd::operator= (const GAEBsd &x)
{
#ifdef REALPRIV
    if (! impl) { impl = generate_GAEBsdImpl (nullptr); }
#endif
    priv_set (impl, x.impl);
    return *this;
}

GAEBsd &GAEBsd::operator= (GAEBsd &&x)
{
    impl = priv_move (x.impl);
    priv_resetptr (x.impl);
    return *this;
}

static __attribute__((unused))
string dump_startTag (const string &tag,
			     const XMLCh *const uri,
			     const XMLCh *const localname,
			     const XMLCh *const qualname,
			     const Attributes &attrs)
{
    string res (tag);
#if 1
    res.append (": " + to_string (qualname));
    const char *nv[] = { "name", "value" };
    bool first = true;
    if (attrs.getLength() > 0) {
	res.append (" (");
	for (const char *an: nv) {
	    XMLCh *x_an = XMLString::transcode (an);
	    const XMLCh *x_av = attrs.getValue (x_an);
	    if (x_av && XMLString::stringLen (x_av) > 0) {
		if (first) { first = false; } else { res.append ("|"); }
		res.append (string (an) + ":" + to_string (x_av));
	    }
	    //if (x_av) { XMLString::release (&x_av); }
	    XMLString::release (&x_an);
	}
	for (XMLSize_t ix = 0; ix < attrs.getLength(); ++ix) {
	    string name = to_string (attrs.getLocalName (ix));
	    if (name == "name" || name == "value") { continue; }
	    string value = to_string (attrs.getValue (ix));
	    if (first) { first = false; } else { res.append ("|"); }
	    res.append (name + ":" + value);
	}
	res.append (")");
    }
    res.append (EOL);
#else
    res.append (": " + to_string (qualname) +
		" (localname = " + to_string (localname) +
		", uri = " + to_string (uri) +
		")" EOL);
    for (XMLSize_t ix = 0; ix < attrs.getLength(); ++ix) {
	res.append ("    " + to_string (ix + 1) +
		    ": " + to_string (attrs.getQName (ix)) +
		    " (localname = " + to_string (attrs.getLocalName (ix)) +
		    ", type = " + to_string (attrs.getType (ix)) +
		    ", value = " + to_string (attrs.getValue (ix)) +
		    ")" EOL);
    }
#endif
    return res;
}



static SAX2XMLReader *gen_parser ()
{
    SAX2XMLReader *parser = XMLReaderFactory::createXMLReader();

    parser->setFeature (XMLUni::fgSAX2CoreNameSpaces, true);
    parser->setFeature (XMLUni::fgXercesSchema, true);
    parser->setFeature (XMLUni::fgXercesHandleMultipleImports, true);
    parser->setFeature (XMLUni::fgXercesSchema, false);
    parser->setFeature (XMLUni::fgXercesSchemaFullChecking, false);
    parser->setFeature (XMLUni::fgXercesIdentityConstraintChecking, true);
    parser->setFeature (XMLUni::fgSAX2CoreNameSpacePrefixes, false);

    parser->setFeature (XMLUni::fgSAX2CoreValidation, false);

    return parser;
}

PData doparse (const char *filename)
{
    auto *parser = gen_parser();
    GAEBsd xsdp (filename);
    parser->setContentHandler (&xsdp);
    parser->setErrorHandler (&xsdp);

    try {
	parser->parse (filename);
    } catch (const OutOfMemoryException &e) {
	delete parser;
	throw runtime_error ("Out of memory exception caught.");
    } catch (const XMLException &e) {
	delete parser;
	string msg = "Parse error in '"s + filename + "': " +
		     to_string (e.getMessage());
	throw runtime_error (msg);
    } catch (...) {
	throw runtime_error ("Caught a problem with '"s + filename + "'");
    }
    PData pdata = /*move*/ (xsdp.data());
    pdata.errors += xsdp.errCount();
    bool is_fatal = xsdp.fatalError();
    delete parser;
    if (is_fatal) {
	throw runtime_error
	    ("FATAL ERROR! There is some problem with '"s + filename + "'.");
    }
    fixtrefs (pdata);
    return pdata;
}

template<class KT, class VT> static bool has (map<KT, VT> m, KT k)
{
    return m.count (k) > 0;
}

void GAEBsd::startElement (const XMLCh *const uri,
			   const XMLCh *const localname,
			   const XMLCh *const qualname,
			   const Attributes &attrs)
{
    string elname = to_string (localname), fqname = to_string (qualname);
    if (! ref(impl).textval.empty()) {
	// Some preceding text content. This should be procesed by the
	// action routine for text in 'sp-action.{cc,h}' before the
	// action routine associated with 'elname' ...
	process_textcont (ref(impl).pdata, ref(impl).textval);
	ref(impl).textval.clear();
    }

    // The XSD-element 'xs:redefine' must be processed specially, because
    // it means that an external schema file must be loaded into the current
    // schema.
    if (fqname == "xs:redefine") {
	// Need a recursive invocation here (meaning: Creating a new `GAEBsd`
	// instance which then loads the XSD referenced by the `schemalocation`
	// attribute. Currently, only local files can be used here!
	// Getting the (value of the) 'schemaLocation' attribute.
	XMLCh *tan = XMLString::transcode ("schemaLocation");
	string oschema = to_string (attrs.getValue (tan));

	// Because the "outer" schema's ('oschema') name is now a
	// 'std::string', the XMLString value is no longer needed.
	XMLString::release (&tan);
	// If the 'oschema' name is a relative pathname (...), the
	// corresponding schema is searched in (below) the same directory as
	// the schema currently processed. Otherwise, 'oschema' must point
	// directly to an existing XSD file.
	// WARNING! This mechanism DOESN'T recognize complete URIs, only
	// pathnames.
	if (! is_abspath (oschema) && ! ref(impl).filepath.empty()) {
	    oschema = (ref(impl).filepath) / oschema;
	}

	// Parsing the "included" external schema
	PData xpdata = doparse (oschema.c_str());

	// Inserting the data retrieved during the parsing of the external
	// schema ('oschema') into the current schema data (impl->pdata)
	PData &pdata = ref(impl).pdata;
	// Assuming that the bottom element of the stack has an element
	// ('<schema>') the data from the external schema definition must
	// be moved to this '<schema>' element.
	PItem &xschema = xpdata.items.front();
	PItem &schema = pdata.items.front();
	schema.name = xschema.name;
	schema.basetype = xschema.basetype;
//	schema.content.clear();
//	schema.content.reserve (xschema.content.size() + 128);
	for (auto &el: xschema.content) {
	    schema.content.push_back (el);
	}
	xschema.content.clear();
	for (auto &[n, a]: xschema.attrs) {
	    schema.attrs.insert (make_pair (n, a));
	}
	schema.rs.clear();
	schema.min = schema.max = 1;
	schema.modtype = ModificationType::none;
	schema.simpleContent = false;
	schema.hasErrors = xschema.hasErrors;
	pdata.tindex.clear();
	for (auto &[n, ix]: xpdata.tindex) {
	    xpdata.tindex.insert (make_pair (n, ix));
	}
	pdata.nc = xpdata.nc;
	pdata.errors = xpdata.errors;
	pdata.redefinable = true;
	pdata.skipContent = false;
	pdata.tindex = /*move*/ (xpdata.tindex);
	pdata.redefinable = true;
    } else {
	// Any other element is handled by the function 'startAction()' of the
	// module 'sp-action.{cc,h}'. Because of the handling of the Xerces
	// data structure is unnecessary complex (compared to the C++ standard
	// library classes), this routine gets only parameters which are
	// compatible to the latter one. This means, that 'uri' and 'attrs'
	// must be converted before the invocation of 'startAction()' takes
	// place.
	string uristr = to_string (uri);
	PAttrs pattrs;
	for (XMLSize_t ix = 0; ix < attrs.getLength(); ++ix) {
	    pattrs.emplace (to_string (attrs.getLocalName (ix)),
			    to_string (attrs.getValue (ix)));
	}
	if (elname == "schema") {
	    pattrs.emplace ("schemaPrefix", ref(impl).schema_prefix);
	    pattrs.emplace ("schemaURI", ref(impl).schema_uri);
	    pattrs.emplace ("GAEBPrefix", ref(impl).gaeb_prefix);
	    pattrs.emplace ("GAEBURI", ref(impl).gaeb_uri);
	}

	// 'startAction()' internally splits the actione to be processed
	startAction (uristr, elname, pattrs, ref(impl).pdata);
    }
}

static __attribute__((unused))
string dump_endTag (const string &tag,
			   const XMLCh *const uri,
			   const XMLCh *const localname,
			   const XMLCh *const qualname)
{
    string res (tag);
    res.append (": " + to_string (qualname) +
		" (localname = " + to_string (localname) +
		", uri = " + to_string (uri) +
		")" EOL);
    return res;
}

void GAEBsd::endElement (const XMLCh *const uri,
			const XMLCh *const localname,
			const XMLCh *const qualname)
{
    string elname (to_string (localname));
    if (elname == "redefine") {
	// Here ends the '<xs:redefine>' element, which means that all
	// following definitions must not be further overwritten
	ref(impl).pdata.redefinable = false;
    }
    if (! ref(impl).textval.empty()) {
	// Some preceding text content. This should be procesed by the
	// action routine for text in 'sp-action.{cc,h}' before the
	// action routine associated with 'elname' ...
	process_textcont (ref(impl).pdata, ref(impl).textval);
	ref(impl).textval.clear();
    }
    endAction (to_string (uri), to_string (localname), ref(impl).pdata);
}

void GAEBsd::characters (const XMLCh *text, XMLSize_t textlen)
{
    ref(impl).textval += to_string (text, textlen);
}

void GAEBsd::ignorableWhitespace (const XMLCh* const chars,
				  const XMLSize_t length)
{
}

void GAEBsd::startDocument()
{
    reset();
}

void GAEBsd::startPrefixMapping (const XMLCh *const _prefix,
				 const XMLCh *const _uri)
{
    string prefix = to_string (_prefix);
    string uri = to_string (_uri);
    if (is_prefix ("http://www.w3.org", uri) && is_suffix ("/XMLSchema", uri)) {
	ref(impl).schema_prefix = prefix;
	ref(impl).schema_uri = uri;
    } else if (is_prefix ("http://www.gaeb.de/GAEB_DA_XML/", uri)) {
	ref(impl).gaeb_prefix = prefix;
	ref(impl).gaeb_uri = uri;
    }
}

void GAEBsd::error (const SAXParseException &e)
{
    ++ref(impl).errcount;
    cerr << endl << "In " << to_string (e.getSystemId()) <<
	    "(" << e.getLineNumber() << ":" << e.getColumnNumber() << "): " <<
	    to_string (e.getMessage()) << endl;
}

void GAEBsd::fatalError (const SAXParseException &e)
{
    ++ref(impl).errcount;
    ref(impl).fatal = true;
    cerr << endl << "In " << to_string (e.getSystemId()) <<
	    "(" << e.getLineNumber() << ":" << e.getColumnNumber() <<
	    "): FATAL ERROR! " << to_string (e.getMessage()) << endl;
}

void GAEBsd::warning (const SAXParseException &e)
{
    cerr << endl << "In " << to_string (e.getSystemId()) << "(" <<
	    e.getLineNumber() << ":" << e.getColumnNumber() <<
	    "): WARNING! " << to_string (e.getMessage()) << endl;
}

void GAEBsd::resetErrors()
{
    ref(impl).errcount = 0;
}

void GAEBsd::reset()
{
    ref(impl).textval.clear(); ref(impl).textval.shrink_to_fit();
    ref(impl).textval.reserve (IMPL_TEXTVAL_RESERVE);
    PData &pdata = ref(impl).pdata;
    pdata.items.clear(); pdata.items.shrink_to_fit();
    pdata.items.reserve (PDATA_ITEMS_RESERVE);
    ref(impl).errcount = 0;
}

void GAEBsd::filepath (const char *p)
{
    ref(impl).filepath = (p ? p : ".");
}

PData &GAEBsd::data()
{
    return ref(impl).pdata;
}

unsigned GAEBsd::errCount() const
{
    return ref(impl).errcount;
}

bool GAEBsd::fatalError() const
{
    return ref(impl).fatal;
}

} /*namespace GAEBsd*/
