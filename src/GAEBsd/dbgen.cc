/* dbgen.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Database schema output generator
**
*/

#include <map>
#include <stdexcept>

#include "Common/flipflop.h"
#include "Common/sutil.h"

#include "ogen.h"

#include "dbgen.h"

namespace GAEBsd {

using std::ostream;
using std::map;
using std::runtime_error;
using std::string, std::to_string;

struct DBStrings : OStrings {
    string preamblestr (const string &dbname)
    {
	return "CREATE DATABASE IF NOT EXISTS `" + dbname + "`;" EOL EOL
	       "USE `" + dbname + "`;" EOL EOL
	       "CREATE TABLE `_GAEBfile` (" EOL
	       "  `gaebid` INTEGER NOT NULL AUTO_INCREMENT," EOL
	       "  `gaeb_version` VARCHAR(10) NOT NULL," EOL
	       "  `gaeb_versdate` DATE NOT NULL," EOL
	       "  `gaeb_phase` VARCHAR(30) NOT NULL," EOL
	       "  `date` DATE NOT NULL," EOL
	       "  `time` TIME NOT NULL," EOL
	       "  `prjname` VARCHAR(255)," EOL
	       "  `prjlabel` VARCHAR(255)," EOL
	       "  `uid` VARCHAR(128)," EOL
	       "  PRIMARY KEY(`gaebid`)" EOL
	       ");" EOL EOL
	       "CREATE INDEX `_GAEBfile_version` ON"
	       " `_GAEBfile`(`gaeb_version`);" EOL
	       "CREATE INDEX `_GAEBfile_uid` ON `_GAEBfile`(`uid`);" EOL EOL;
    }

    string typestr (const std::string &type, const RestrList &rslist)
    {
	if (type == "integer") { return "INTEGER"; }
	if (type == "positiveInteger") { return "INTEGER"; }
	if (type == "nonNegativeInteger") { return "INTEGER"; }
	if (type == "date") { return "DATE"; }
	if (type == "time") { return "TIME"; }
	//if (type == "datetime") { return "DATETIME"; }
	//##maybe: if (type == "gYearMonth") { return "DATE"; }
	if (type == "gYearMonth") { return "CHAR(7)"; }
	if (type == "string") {
	    size_t len = 0, maxlen = 0;
	    for (auto &rs: rslist) {
		switch (rs.type()) {
		    case Restriction::length:
			if (len == 0 || rs.value() < len) { len = rs.value(); }
			break;
		    case Restriction::maxLength:
			if (maxlen == 0 || rs.value() < maxlen) {
			    maxlen = rs.value();
			}
			break;
		    default: break;
		}
	    }
	    if (len > 0) {
		return "CHAR(" + to_string (len) + ")";
	    } else if (maxlen > 0) {
		if (maxlen < 256) {
		    return "VARCHAR(" + to_string (maxlen) + ")";
		} else {
		    return "TEXT";
		}
	    } else { // len == 0 && maxlen == 0!
		return "TEXT";
	    }
	}
	if (type == "normalizedString") {
	    size_t maxlen = 0;
	    for (auto &rs: rslist) {
		switch (rs.type()) {
		    case Restriction::maxLength:
			if (maxlen == 0 || rs.value() < maxlen) {
			    maxlen = rs.value();
			}
			break;
		    default: break;
		}
	    }
	    if (maxlen == 0) { maxlen = 255; }
	    return "VARCHAR(" + to_string (maxlen) + ")";
	}
	if (type == "decimal") {
	    string res = "DECIMAL(";
	    size_t total = 0, fraction = 0;
	    for (auto &rs: rslist) {
		switch (rs.type()) {
		    case Restriction::totalDigits:
			if (total == 0 || total > rs.value()) {
			    total = rs.value();
			}
			break;
		    case Restriction::fractionDigits:
			if (fraction == 0 || fraction > rs.value()) {
			    fraction = rs.value();
			}
			break;
		    default: break;
		}
	    }
	    if (total == 0) { total = 10; }
	    res += to_string (total);
	    if (fraction > 0) { res += "," + to_string (fraction); }
	    res += ")";
	    return res;
	}
	//if (type == "real") { return "DOUBLE"; }
	if (type == "token") { return "VARCHAR(255)"; }
	if (type == "ID" || type == "IDREF") { return "VARCHAR(128)"; }
	if (type == "NCName") { return "VARCHAR(128)"; }
	throw runtime_error
	    ("INTERNAL ERROR! Unknown schema type '" + type + "'");
    }

    string attrstr (const string &name, const string &type)
    {
	return "`a_" + name + "` " + type;
    }

    string tbl_prefix (const string &tblname, const string &elname,
		       bool is_array, bool nodis)
    {
	string res = "CREATE TABLE `" + tblname +
		     "` (`gaebid` INTEGER NOT NULL, `id` INTEGER NOT NULL";
	if (is_array) { res += ", `ix` INTEGER NOT NULL"; }
	res.shrink_to_fit();
	return res;
    }

    string tbl_prefix (const string &tblname, const string &elname,
		       bool is_array)
    {
	return tbl_prefix (tblname, elname, is_array, false);
    }

    string itdel() { return ", "; }

    string add_fkconstr (const map<string, string> &fkconstr)
    {
	string res;
	res.reserve (1024);
	for (auto &[fk, tbl]: fkconstr) {
	    res += ", FOREIGN KEY(`gaebid`, `" + fk + "`) REFERENCES `" + tbl +
		   "`(`gaebid`, `id`)";
	}
	return res;
    }

    string tbl_suffix (const string &tblname, bool is_array)
    {
	string res = ", PRIMARY KEY (`gaebid`, `id`";
	if (is_array) { res += ", ix"; }
	res += "));";
	if (is_array) {
	    res += " CREATE INDEX `" + tblname + "_arrx` ON `" + tblname +
		   "`(`gaebid`, `id`);";
	}
	res.shrink_to_fit();
	return res;
    }

    string ctbl_column (const string &tblname, bool multivalue)
    {
	return "`" + tref_colname (tblname) + "` INTEGER";
    }

    string etbl_column (const string &colname, const string &tblname,
			bool multivalue)
    {
	return "`" + tref_colname (colname) + "` INTEGER";
    }

    string stbl_column (const string &tblname, bool multivalue)
    {
	return "`" + tref_colname (tblname) + "` INTEGER";
    }

    string columnstr (const string &colname, const string &type)
    {
	return "`" + colname + "` " + type;
    }

    string textcont (const string &type, const string &name = "")
    {
	string colname = (name.empty() ? textcont_name : name);
	return "`" + colname + "` " + type;
    }

    bool need_fkconstr() const { return true; }

    string modify_table_addfkeys (const string &tblname,
				  const map<string, string> &fkconstr)
    {
	string res;
	res.reserve (4096);
	if (! fkconstr.empty()) {
	    flipflop<false> needcomma;
	    res = "ALTER TABLE `" + tblname + "`";
	    for (auto &[col, ttbl]: fkconstr) {
		if (needcomma) { res += ","; }
		res += " ADD FOREIGN KEY(`gaebid`, `" + col +
		       "`) REFERENCES `" + ttbl + "`(`gaebid`, `id`)";
	    }
	    res += ";";
	}
//	{ string t(res); res.swap (t); }
	return res;
    }
};

void gen_dbschema (PData &pdata, ostream &outfile, const string &dbname)
{
    DBStrings ostr;
    gen_output (ostr, pdata, outfile, dbname);
}

} /*namespace GAEBsd*/
