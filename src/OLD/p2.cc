/* p2.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
**
** Second test version of an XML parser reading GAEB.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <iostream>
#include <string>

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>
//#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

#include "u16to8.h"

using std::cerr, std::cout, std::endl, std::flush;
using namespace xercesc;

int main (int argc, char *argv[])
{
    try {
	XMLPlatformUtils::Initialize();
    } catch (const XMLException &e) {
	cerr << "Error during the initialisation: " << e.getMessage() << endl;
	return 1;
    }

    cout << "Successfully initialised the Xerces-C++ code ..." << endl;

    auto parser = new XercesDOMParser;
    parser->setValidationScheme (XercesDOMParser::Val_Always);
    parser->setValidationSchemaFullChecking (true);
    parser->setDoNamespaces (true);
    parser->setDoSchema (true);
    parser->setLoadSchema (true);
    parser->setLoadExternalDTD (true);

    auto errHandler = (ErrorHandler *) new HandlerBase;
    parser->setErrorHandler (errHandler);

    const char *xmlFile = "gaeb-t1.x81";

    try {
	parser->parse (xmlFile);
	XMLCh *sl = parser->getExternalSchemaLocation();
	cout << "XML file parsed; schema location: " << sl << endl;
	XMLCh *nnssl = parser->getExternalNoNamespaceSchemaLocation();
	cout << "XML file parsed; no namespace schema location: " << nnssl <<
		endl;
    } catch (const XMLException &e) {
	cerr << "Caught an 'XMLException': " << e.getMessage() << endl;
	return -1;
    } catch (const DOMException &e) {
	cerr << "Caught a 'DOMException': " << e.getMessage() << endl;
	return -1;
    } catch (...) {
	cerr << "Something unexpected happened!" << endl;
	return -1;
    }

    cout << "Successfully parsed '" << xmlFile << "' ..." << endl;

    delete errHandler;
    delete parser;

    XMLPlatformUtils::Terminate();
    return 0;
}
