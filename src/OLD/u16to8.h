/* u16to8.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
**
** Interface part of the 'u16to8' module.
** (Some helper functions which (UNCONDITIONALLY!) convert std::u16string
**  values to std::string (by using the standard algorithm on encoding UNICODE
**  code points (UTF-16 or UTF-32) to UTF-8).
**
*/
#ifndef U16TO8_H
#define U16TO8_H

#include <iostream>
#include <string>

//! Converts a UTF-16 string into a UTF-8 string.
std::string u8 (const std::u16string &u16s);

//! Writes a UTF-16 string directly to a 'std::ostream' (aka
//! 'std::basic_ostream<char>').
std::ostream &operator<< (std::ostream &o, const std::u16string &s);

//! Converts a UTF-32 string into a UTF-8 string.
std::string u8 (const std::u32string &u32s);

//! Writes a UTF-32 string directly to a 'std::ostream' (aka
//! 'std::basic_ostream<char>').
std::ostream &operator<< (std::ostream &o, const std::u32string &s);

#endif /*U16TO8_H*/
