/* p4.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
**
** Test program for the modules `getversion.{cc,h}`, `versionhandler.{cc,h}`,
** and `tostdstring.{cc,h}`.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <iostream>
#include <stdexcept>
#include <string>
#include <tuple>
#include <utility>
#include <xercesc/util/PlatformUtils.hpp>

#include "progname.h"
#include "finit.h"
#include "getinfo.h"

using std::cerr, std::cout, std::endl, std::flush;
using std::exception;
using std::string, std::operator""s, std::to_string;
using std::tie;
//using namespace xercesc;

int main (int argc, char *argv[])
{
    progname prog (*argv);

    if (argc < 2) {
	cout << "Usage: " << (string) prog << " GAEB-file" << endl;
	return 0;
    }

    const char *filename = argv[1];

    GAEB::initialise();

    GAEB::Info vtinfo;
    try {
	vtinfo = GAEB::getinfo (filename);
	cout << "The version is:      " << vtinfo.version << endl;
	cout << "The version date is: " << vtinfo.versiondate << endl;
	cout << "The type is:         " << vtinfo.type << endl;
    } catch (exception &e) {
	cerr << "Errors found:" << endl << e.what() << endl;
	GAEB::finalise();
	return 1;
    }
    GAEB::finalise();
    return 0;
}
