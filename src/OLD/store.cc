/* store.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
**
** Convert a GAEB file into a DOM structure and then store this DOM structure
** in a relational database.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <string>

#include "Common/config.h"

namespace gaeb {

using std::string;

void store_file (const Config &cfg, const string &gaeb_file)
{

    // Construct the pathnames of the required XSD files.
    string xsdlib = cfg["XSDBASE"] / version / "lib.xsd";
    string xsdpath = cfg["XSDBASE"] / version / type + ".xsd";

    // Pathname of the XSD cache (not yet clear) for the W3C files.
    string xsdcache = cfg.has ("XSDCACHE") ? cfg["XSDCACHE"] : "";

    /* Prepare the DOM parser */
    /*...*/

    /* Parse the file into a DOM structure */
    try {
	/*...*/
    } catch (...) {
	/*...*/
    }

    /* Insert the DOM structure into the database. */
    DOMtoDB (cfg, /*...*/);

}
