/* p5.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
**
** Test program for the modules `getversion.{cc,h}`, `versionhandler.{cc,h}`,
** and `tostdstring.{cc,h}`, and also `readconf.{cc,h}` and the library
** 'Common/libcommon.a.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <iostream>
#include <stdexcept>
#include <string>
#include <utility>

#include "Common/pathcat.h"
#include "Common/sysutils.h"
#include "finit.h"
#include "readconf.h"
#include "getinfo.h"
#include "parse.h"

using std::cerr, std::cout, std::endl, std::flush;
using std::exception;
using std::string, std::operator""s, std::to_string;
using std::pair;

int main (int argc, char *argv[])
{
    if (argc < 2) {
	cout << "Usage: " << appname() << " GAEB-file" << endl;
	return 0;
    }

    Config cfg;
    try {
	cfg = readconf ("gaeb-reader");
    } catch (exception &e) {
	cerr << appname() << ": Reading configuration failed - " <<
		e.what() << endl;
	return 1;
    }

    const char *filename = argv[1];

    /* Initialise all parts of the 'GAEB Reader' which require initialisation
    ** – such as the 'Xerces-C' library.
    */
    GAEB::initialise();

    GAEB::Info vtinfo;
    try {
	vtinfo = GAEB::getinfo (filename);
	cout << "The version is:      " << vtinfo.version << endl;
	cout << "The version date is: " << vtinfo.versiondate << endl;
	cout << "The type is:         " << vtinfo.type << endl;
    } catch (exception &e) {
	cerr << appname() << ": FATAL ERROR! " << e.what() << endl;
	GAEB::finalise();
	return 1;
    }

    GAEB::parse_file (cfg, vtinfo, filename);

    GAEB::finalise();

    return 0;
}
