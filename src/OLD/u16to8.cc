/* u16to8.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
**
** Some helper functions which (UNCONDITIONALLY!) convert std::u16string values
** to std::string (by using the standard algorithm on encoding UNICODE code
** points to UTF-8).
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <cstdint>
#include <stdexcept>

#include "u16to8.h"

using std::ostream;
using std::runtime_error;
using std::string, std::u16string, std::u32string, std::wstring;

using UCP = uint32_t;

// This function is essential for the conversion. It converts a single UTF-32
// code point into a UTF-8 code sequence (a 'std::string')and returns this
// code sequence.
static string utf8encode (char32_t uc)
{
    char buf[7], *bp;
    if (uc < 0 || uc > 0x10FFFF || (uc >= 0xD800 && uc < 0xDFFF)) {
	uc = 0xFFFD;
    }
    bp = buf;
    if (uc <= 0x7F) {
	*bp++ = (char) uc;
    } else if (uc <= 0x1FF) {
	*bp++ = (char) ((uc >> 6) | 0xC0);
	*bp++ = (char) ((uc & 0x3F) | 0x80);
    } else if (uc <= 0xFFFF) {
	*bp++ = (char) ((uc >> 12) | 0xE0);
	*bp++ = (char) (((uc >> 6) & 0x3F) | 0x80);
	*bp++ = (char) ((uc & 0x3F) | 0x80);
    } else /*if (uc <= 0x10FFFF)*/ {
	*bp++ = (char) ((uc >> 18) | 0xF0);
	*bp++ = (char) (((uc >> 12) & 0x3F) | 0x80);
	*bp++ = (char) (((uc >> 6) & 0x3F) | 0x80);
	*bp++ = (char) ((uc & 0x3F) | 0x80);
    }
    *bp = '\0';
    return buf;
}

// Helper function which tests if a given UTF-16 code is a high or low
// surrogate.
static bool is_surrogate (char16_t u16c)
{
    return (u16c - 0xD800u) < 2048u;
}

// Helper function which tests if a given UTF-16 code is a high surrogate.
static bool is_high_surrogate (char16_t u16c)
{
    return (u16c & 0xFFFFFC00) == 0xD800;
}

// Helper function which tests if a given UTF-16 code is a low surrogate.
static bool is_low_surrogate (char16_t u16c)
{
    return (u16c & 0xFFFFFC00) == 0xDC00;
}

// Helper function which enforces an unsigned conversion of a UTF-16 code into
// a UTF-32 value.
static inline char32_t u32 (char16_t u16c)
{
    return (char32_t) u16c & 0xFFFFFF;
}

// Helper function which converts a given <high-surrogate, low-surrogate>-pair
// into the corresponding UTF-32 code point.
static char32_t surrogate_to_utf32 (char16_t high, char16_t low)
{
    return (u32(high) << 10) + u32(low) - 0x35FDC00;
}

// Helper functions which returns the length, a UTF-8 code sequence
// corresponding to a given UTF-16 code will have.
static inline size_t utf8length (char16_t u16c)
{
    UCP uc = (UCP) u16c & 0xFFFF;
    return (uc <= 0x7F ? 1 :
		// 7 bits (ASCII code)
	    uc <= 0x1FF ? 2 :
		// 9 bits
	    (uc < 0xD800 || uc > 0xDFFF) ? 3 :
		// It is assumed here that surrogate codes always lead to
		// UTF-32 code points > 0xFFFF, so the result must be
		// the maximum size an unicode character can have. Any error
		// checking is somewhere else.
	    4);
}

// This function is used for an exact calculation of the size of the string
// which results from a conversion from UTF-16 to UTF-8.
// This step guarantees a minimum memory consumption, which may be unimportant
// for programs with a low memory overhead, but is essential for programs which
// use much of the available RAM.
static inline size_t calculate_utf8size (const u16string &u16s)
{
    size_t u16sz = u16s.size(), ix, u8sz = 0;
    for (ix = 0; ix < u16sz; ++ix) {
	char16_t u16c = u16s[ix];
	if (! is_surrogate (u16c)) {
	    u8sz += utf8length (u16c);
	} else {
	    if (is_high_surrogate (u16c)
	    &&  ix + 1 < u16sz && is_low_surrogate (u16s[ix + 1])) {
		u8sz += utf8length (u16c); ++ix;
	    } else {
		u8sz += utf8length (u16c);
		//alt: throw runtime_error ("Invalid UTF-16 string!");
	    }
	}
    }
    return u8sz;
}

// Convert a UTF-16 string into a UTF-8 string (ignoring locales, because
// these are unnecessary here (UTF-16 doesn't decide between locales and the
// resulting UTF-8 string will be always correct (independently of any locale).
string u8 (const u16string &u16s)
{
    string res;
    size_t u16sz = u16s.size(), ix;

    size_t u8sz = calculate_utf8size (u16s);

    // Allocating enough memory for the result. Because it is unclear if the
    // additional memory for the ending '\0' byte is allocated, the size of
    // the memory allocated is 'u8sz + 1'.
    res.reserve (u8sz + 1);

    // Now, the string 'res' is filled with short strings consisting of the
    // UTF-8 code sequences which correspond to the UTF-16 codes.
    for (ix = 0; ix < u16sz; ++ix) {
	char16_t u16c = u16s[ix];
	if (! is_surrogate (u16c)) {
	    res += utf8encode (u32 (u16c));
	} else if (is_high_surrogate (u16c) && ix + 1 < u16sz &&
		   is_low_surrogate (u16s[ix + 1])) {
	    res += utf8encode (surrogate_to_utf32 (u16c, u16s[++ix]));
	} else {
	    res += utf8encode (0xFFFD);
	}
    }
    return res;
}

// Write a UTF-16 encoded string directly
ostream &operator<< (ostream &o, const u16string &s)
{
    for (auto ci = s.begin(); ci != s.end();) {
	char16_t c = *ci++;
	if (! is_surrogate (c)) {
	    o << utf8encode ((char32_t) c & 0xFFFF);
	} else if (is_high_surrogate (c) && ci != s.end() &&
		   is_low_surrogate (*ci)) {
	    o << utf8encode (surrogate_to_utf32 (c, *ci++));
	} else {
	    // ERROR! Use the replacement char instead!
	    o << utf8encode (0xFFFD);
	}
    }
    return o;
}

// Helper functions which returns the length, a UTF-8 code sequence
// corresponding to a given UTF-32 code point, will have.
static inline size_t utf8length (char32_t u32c)
{
    return (u32c < 0 ? 3 :	// ERROR CASE
	    u32c <= 0x7F ? 1 :
	    u32c <= 0x1FF ? 2 :
	    u32c <= 0xFFFF && (u32c < 0xD800 || u32c > 0xDFFD) ? 3 :
	    u32c <= 0x10FFFF ? 4 :
	    3);			// ERROR CASE
}

// This function is used for an exact calculation of the size of the string
// which results from a conversion from UTF-32 to UTF-8.
static inline size_t calculate_utf8size (const u32string &u32s)
{
    size_t u32sz = u32s.size(), ix, u8sz = 0;
    for (ix = 0; ix < u32sz; ++ix) {
	u8sz += utf8length (u32s[ix]);
    }
    return u8sz;
}

string u8 (const u32string &u32s)
{
    string res;
    size_t u32sz = u32s.size(), ix;

    size_t u8sz = calculate_utf8size (u32s);

    res.reserve (u8sz + 1);

    for (ix = 0; ix < u32sz; ++ix) {
	res += utf8encode (u32s[ix]);
    }

    return res;
}

ostream &operator<< (ostream &o, const u32string &s)
{
    for (auto c: s) { o << utf8encode (c); }
    return o;
}

#if 0
u16string u16 (const string &s)
{
    unsigned char ch;
    char32_t ir;
    u16string res;
    char16_t l, h;
    size_t ix = 0, ssz = s.size();
    while (ix < ssz) {
	ch = (unsigned char) s[ix++] & 0xFF;
	if (ch < 0x7F) {
	    ir = (char32_t) ch;
	} else if (ch >= 0xC0 && ch < 0xDF) {
	    ir = (char32_t) (ch & 0x1F);
	    if (ix >= ssz) { goto INV_UTF8; }
	    ch = s[ix++];
	    if (ch < 0x80 || ch > 0xBF) { goto INV_UTF8; }
	    ir = (ir << 6) | (char32_t) (ch & 0x3F);
	} else if (ch >= 0xE0 && ch <= 0xEF) {
	    ir = (char32_t) (ch & 0x0F);
	    if (ix >= ssz) { goto INV_UTF8; }
	    ch = s[ix++]
	    if (ch < 0x80 || ch > 0xBF) { goto INV_UTF8; }
	    ir = (ir << 6) | (char32_t) (ch & 0x3F);
	    if (ix >= ssz) { goto INV_UTF8; }
	    ch = s[ix++];
#endif



/**************** TESTING PART ****************/

#ifdef TESTING

#include <iostream>

using std::cerr, std::cout, std::endl, std::flush;
using std::wcout;

int main (int argc, char *argv[])
{
    u16string x16 = u"¡Hola, mundo! \U0001F600";
    u32string x32 = U"¡Hola, mundo! \U0001F600";


    cout << "UTF-16 (direct output): |" << x16 << "|" << endl;
    cout << "UTF-16 (string output): |" << u8 (x16) << "|" << endl;
    cout << "UTF-32 (direct output): |" << x32 << "|" << endl;
    cout << "UTF-32 (string output): |" << u8 (x32) << "|" << endl;

    cout << endl << "At END" << endl;
    return 0;
}

#endif /*TESTING*/
