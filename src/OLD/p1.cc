/* p1.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
**
** First test version of an XML parser reading GAEB
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <iostream>
#include <string>

#include <xercesc/util/PlatformUtils.hpp>

#include "u16to8.h"

using std::cerr, std::cout, std::endl, std::flush;
using namespace xercesc;

int main (int argc, char *argv[])
{
    try {
	XMLPlatformUtils::Initialize();
    } catch (const XMLException &e) {
	std::string msg (u8 (e.getMessage()));
	std::string file (e.getSrcFile());

	//cerr << "An 'XMLException' exception was raised: " << msg << endl;
	cerr << "An 'XMLException' was raised: " << e.getMessage() << endl;
	return 1;
    }

    cout << "Successfully initialised the Xerces-C++ code ..." << endl;

    XMLPlatformUtils::Terminate();
    return 0;
}
