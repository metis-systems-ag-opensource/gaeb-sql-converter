/* dbhelper.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Implementation part of 'dbhelper.{cc,h}'
** (A small helper class for collecting data as (name, value) pairs ant then
**  generating an INSERT statement from the collected data.)
**
*/

#include <stdexcept>

#include "Common/flipflop.h"
#include "Common/sutil.h"

#include "dbhelper.h"

namespace GAEB {

using std::ostream;
using std::exception, std::range_error, std::runtime_error;
using std::string, std::to_string;
using std::pair;

InsStmt::InsStmt (const std::string &tblname, size_t prealloc)
  : tblname(tblname)
{
    if (prealloc > 0) {
	colnames.reserve (prealloc); colvalues.reserve (prealloc);
    }
}

pair<string &, string &> InsStmt::operator[] (unsigned ix)
{
    try {
	string &colname = colnames.at (ix);
	string &colvalue = colvalues.at (ix);

	return pair<string &, string &> (colname, colvalue);
    } catch (exception &e) {
	throw range_error ("InsStmt[] - index out of range");
    }
}

void InsStmt::clear()
{
    colnames.clear();
    colvalues.clear();
}

string db_trans (const string &s)
{
    size_t slen = s.size();
    string res;
    res.reserve (s.size() * 2);
    for (size_t ix = 0; ix < slen; ++ix) {
        int ch = (int) s[ix] & 0xFF;
        switch (ch) {
            case '\b': res.append ("\\b"); break;
            case '\t': res.append ("\\t"); break;
            case '\n': res.append ("\\n"); break;
            case '\r': res.append ("\\r"); break;
            case '\'': res.append ("''"); break;
            case '\\': res.append ("\\\\"); break;
            default: {
                if (ch < 32 || ch == 127) {
                    res.append ("\\x" + i2hex (ch));

                } else {
                    res.push_back (ch);
                }
                break;
            }
        }
    }
    res.shrink_to_fit();
//    { string t(res); res.swap (t); }
    return res;
}

static
string val2db (const string &value, char vtype)
{
    switch (vtype) {
	case 's': {
	    string xv = db_trans (value);
	    return "'" + xv + "'";
	}
	case 'D': case 'd': case 'T': {
	    // Normal DATE, TIME, and DATETIME (DATE+TIME) values
	    return "'" + value + "'";
	}
#if 0	// ONLY if 'gYearMonth' is translated into a 'DATE' value in the
	// database.
	case 'm': {
	    // BUG-Fix: 'm' is the type letter for the `VersDate` field.
	    // On the other hand, if 'gYearMonth' were declared as string
	    // (with a fixed size: as 'CHAR(7)'), this case could be avoided.
	    return "'" + value + "-01'";
	}
#endif
	case 'b':
	    // BOOLEAN
	    return (value == "false" ? "FALSE" : "TRUE");
	case 'i': case 'k': case 'f': case 'r':
	    // INTeger, KEY, FIXED, DOUBLE
	case 'X': case 'x': case 'Y': case 'y':
	    // References to external tables (always INTEGER/LONG)
	case 'v': // Content isn't numerical (like variables)
	    return value;
	default: {
	    throw runtime_error ("Invalid type '" + string (1, vtype) + "'");
	}
    }
}

InsStmt &InsStmt::addColumn (const string &name, const string &value, char t)
{
    string v = value;
    trim (v, "\t\n\r ");
    colnames.push_back (name);
    colvalues.push_back (val2db (value, t));
    return *this;
}

InsStmt &InsStmt::addColumn (const string &name, long index)
{
    colnames.push_back (name);
    colvalues.push_back (to_string (index));
    return *this;
}

string InsStmt::generate() const
{
    flipflop<false> needcomma;
    string res;
    res.reserve (4096);
    res = "INSERT INTO `" + tblname + "` (";
    for (const string &n: colnames) {
	if (needcomma) { res += ", "; }
	res += "`" + n + "`";
    }
    needcomma.reset();
    res += ") VALUES (";
    for (const string &v: colvalues) {
	if (needcomma) { res += ", "; }
	res += v;
    }
    res += ")";
    { string t(res); res.swap (t); }
    return res;
}

InsStmt::operator string() const
{
    return generate();
}

size_t InsStmt::size() const
{
    return colnames.size();
}

ostream &operator<< (ostream &out, const InsStmt &im)
{
    out << im.generate();
    return out;
}

} /*namespace GAEB*/
