/* getinfo.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Get the version and type of the schema to be used from the XML document
** in a first pass
**
*/
#ifndef GETINFO_H
#define GETINFO_H

#include <string>

namespace GAEB {
    struct Info {
	std::string version, versiondate, type, prjname, date, time;
    };
    Info getinfo (const char *filename);
} /*namespace gaeb_info*/

#endif /*GETINFO_H*/
