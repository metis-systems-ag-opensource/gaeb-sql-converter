/* writer.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Abstract class (interface) for two different output mechanisms.
**
*/
#ifndef WRITER_H
#define WRITER_H

#include <string>

namespace GAEB {

class Writer {
public:
    virtual ~Writer() { }
    virtual Writer &dset (const std::string &data) = 0;
    virtual std::string get_id() = 0;
    virtual Writer &str (const std::string &msg) = 0;
    virtual Writer &commit() = 0;
    virtual Writer &operator= (const Writer &src) = delete;
    virtual Writer &operator= (Writer &&src) = 0;
};

} /*namespace GAEB*/

#endif /*WRITER_H*/
