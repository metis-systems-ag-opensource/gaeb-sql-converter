/* process.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Processing a valid GAEB DOM structure.
**
*/

#include <iostream>
#include <ostream>
#include <map>
#include <set>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMNamedNodeMap.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/dom/DOMNodeList.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMCDATASection.hpp>
#include <xercesc/dom/DOMEntity.hpp>
#include <xercesc/dom/DOMText.hpp>
#include <xercesc/dom/DOMProcessingInstruction.hpp>
#include <xercesc/dom/DOMComment.hpp>

#include "Common/flipflop.h"
#include "Common/sutil.h"

#include "getinfo.h"
#include "insmeta.h"
#include "tostdstring.h"
#include "xml2s.h"
#include "dbhelper.h"

#include "process.h"


namespace GAEB {

using std::cerr, std::cout, std::endl, std::flush;
using std::ostream;
using std::map;
using std::set;
using std::runtime_error;
using std::string, std::operator""s, std::to_string;
using std::vector;

using namespace xercesc;

class GAEBDocument;

struct gaeb_processor {
    gaeb_processor()
      : insmeta(), gaebid(), prjname(), date(), time(), out(nullptr), id(0)
    { }
    gaeb_processor (Writer &out, long gaebid)
      : insmeta(), gaebid(gaebid == 0 ? "" : to_string (gaebid)),
	prjname(), date(), time(), out(&out), id(0)
    { }
    void dbstruct (InsMeta &dbmeta) {
	insmeta = dbmeta;
    }
    void setout (Writer &_out) { out = &_out; }
    void prjdata (const string &_prjname,
		  const string &_date,
		  const string &_time) {
	prjname = _prjname;
	date = _date;
	time = _time;
    }
    void write_preamble (const Info &info);
    void process_elements (DOMNode *n);
protected:
    long next_id() { return ++id; }
    string gaeb_id() { return gaebid; }
    long gen_insert (DOMNode *n, const string &name);
    long choice_insert (DOMNode * &n, ElSpec &el);
    long sequence_insert (DOMNode * &n, ElSpec &el);
    long selement_insert (DOMNode * &n, ElSpec &el);
    long celement_insert (DOMNode * &n, ElSpec &el);
private:
    long choice_minsert (DOMNode * &n, TblSpec &tspec);
    long choice_sinsert (DOMNode * &n, TblSpec &tspec);
    long sequence_minsert (DOMNode * &n, TblSpec &tspec);
    long sequence_sinsert (DOMNode * &n, TblSpec &tspec);
    long selement_minsert (DOMNode * &n, ElSpec &el);
    long selement_sinsert (DOMNode * &n, ElSpec &el);
    void ins_select (DOMNode * &n, InsStmt &stmt, TblSpec &tdata);
    void ins_attributes (DOMNode *n, InsStmt &stmt, TblSpec &tdata);
    void assemble_node (DOMNode * &n, string &res);
    void assemble_element (DOMElement * &n, string &res);
    string reassemble (DOMNode * &n);
private:
    InsMeta insmeta;
    string gaebid, prjname, date, time;
    Writer *out;
    long id;
};

void process_gaeb (GAEBDocument *gaeb, const Info &info, InsMeta &meta,
		   Writer &out)
{
    gaeb_processor insdb;
    auto doc = (DOMDocument *) gaeb;
    insdb.setout (out);
    insdb.prjdata (info.prjname, info.date, info.time);
    insdb.dbstruct (meta);
    insdb.write_preamble (info);
    insdb.process_elements (doc);
}

void gaeb_processor::write_preamble (const Info &info)
{
    out->dset (insmeta.dbname) . commit();
    InsStmt stmt ("_GAEBfile");
    stmt . addColumn ("gaeb_version", info.version, 's')
	 . addColumn ("gaeb_versdate", info.versiondate + "-01", 'D')
	 . addColumn ("gaeb_phase", info.type, 's')
	 . addColumn ("date", info.date, 'D')
	 . addColumn ("time", info.time, 'T')
	 . addColumn ("prjname", info.prjname, 's');
    out->str (stmt) . commit();
    gaebid = out->get_id();
}

static
string nodename (DOMNode *n)
{
    return to_string (n->getNodeName());
}

static
string nodename (DOMEntity *n)
{
    return to_string (n->getNotationName());
}

static
string nodename (DOMProcessingInstruction *n)
{
    return to_string (n->getTarget());
}
static
string nodedata (DOMProcessingInstruction *n)
{
    return to_string (n->getData());
}
static
string nodedata (DOMComment *n)
{
    return to_string (n->getData());
}

static
string nodedata (DOMText *n)
{
    return xml2s (n->getWholeText(), false);
}

static
string nodedata (DOMCDATASection *n)
{
    return to_string (n->getWholeText());
}

static
bool is_textnode (DOMNode *n)
{
    return (n ? n->getNodeType() == DOMNode::TEXT_NODE : false);
}

static
const char *nodetype_name (DOMNode::NodeType nodetype)
{
    switch (nodetype) {
	case DOMNode::ELEMENT_NODE: return "ELEMENT_NODE";
	case DOMNode::ATTRIBUTE_NODE: return "ATTRIBUTE_NODE";
	case DOMNode::TEXT_NODE: return "TEXT_NODE";
	case DOMNode::CDATA_SECTION_NODE: return "CDATA_SECTION_NODE";
	case DOMNode::ENTITY_REFERENCE_NODE: return "ENTITY_REFERENCE_NODE";
	case DOMNode::ENTITY_NODE: return "ENTITY_NODE";
	case DOMNode::PROCESSING_INSTRUCTION_NODE:
	    return "PROCESSING_INSTRUCTION_NODE";
	case DOMNode::COMMENT_NODE: return "COMMENT_NODE";
	case DOMNode::DOCUMENT_NODE: return "DOCUMENT_NODE";
	case DOMNode::DOCUMENT_TYPE_NODE: return "DOCUMENT_TYPE_NODE";
	case DOMNode::DOCUMENT_FRAGMENT_NODE: return "DOCUMENT_FRAGMENT_NODE";
	case DOMNode::NOTATION_NODE: return "NOTATION_NODE";
	default: return "<UNKNOWN>";
    }
}

static
string textContent (DOMNode *n)
{
    int ec = 0;
    string res;
    res.reserve (4096);
    for (DOMNode *sub = n->getFirstChild(); sub; sub = sub->getNextSibling()) {
	DOMNode::NodeType nodetype = sub->getNodeType();
	switch (nodetype) {
	    case DOMNode::TEXT_NODE:
		res += to_string (sub->getNodeValue());
		break;
	    default:
		++ec;
		break;
	}
    }
    if (ec > 0) {
	throw runtime_error
	    ("Element '" + nodename (n) + "' has non-textual content.");
    }
    { string tmp (res); res.swap (tmp); }
    return res;
}

void gaeb_processor::process_elements (DOMNode *n)
{
    DOMNode::NodeType nodetype = n->getNodeType();
    string nodename = GAEB::nodename (n);
    switch (nodetype) {
	case DOMNode::DOCUMENT_NODE: {
	    DOMNodeList *nodes = n->getChildNodes();
	    if (! nodes) {
		throw runtime_error ("EMPTY DOCUMENT!");
	    }
	    // The document should contain only one element, the root node,
	    // but this only means that the loop is run once.
	    for (DOMNode *el = n->getFirstChild(); el;
		 el = el->getNextSibling()) {
		DOMNode::NodeType elt = el->getNodeType();
		if (elt != DOMNode::ELEMENT_NODE) { continue; }
		process_elements (el);
	    }
	    break;
	}
	case DOMNode::ELEMENT_NODE: {
	    vector<string> tblnames = insmeta.find_tables (nodename);
	    size_t tbnsz = tblnames.size();
	    if (tbnsz == 1) {
		// Search for the element in the list of tables. In this case,
		// the search is done manually, but this is done only once
		// for the top-level element(s).
		// Process a structured element
		gen_insert (n, tblnames.front());
	    } else if (tbnsz == 0) {
		throw runtime_error (
		    "INTERNAL ERROR! Missing INSERT description for element <" +
		    nodename + ">");
	    } else {
		throw runtime_error
		    ("INTERNAL ERROR! Multiple INSERT descriptions for element"
		     " <" + nodename + ">");
		// Don't know yet ... probably an error?!?
	    }
	    break;
	}
	default: {
	    // Definitely an error, because all othe node types should be
	    // processed in gen_insert (and other functions)
	    throw runtime_error ("Node type "s + nodetype_name (nodetype) +
				 " is invalid here.");
	}
    }
}

// This member function decides which (if any) of the member functions for
//   - inserting values of a sequnce,
//   - inserting the value of a complex type element,
//   - inserting values of a choice,
//   - inserting the values off a complex type definition,
// or if none of these member should be invoked, and a textual value being
// inserted.
void gaeb_processor::ins_select (DOMNode * &n, InsStmt &stmt, TblSpec &tdata)
{
    string elname = GAEB::nodename (n);
    ElSpec &el = tdata[elname];
    switch (el.type) {
	case 'x': {
	    // Inserting a sequence (own table).
	    long index = sequence_insert (n, el);
	    stmt.addColumn (el.name, index);
	    break;
	}
	case 'X': {
	    // Inserting an element consisting of a sequence of elements or
	    // text data
	    long index = selement_insert (n, el);
	    stmt.addColumn (el.name, index);
	    break;
	}
	case 'y': {
	    // Inserting an element into '<choice>'
	    long index = choice_insert (n, el);
	    stmt.addColumn (el.name, index);
	    break;
	}
	case 'Y': {
	    // Inserting an element solely consisting of a choice
	    long index = celement_insert (n, el);
	    stmt.addColumn (el.name, index);
	    break;
	}
	default: {
	    // Inserting elements which consist of text-only content and
	    // don't have attributes
	    stmt.addColumn (el.name, textContent (n), el.type);
	    n = n->getNextSibling();
	    break;
	}
    }
}

// Generate an insert statement for a complex element.
long gaeb_processor::gen_insert (DOMNode *n, const string &name)
{
    // FACT: 'n' is an element node of a complex structured element.
    // An insert statement must be generated for this element
    long id = next_id();
    auto modid = gaeb_id();
    TblSpec &tbldata = insmeta[name];
    InsStmt insstmt (tbldata.name);
    insstmt.addColumn ("gaebid", modid, 'v');
    insstmt.addColumn ("id", id);
    for (DOMNode *sub = n->getFirstChild(); sub;) {
	if (is_textnode (sub)) { sub = sub->getNextSibling(); continue; }
	// Remember! This routine _only_ processes complex elements. For
	// simple elements, another routine is used.
	DOMNode::NodeType subt = sub->getNodeType();
	if (subt != DOMNode::ELEMENT_NODE) {
	    throw runtime_error
		("Unexpected child element ("s + nodetype_name (subt) +
		 ") in a complex type element found.");
	}
	ins_select (sub, insstmt, tbldata);
    }
    string is = insstmt;
    out->str (insstmt) . commit();
    return id;
}

// Group insert (single/multi value versions)
long gaeb_processor::sequence_insert (DOMNode * &n, ElSpec &el)
{
    TblSpec &tspec = insmeta[el.ref];
    return (tspec.multivalue ? sequence_minsert (n, tspec)
			     : sequence_sinsert (n, tspec));
}

// Group insert (multi value version)
long gaeb_processor::sequence_minsert (DOMNode * &n, TblSpec &tbldata)
{
    string ref = tbldata.name;
    long id = next_id();
    auto modid = gaeb_id();
    InsStmt insstmt (ref);
    map<string, unsigned> inserted;
    long ix = 0;
    unsigned insertions_performed = 0;

    insstmt.addColumn ("gaebid", modid, 'v');
    insstmt.addColumn ("id", id);
    insstmt.addColumn ("ix", ++ix);

    while (n) {
	// Skip '#text' nodes, because the type of element processed here
	// solely consists of sub-elements. Any '#text' nodes here are
	// layout formatting information only and must be skipped.
	if (is_textnode (n)) { n = n->getNextSibling(); continue; }
	// Converting the node's name into a 'std::string'.
	string elname = GAEB::nodename (n);
	// If the current table ('tbldata') doesn't contain an element named
	// by the node name, we are at the end of collecting the data for the
	// multi-value sequence.
	if (! tbldata.has (elname)) { break; }
	// Extract the column data attached to the node name.
	ElSpec &el = tbldata[elname];

	// Before the element (name and value) is added to the INSERT statement
	// in progress, it must be checked if this element already was inserted
	// in a previous round (by inserting it into the 'inserted' map.
	auto [ptr, done] = inserted.emplace (el.seqn, el.seqx);
	if (! done) {
	    // The element was inserted in a previous round, but this does not
	    // mean that it it was inserted in this round. In order to avoid
	    // conflicts in the end detection of this sequence item, a check
	    // if this element is part of the current sequence item is
	    // required. This works like this:
	    // If the element's order number within the sequence item is
	    // greater than the one of the last stored element, the new element
	    // is part of the current sequence item.
	    unsigned &seqx = ptr->second;
	    if (el.seqx <= seqx) {
		// Otherwise, the current sequence item is complete and must
		// be written out ...
		out->str (insstmt) . commit();
		// ... and a new insert statement be started.
		insstmt.clear(); ++insertions_performed;
		insstmt.addColumn ("gaebid", modid, 'v');
		insstmt.addColumn ("id", id);
		insstmt.addColumn ("ix", ++ix);
	    }
	    // Overwrite the order number of the last sequence item with the
	    // one of the new element.
	    seqx = el.seqx;
	}
	// The real "insertion" step depends on the type of the element, and
	// is done with the same function being used for all insertions:
	// 'ins_select()'.
	ins_select (n, insstmt, tbldata);
	// 'ins_select()' always advances through the nodes, so no extra
	// advancement step is necessary here.
    }
    // Sometimes, the situation arises that a new INSERT statement was started,
    // but there was no more data to be inserted. In this case, 'insstmt' is
    // empty save from the key attributes/columns.
    if (insstmt.size() > 3) {
	// Otherwise, the INSERT statement must be written out.
	out->str (insstmt) . commit();
	++insertions_performed;
    }
    // If no INSERT statements were written out, there is not much sense in
    // returning the generated 'id'. In this case, '0' is returned instead.
    // This may be either used directly as "foreign key" in the table
    // referencing the entries processed here, or converted to 'NULL'.
    return (insertions_performed > 0 ? id : 0);
}

// The insertion of a single <sequence> is much easier than that of a list
// of <sequence>s. The reason is simple. Elements never occur twice, so all
// elements which are members of the <sequence>.
long gaeb_processor::sequence_sinsert (DOMNode * &n, TblSpec &tbldata)
{
    long id = next_id();
    auto modid = gaeb_id();
    InsStmt insstmt (tbldata.name);
    unsigned insertions_performed = 0;

    insstmt.addColumn ("gaebid", modid, 'v');
    insstmt.addColumn ("id", id);

    while (n) {
	// <sequence> tables _never_ contain text nodes, which means that any
	// text found here was inserted solely for layout.
	if (is_textnode (n)) { n = n->getNextSibling(); continue; }
	string elname = GAEB::nodename (n);
	if (! tbldata.has (elname)) { break; }
	// 'ins_select()' always advances through the nodes.
	ins_select (n, insstmt, tbldata);
    }
    if (insstmt.size() > 2) {
	// Non-empty INSERT statement.
	out->str (insstmt) . commit();
	++insertions_performed;
    }
    // At the end of this member function, 'n' either points to the next
    // sibling which is not part of the sequence, or contains 'nullptr',
    // indicating the end of the node list.
    return (insertions_performed > 0 ? id : 0);
}

long gaeb_processor::choice_insert (DOMNode * &n, ElSpec &el)
{
    TblSpec &tspec = insmeta[el.ref];
    auto p = tspec.multivalue ? &gaeb_processor::choice_minsert
			      : &gaeb_processor::choice_sinsert;
    return (this->*p) (n, tspec);
//    return (tspec.multivalue ? choice_minsert (n, tspec) : choice_sinsert (n, tspec));
}

// Generate INSERT statements for a multiple-value choice
long gaeb_processor::choice_minsert (DOMNode * &n, TblSpec &tbldata)
{
    string nodename = GAEB::nodename (n);
    long id = next_id(), ix = 0;
    auto modid = gaeb_id();
    InsStmt insstmt (tbldata.name);
    map<string, unsigned> inserted;
    unsigned insertions_performed = 0;

    // In contrast to a <sequence>, a <choice> can contain only one value
    // (at most). This means that for each "element" being found, an extra
    // entry in the corresponding database table is created.
    while (n) {
	if (is_textnode (n)) { n = n->getNextSibling(); }
	string elname = GAEB::nodename (n);

	// End the loop iff the node is no longer part of the multiple value
	// choice.
	if (! tbldata.has (elname)) { break; }

	// Create a new table entry in the <choice> table (a new INSERT
	// statement).
	insstmt.addColumn ("gaebid", modid, 'v');
	insstmt.addColumn ("id", id);
	insstmt.addColumn ("ix", ++ix);
	// REMEMBER! 'ins_select()' advances 'n'.
	ins_select (n, insstmt, tbldata);

	// Write the created INSERT statement to the output stream
	out->str (insstmt) . commit();

	// Reset 'insstmt' in preparation of the next INSERT statement and
	// increase the counter for the just constructed INSERT statement.
	insstmt.clear(); ++insertions_performed;
    }
    return (insertions_performed > 0 ? id : 0);
}

// Generate an INSERT statement for a single value choice
long gaeb_processor::choice_sinsert (DOMNode * &n, TblSpec &tbldata)
{
    long id = next_id();
    auto modid = gaeb_id();
    InsStmt insstmt (tbldata.name);
    unsigned insertions_performed = 0;

    insstmt.addColumn ("gaebid", modid, 'v');
    insstmt.addColumn ("id", id);

//    // Skip '#text' nodes.
//    while (n && is_textnode (n)) { n = n->getNextSibling(); }

    // It there is nothing ... do nothing!
    if (n) {
	// Get the node name
	string elname = GAEB::nodename (n);
	// If the node is part of the <choice>, create an INSERT statement
	// for it.
	if (tbldata.has (elname)) {
	    ins_select (n, insstmt, tbldata);
	    out->str (insstmt) . commit();
	    ++insertions_performed;
	}
    }
    return (insertions_performed > 0 ? id : 0);
}

// Add columns for the attributes of a given DOM node to the current INSERT
// statement.
void gaeb_processor::ins_attributes (DOMNode *n, InsStmt &stmt, TblSpec &tdata)
{
    if (n->hasAttributes()) {
	string nodename = GAEB::nodename (n);
	DOMNamedNodeMap *attrs = n->getAttributes();
	XMLSize_t num_attrs = attrs->getLength();
	for (XMLSize_t ix = 0; ix < num_attrs; ++ix) {
	    DOMNode *attr = attrs->item (ix);
	    string elname = GAEB::nodename (attr);
	    ElSpec &el = tdata[elname];
	    switch (el.type) {
		case 'x': case 'X': case 'y': case 'Y': {
		    // Attributes can _never_ be of a complex type.
		    throw runtime_error
			("Attribute '" + nodename + "." + elname +
			 "' cannot have a complex value.");
		    break;
		}
		default: {
		    // Add a column for the attribute to 'stmt'
		    stmt.addColumn (el.name,
				    to_string (attr->getNodeValue()),
				    el.type);
		    break;
		}
	    }
	}
    }
}

// Generate an INSERT statement (or INSERT statements) for the given simple
// type element.
//   `n` is the currently processed DOM node
//   `el` is the already selected INSERT metadata for this node
long gaeb_processor::selement_insert (DOMNode * &n, ElSpec &el)
{
    long id = next_id(), ix = 0;
    auto modid = gaeb_id();
    string nodename = GAEB::nodename (n);
    TblSpec &tbldata = insmeta[el.ref];
    InsStmt insstmt (tbldata.name);
    bool multivalue = tbldata.multivalue;
    do {
	// Add the PRIMARY KEY columns
	insstmt.addColumn ("gaebid", modid, 'v');
	insstmt.addColumn ("id", id);
	// If the element may occur more than once, the PRIMARY KEY contains
	// of a third attribute, which is an index of the corresponding
	// element.
	if (multivalue) { insstmt.addColumn ("ix", ++ix); }

	if (tbldata.reassemble) {
	    // Reassemble the complete content of the element, be it text or
	    // any combination of sub-elements.
	    string forced_text = reassemble (n);
	    // Add the corresponding <column, value> pair to the INSERT
	    // statement currently generated. The type 's' means that the
	    // resulting text must be converted into a DB-alike string. This
	    // means that all control characters and the single quotes must
	    // be converted into matching escape sequences.
	    insstmt.addColumn ("_content", forced_text, 's');
	} else {
	    // Add the existing sub-elements (iff they are text nodes)
	    for (DOMNode *sub = n->getFirstChild(); sub;) {
		// If the element contains a '#text' node, this node must be
		// inserted.
		if (is_textnode (sub)) {
		    if (tbldata.has ("_content")) {
			ElSpec &el = tbldata["_content"];
			insstmt.addColumn ("_content",
					   to_string (sub->getNodeValue()),
					   el.type);
		    }
		    sub = sub->getNextSibling();
		} else {
		    ins_select (sub, insstmt, tbldata);
		}
	    }
	}
	// If the element has attributes, add these, too.
	ins_attributes (n, insstmt, tbldata);
	// Finish the insert statement (writing it to the output stream
	// referenced by the member variable 'out'.
	out->str (insstmt) . commit();
	// Prepare 'insstmt' for the next round.
	insstmt.clear();
	// Advance to the next <element> node.
	n = n->getNextSibling();
	// Terminate this if the <element> was not multi-value, or if the
	// end of the nodes was reached, or if the name of the next node
	// doesn't the name of the <element> node just inserted.
    } while (multivalue && n && nodename == GAEB::nodename (n));
    // Return the id-part of thr two-/three-fold primary key (this is the
    // value which is inserted into the outer table).
    return id;
}

// This member function is currently unused, primarily because of the structure
// of the generated insertion metadata. In a later step, it may be used for
// elements which solely consist of a choice.
long gaeb_processor::celement_insert (DOMNode * &n, ElSpec &el)
{
    long id = next_id(), ix = 0;
    auto modid = gaeb_id();
    string nodename = GAEB::nodename (n);
    TblSpec &tbldata = insmeta[el.ref];
    InsStmt insstmt (tbldata.name);
    if (! n) { return 0; }
    bool multivalue = tbldata.multivalue;
    do {
	DOMNode *sub = n->getFirstChild();
	if (sub) {
	    insstmt.addColumn ("gaebid", modid, 'v');
	    insstmt.addColumn ("id", id);
	    if (multivalue) { insstmt.addColumn ("ix", ++ix); }
	    if (! is_textnode (sub)) {
		ins_select (sub, insstmt, tbldata);
	    }
	    // If the element has attributes, add these, too.
	    ins_attributes (n, insstmt, tbldata);
	    out->str (insstmt) . commit();
	    insstmt.clear();
	}
	n = n->getNextSibling();
    } while (multivalue && n && nodename == GAEB::nodename (n));
    return id;
}

void gaeb_processor::assemble_element (DOMElement * &n, string &res)
{
    string elname = to_string (n->getNodeName());
    res.push_back ('<'); res.append (elname);
    DOMNamedNodeMap *attrs = n->getAttributes();
    XMLSize_t num_attrs = attrs->getLength();
    for (XMLSize_t ix = 0; ix < num_attrs; ++ix) {
	DOMNode *attr = attrs->item (ix);
	string attrname = GAEB::nodename (attr);
	const XMLCh *attrval = attr->getNodeValue();
	res.push_back (' '); res.append (attrname);
	res.push_back ('='); res.push_back ('"');
	res.append (xml2s (attrval));
	res.push_back ('"');
    }
    if (n->hasChildNodes()) {
	res.push_back ('>');
	for (DOMNode *sub = n->getFirstChild(); sub;
	     sub = sub->getNextSibling()) {
	    assemble_node (sub, res);
	}
	res.append ("</"); res.append (elname); res.push_back ('>');
    } else {
	res.append ("/>");
    }
}

static
void check_conversion (void *p, const string &type)
{
    if (! p) {
	throw runtime_error (
	    "INTERNAL ERROR! Convertion ('DOMNode' => '" + type +
	    "') failed."
	);
    }
}

void gaeb_processor::assemble_node (DOMNode * &n, string &res)
{
    switch (n->getNodeType()) {
	case DOMNode::ELEMENT_NODE: {
	    auto *en = dynamic_cast<DOMElement *>(n);
	    check_conversion (en, "DOMElement");
	    assemble_element (en, res);
	    break;
	}
	case DOMNode::TEXT_NODE: {
	    auto *tn = dynamic_cast<DOMText *>(n);
	    check_conversion (tn, "DOMText");
	    res.append (nodedata (tn));
	    break;
	}
	case DOMNode::CDATA_SECTION_NODE: {
	    auto *cd = dynamic_cast<DOMCDATASection *>(n);
	    check_conversion (cd, "DOMCDATASection");
	    res.append ("<!<CDATA[");
	    res.append (nodedata (cd));
	    res.append ("]]>");
	    break;
	}
	case DOMNode::PROCESSING_INSTRUCTION_NODE: {
	    auto *pn = dynamic_cast<DOMProcessingInstruction *>(n);
	    check_conversion (pn, "DOMProcessingInstruction");
	    res.append ("<?"); res.append (nodename (pn));
	    res.push_back (' '); res.append (nodedata (pn));
	    res.append ("?>");
	    break;
	}
	case DOMNode::COMMENT_NODE: {
	    auto *cn = dynamic_cast<DOMComment *>(n);
	    check_conversion (cn, "DOMComment");
	    res.append ("<!--");
	    res.append (nodedata (cn));
	    res.append ("-->");
	    break;
	}
#if 0 // Don't know how to handle this (yet)
	case DOMNode::ENTITY_REFERENCE_NODE: {
	    auto er = dynamic_cast<DOMEntityReference *>(n);
	    res.push_back ('&');
	    res.append (to_string (n));
	    res.push_back (';');
	    break;
	}
#endif
	case DOMNode::ENTITY_NODE: {
	    auto en = dynamic_cast<DOMEntity *>(n);
	    check_conversion (en, "DOMEntity");
	    res.append ("&#"); res.append (nodename (en));
	    res.push_back (';');
	    break;
	}
	default:
	    // Ignoring the remaining types of nodes, because they would not
	    // make any sense here.
	    break;
    }
}


string gaeb_processor::reassemble (DOMNode * &n)
{
    string res;
    res.reserve (4096);
    for (DOMNode *sub = n->getFirstChild(); sub; sub = sub->getNextSibling()) {
	assemble_node (sub, res);
    }
    res.shrink_to_fit();
    return res;
}

} /*namespace GAEB*/
