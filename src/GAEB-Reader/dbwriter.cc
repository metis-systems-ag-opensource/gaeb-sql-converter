/* dbwriter.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** `DBQ` implementation of the "interface" `Writer` (implementation part).
**
*/

#include <stdexcept>
#include <utility>

#include "Common/sutil.h"

#include "readconf.h"

#include "dbwriter.h"

namespace GAEB {

using std::runtime_error;
using std::string, std::to_string;

DBWriter::DBWriter (const string &host, int port, const string &authfile)
  : buf(), id(), hostname(host), _dbh(nullptr), portnum(0)
{
    UserPass authdata (read_credentials (authfile));
    auto dbh = new DBH (hostname, portnum, NoDatabase,
			authdata.username, authdata.password);
    dbh->connect();
    _dbh = dbh; exthandle = false;
    _dbh->autocommit (false);
}

DBWriter::DBWriter (DBH &dbh)
  : buf(), id(), hostname(), _dbh(&dbh), portnum(0)
{
    buf.reserve (8192); exthandle = true;
}

DBWriter::DBWriter (DBWriter &&src)
  : buf(std::move (src.buf)), id(std::move (src.id)), _dbh(src._dbh)
{
    src._dbh = nullptr;
}

DBWriter::~DBWriter()
{
    close();
}

DBWriter &DBWriter::dset (const std::string &dbname)
{
    if (! _dbh->selectdb (dbname)) {
	throw runtime_error ("Selecting `" + dbname + "` failed.");
    }
    return *this;
}

string DBWriter::get_id()
{
    if (id.empty()) {
	id = to_string (_dbh->insert_id());
    }
    return id;
}

DBWriter &DBWriter::str (const string &msg)
{
    if (! _dbh) { throw runtime_error ("DBWriter closed"); }
    buf += msg;
    return *this;
}

DBWriter &DBWriter::commit()
{
    if (! _dbh) { throw runtime_error ("DBWriter closed"); }
    if (! buf.empty()) {
	int rc = _dbh->nrquery (buf);
	if (rc != 0) {
	    throw runtime_error ("Query failed.");
	}
	buf.clear();
    }
    return *this;
}

DBWriter &DBWriter::operator= (Writer &&src)
{
    DBWriter &s = dynamic_cast<DBWriter &>(src);
    buf = std::move (s.buf); id = std::move (s.id); _dbh = s._dbh;
    s._dbh = nullptr;
    return *this;
}

void DBWriter::close()
{
   if (_dbh) {
	commit(); _dbh->commit();
	if (! exthandle) { delete _dbh; }
	_dbh = nullptr;
    }
}

} /*namespace GAEB*/
