/* infohandler.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Implementation part of the 'class VersiionHandler'
** (Simple action rutines for catching specific data from a GAEB file, enclosed
**  in a class.)
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <cctype>
#include <forward_list>
#include <iostream>
#include <stdexcept>
#include <string>

#include <xercesc/sax2/Attributes.hpp>
#include <xercesc/sax/SAXParseException.hpp>
#include <xercesc/sax/SAXException.hpp>

#include "tostdstring.h"

#include "infohandler.h"

namespace GAEB {

using std::cerr, std::endl, std::flush;
using std::exception, std::runtime_error;
using std::string, std::operator""s, std::to_string;
using CtxStack = std::forward_list<string>;

struct InfoHandlerImpl {
    CtxStack ctxstack;
    string version, versiondate, type, nsident;
    string prjname, date, time;
    bool is_type, is_version, is_versiondate;
    bool is_prjname, is_date, is_time;
    bool have_type, have_version, have_versiondate;
    bool have_prjname, have_date, have_time;
    unsigned errcount;
};

InfoHandler::InfoHandler()
    : impl(nullptr)
{
    impl = new InfoHandlerImpl;
    impl->is_type = impl->is_version = impl->is_versiondate = false;
    impl->is_prjname = impl->is_date = impl->is_time = false;
    impl->have_type = impl->have_version = impl->have_versiondate = false;
    impl->have_prjname = impl->have_date = impl->have_time = false;
    impl->version.clear(); impl->versiondate.clear(); impl->type.clear();
    impl->nsident.clear();
    impl->prjname.clear(); impl->date.clear(); impl->time.clear();
    impl->errcount = 0;
}

InfoHandler::~InfoHandler()
{
    delete impl;
}

bool is_context (const std::initializer_list<const char *> &test,
		 const CtxStack &ctx)
{
    auto ctxi = ctx.begin(), ctxe = ctx.end();
    auto testi = test.begin(), teste = test.end();
    while (testi != teste) {
	// The context stack has less elements than the test list => failure
	if (ctxi == ctxe) { return false; }
	// The current elements of test list and context stack don't match
	// => failure
	if (*testi != *ctxi) { return false; }
	testi++; ctxi++;
    }
    // The test succeeds *only* if the context stack has the same number of
    // elements as the test list. Translated means this: The context stack
    // iterator has reached the end of the context stack.
    return ctxi == ctxe;
}

void InfoHandler::startElement (const XMLCh *const uri,
				const XMLCh *const localname,
				const XMLCh *const qualname,
				const Attributes &attrs)
{
    string elname = to_string (localname);
    // Entering the <elname> context
    impl->ctxstack.push_front (elname);
    if (impl->nsident.empty()) {
	impl->nsident = to_string (uri);
    }

    // Testing for the `<Version>` element *only* if the version was not
    // already retrieved.
    if (! impl->have_version && elname == "Version") {
	// Checking if the context is right. The list to be tested must have
	// the same order as the context stack (innermost to outermost).
	auto vctx = { "Version", "GAEBInfo", "GAEB" };
	if (is_context (vctx, impl->ctxstack)) {
	    // Only if the context is correct, the element's content can be
	    // retrieved (as version number).
	    impl->is_version = true;
	}
	return;
    }

    // Testing for the `<VersDate>` element *only* if the version was not
    // already retrieved.
    if (! impl->have_versiondate && elname == "VersDate") {
	// Checking if the context is right. The list to be tested must have
	// the same order as the context stack (innermost to outermost).
	auto vctx = { "VersDate", "GAEBInfo", "GAEB" };
	if (is_context (vctx, impl->ctxstack)) {
	    impl->is_versiondate = true;
	}
	return;
    }

    // Testing for the `<DP>` element *only* if the type was not already
    // retrieved. (The `<DP>` element specifies the type of the GAEB file
    // in the GAEB specification.)
    if (! impl->have_type && elname == "DP") {
	// Like above, but for the GAEB file type.
	auto tctx = { "DP", "Award", "GAEB" };
	if (is_context (tctx, impl->ctxstack)) {
	    impl->is_type = true;
	}
    }

    // Additional elements whose contents are extracted

    // <GAEB>
    //   ...
    //   <PrjInfo>
    //     ...
    //     <NamePrj>`name of project`</NamePrj>
    //     ...
    //   </PrjInfo>
    //   ...
    // </GAEB>
    if (! impl->have_prjname && elname == "NamePrj") {
	auto vctx = { "NamePrj", "PrjInfo", "GAEB" };
	if (is_context (vctx, impl->ctxstack)) {
	    // Only if the context is correct, the element's content can be
	    // retrieved (as version number).
	    impl->is_prjname = true;
	}
	return;
    }

    // <GAEB>
    //   <GAEBInfo>
    //     ...
    //     <Date>` GAEB file date`</Date>
    //     ...
    //   </GAEBInfo>
    //   ...
    // </GAEB>
    if (! impl->have_date && elname == "Date") {
	auto vctx = { "Date", "GAEBInfo", "GAEB" };
	if (is_context (vctx, impl->ctxstack)) {
	    // Only if the context is correct, the element's content can be
	    // retrieved (as version number).
	    impl->is_date = true;
	}
	return;
    }

    // <GAEB>
    //   <GAEBInfo>
    //     ...
    //     <Time>` GAEB file time`</Time>
    //     ...
    //   </GAEBInfo>
    //   ...
    // </GAEB>
    if (! impl->have_time && elname == "Time") {
	auto vctx = { "Time", "GAEBInfo", "GAEB" };
	if (is_context (vctx, impl->ctxstack)) {
	    // Only if the context is correct, the element's content can be
	    // retrieved (as version number).
	    impl->is_time = true;
	}
	return;
    }
}

static bool isws (char ch) { return isspace (ch) != 0; }

static void trim (string &s, bool (*p) (char))
{
    size_t ssize = s.size(), ix = 0, jx = ssize;
    size_t l_past_x = string::npos, t_first_x = string::npos;
    while (ix < ssize && p (s[ix])) { ++ix; }
    if (ix > 0) { l_past_x = ix; }
    while (jx > ix && p (s[jx - 1])) { --jx; }
    if (jx < ssize) { t_first_x = jx; }
    if (t_first_x != string::npos) { s.erase (t_first_x); }
    if (l_past_x != string::npos) { s.erase (0, l_past_x); }
    s.shrink_to_fit();
    //string t(s); s = std::move (t);
}

void InfoHandler::endElement (const XMLCh *const uri,
			      const XMLCh *const localname,
			      const XMLCh *const qualname)
{
    string elname (to_string (localname));
    if (impl->is_version) {
	// Ending the `<Version>` element in the correct context
	trim (impl->version, isws); impl->is_version = false;
	// *Only* accept a non-empty version string after trimming
	if (! impl->version.empty()) { impl->have_version = true; }
    } else if (impl->is_versiondate) {
	// Ending the `<VersDate>` element in the correct context
	trim (impl->versiondate, isws); impl->is_versiondate = false;
	// *Only* accept a non-empty versiondate string after trimming
	if (! impl->versiondate.empty()) { impl->have_versiondate = true; }
    } else if (impl->is_type) {
	// Ending the `<DP>` element in the correct context
	trim (impl->type, isws); impl->is_type = false;
	// *Only* accept a non-empty type string after trimming
	if (! impl->type.empty()) { impl->have_type = true; }
    } else if (impl->is_prjname) {
	trim (impl->prjname, isws); impl->is_prjname = false;
	if (! impl->prjname.empty()) { impl->have_prjname = true; }
    } else if (impl->is_date) {
	trim (impl->date, isws); impl->is_date = false;
	if (! impl->date.empty()) { impl->have_date = true; }
    } else if (impl->is_time) {
	trim (impl->time, isws); impl->is_time = false;
	if (! impl->time.empty()) { impl->have_time = true; }
    }
    // Leaving the <elname> context
    impl->ctxstack.pop_front();
}

void InfoHandler::characters (const XMLCh *text, XMLSize_t textlen)
{
    if (impl->is_version) {		// GAEB>>GAEBInfo>>Version element
	impl->version += to_string (text, textlen);
    } else if (impl->is_versiondate) {	// GAEB>>GAEBInfo>>VersDate element
	impl->versiondate += to_string (text, textlen);
    } else if (impl->is_type) {		// GAEB>>Award>>DP element
	impl->type += to_string (text, textlen);
    } else if (impl->is_prjname) {	// GAEB>>PrjInfo>>NamePrj element
	impl->prjname += to_string (text, textlen);
    } else if (impl->is_date) {		// GAEB>>GAEBInfo>>Date element
	impl->date += to_string (text, textlen);
    } else if (impl->is_time) {		// GAEB>>GAEBInfo>>Time element
	impl->time += to_string (text, textlen);
    }
}

void InfoHandler::ignorableWhitespace (const XMLCh* const chars,
				       const XMLSize_t length)
{ }

void InfoHandler::startDocument()
{
    reset();
}

void InfoHandler::error (const SAXParseException& e)
{
    ++impl->errcount;
    cerr << endl << "In " << to_string (e.getSystemId()) <<
	    "(" << e.getLineNumber() << ":" << e.getColumnNumber() << "): " <<
	    to_string (e.getMessage()) << endl;
}

void InfoHandler::fatalError (const SAXParseException& e)
{
    ++impl->errcount;
    cerr << endl << "In " << to_string (e.getSystemId()) << "(" <<
	    e.getLineNumber() << ":" << e.getColumnNumber() <<
	    "): FATAL ERROR! " << to_string (e.getMessage()) << endl;
}

void InfoHandler::warning (const SAXParseException& e)
{
    cerr << endl << "In " << to_string (e.getSystemId()) << "(" <<
	    e.getLineNumber() << ":" << e.getColumnNumber() <<
	    "): WARNING! " << to_string (e.getMessage()) << endl;
}

void InfoHandler::resetErrors()
{
    impl->errcount = 0;
}

void InfoHandler::reset()
{
    impl->ctxstack.clear();
    impl->version.clear(); impl->version.shrink_to_fit();
    impl->type.clear(); impl->type.shrink_to_fit();
    impl->nsident.clear(); impl->nsident.shrink_to_fit();
    impl->is_type = impl->is_version = false;
    impl->have_type = impl->have_version = false;
    impl->errcount = 0;
}

bool InfoHandler::have_version() const
{
    return impl->have_version;
}

const string &InfoHandler::version() const
{
    return impl->version;
}

bool InfoHandler::have_versiondate() const
{
    return impl->have_versiondate;
}

const string &InfoHandler::versiondate() const
{
    return impl->versiondate;
}

bool InfoHandler::have_type() const
{
    return impl->have_type;
}

const string &InfoHandler::type() const
{
    return impl->type;
}

bool InfoHandler::have_prjname() const
{
    return impl->have_prjname;
}

const string &InfoHandler::prjname() const
{
    return impl->prjname;
}

bool InfoHandler::have_date() const
{
    return impl->have_date;
}

const string &InfoHandler::date() const
{
    return impl->date;
}

bool InfoHandler::have_time() const
{
    return impl->have_time;
}

const string &InfoHandler::time() const
{
    return impl->time;
}

} /*namespace GAEB*/
