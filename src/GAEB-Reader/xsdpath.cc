/* xsdpath.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Construct the pathname of an XSD file from the data given by the
** configuration and the version/type information retrieved from a GAEB file.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#include <iostream>
using std::cerr, std::cout, std::flush, std::endl;
#endif /*DEBUG*/

#include <cerrno>
#include <filesystem>

#include "Common/fnutil.h"
#include "Common/pathcat.h"
#include "Common/sutil.h"

#include "xsdpath.h"

namespace GAEB {

namespace fs = std::filesystem;
using std::string;
using std::make_pair;

/* Trailing code (filesystem tests) for the following function(s).
*/
static PathData file_check (const string &tmp)
{
    string fpath;
    fpath.reserve (tmp.size() + 1);
    fpath = tmp;

    // Checking if the file the pathname 'fpath' refers exists and is a regular
    // file
    std::error_code ec;
    fs::file_status fstat (fs::status (fpath, ec));
    if (! fs::exists (fstat)) {
	return make_pair ("File \"" + fpath + "\" not found", ENOENT);
    }
    if (! fs::is_regular_file (fstat)) {
	return make_pair ("File \"" + fpath + "\" is no regular file", EINVAL);
    }

    // Return the pathname 'fpath' and an error code 0 (indicating success)
    return make_pair (fpath, 0);
}

/* Construct a pathname from the configuration, the Info data retrieved from
** the GAEB file to be parsed/validated and (optionally) a special library
** name.
*/
PathData xsdpath (const Config &cfg, const Info &gaebinfo, const string &xlib)
{
    string typedir = "X" + gaebinfo.type + "DIR", uclib = uppercase (xlib);

    if (! cfg.has (typedir)) {
	// No 'typedir' means that the GAEB schema definitions are simply
	// not organised in subdirectories specifying the categories
	typedir = ".";
    }

    if (! xlib.empty() && ! cfg.has (uclib)) {
	return make_pair ("No '" + uclib + "' configured", ENODATA);
    }

    auto &filetmpl = cfg[xlib.empty() ? "XSD" : uclib];

    Substitutes vars {
	{ "version", gaebinfo.version },
	{ "typedir", cfg[typedir] },
	{ "type", gaebinfo.type },
	{ "versiondate", gaebinfo.versiondate },
    };

    if (cfg.has ("VERSIONDIR")) {
	string versiondir = tmpl_subst (cfg["VERSIONDIR"], vars);
	vars.insert (make_pair ("versiondir", versiondir));
    }

    string xsdsubpath = tmpl_subst (filetmpl, vars);

    // Remove a leading './', which may occur if 'typedir' was reduced to '.'.
    if (is_prefix ("./", xsdsubpath, true)) { xsdsubpath.erase (0, 2); }

    return file_check (cfg["XSDBASE"] / xsdsubpath);
}

} /*namespace GAEB*/
