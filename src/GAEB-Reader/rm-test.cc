/* rm-test.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Small test program for 'readmeta.{cc,h}'.
**
*/

#include <cstring>
#include <fstream>
#include <iostream>
#include <string>

#include "Common/fnutil.h"

#include "insmeta.h"
#include "readmeta.h"

using namespace std;

int main (int argc, char *argv[])
{
    const string prog = basename (*argv);
    if (argc < 2) {
	cerr << "Usage: " << prog << " insertion-meta-file" << endl;
	return 0;
    }

    string mdfile = argv[1];

    ifstream im (mdfile);
    if (! im.is_open()) {
	cerr << prog << ": Attempt to open '" << mdfile << "' failed." <<
		endl;
	return 1;
    }

    auto [idata, done] = GAEB::readmeta (im);

    im.close();

    cout << endl << "At END" << endl;
    return 0;
}
