/* parse.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Interface file for 'parse.cc'
** (Parse a GAEB file into a DOM structure, using a list of given XSD files
**  for validation.)
**
*/
#ifndef PARSE_H
#define PARSE_H

#include <string>

#include "Common/config.h"
#include "getinfo.h"

namespace GAEB {

// Abstract class definition of 'DOMDocument' (from Xerces-C). I'm using
// this method here, because i don't want to '#include' the Xerces-C headers
// (they should be invisible for the main program).
// Because i'm using only 'DOMDocument *' here, an opaque class/struct
// definition is _valid_ here.
//
class GAEBDocument;	// Using never anything except pointers here!

GAEBDocument *parse_file (const Config &cfg,
			  const Info &vtinfo,
			  const std::string &gaeb_file);

} /*namespace GAEB*/

#endif /*PARSE_H*/
