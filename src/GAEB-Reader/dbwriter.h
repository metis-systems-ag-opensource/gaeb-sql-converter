/* dbwriter.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** `DBQ` implementation of the "interface" `Writer` (interface part).
**
*/
#ifndef DBWRITER_H
#define DBWRITER_H

#include <string>

#include "Common/dbif.h"

#include "writer.h"

namespace GAEB {

class DBWriter : public Writer {
public:
    DBWriter (const std::string &host, int port, const std::string &authfile);
    DBWriter (DBH &dbh);
    DBWriter (const DBWriter &) = delete;
    DBWriter (DBWriter &&src);
    ~DBWriter();
    DBWriter &dset (const std::string &dbname);
    std::string get_id();
    DBWriter &str (const std::string &msg);
    DBWriter &commit();
    DBWriter &operator= (Writer &&src);
private:
    void close();
    std::string buf, id, hostname;
    DBH *_dbh;
    int portnum;
    bool exthandle;
};

} /*namespace GAEB*/

#endif /*DBWRITER_H*/
