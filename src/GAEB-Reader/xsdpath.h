/* xsdpath.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Interface file for 'xsdpath.cc'
** (Construct the pathname of an XSD file from the data given by the
**  configuration and the version/type information retrieved from a GAEB file.)
**
*/
#ifndef XSDPATH_H
#define XSDPATH_H

#include <string>
#include <utility>

#include "Common/config.h"

#include "getinfo.h"

namespace GAEB {

using PathData = std::pair<std::string, int>;

PathData xsdpath (const Config &cfg, const Info &gaebinfo,
		  const std::string &xlib = "");

} /*namespace GAEB*/

#endif /*XSDPATH_H*/
