/* xml2s.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Convert an XML string (Xerces-C 'XMLCh *') into a 'std::string' (including
** the conversion of special characters into XML entities).
**
*/

#include <cstddef>
#include <cstdint>

#include "xml2s.h"

namespace GAEB {

using std::string, std::operator""s, std::to_string;

static inline bool is_high_surrogate (XMLCh cp)
{
    return cp >= 0xD800 && cp <= 0xDBFF;
}

static inline bool is_low_surrogate (XMLCh cp)
{
    return cp >= 0xDC00 && cp <= 0xDFFF;
}

static uint32_t u16to32 (XMLCh highs, XMLCh lows)
{
    uint32_t res = highs & 0x03FF;	// mask the leading 110110
    res += lows & 0x03FF;		// mask the leading 110111
    res += 0x10000;			// add 65536
    return res;
}

static uint32_t nextcp (const XMLCh *s, size_t &x)
{
    uint32_t cp32;
    XMLCh cp = s[x];
    if (cp != 0) {
	if (is_high_surrogate (cp)) {
	    XMLCh cp1 = s[x + 1];
	    if (! is_low_surrogate (cp1)) {
		// This should never occur!
		cp32 = 0xFFFE; x += 1;
//		throw runtime_error ("Invalid UNICODE code point");
	    } else {
		cp32 = u16to32 (cp, cp1); x += 2;
	    }
	} else {
	    cp32 = cp; ++x;
	}
    } else {
	cp32 = cp;
    }
    return cp32;
}

static string u32to8 (uint32_t cp32)
{
    string res;
    unsigned cc = 0;
    uint32_t mask = 0;
    if (cp32 < 0x80) {
	// Do NOTHING here!
    } else if (cp32 >= 0x80 && cp32 <= 0x7FF) {
	// 110xxxxx 10xxxxxx
	cc = 1; mask = 0xC0;
    } else if (cp32 >= 0x0800 && cp32 <= 0xFFFF) {
	// 1110xxxx 10xxxxxx 10xxxxxx
	cc = 2; mask = 0xE0;
    } else { //cp32 >= 0x10000 && cp32 <= 0x10FFFF
	// 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
	cc = 3; mask = 0xF0;
    }
    res.resize ((size_t) cc + 1);
    while (cc > 0) {
	res[cc] = (char) (0x80 | (cp32 & 0x3F));
	cp32 >>= 6; --cc;
    }
    res[0] = mask | cp32;
    return res;
}

static size_t u32to8_size (uint32_t u32)
{
    if (u32 < 0x80) { return 1; }
    if (u32 < 0x800) { return 2; }
    if (u32 < 0x10000) { return 3; }
    return 4;
}

static size_t xml2s_size (const XMLCh *xmlstr, bool qq)
{
    size_t ix = 0, ressz = 0;
    uint32_t cp;
    while ((cp = nextcp (xmlstr, ix)) != 0) {
	if (cp < 32 || cp == 127) {
	    ressz += 3 + to_string (cp).size();
	} else if (cp < 128) {
	    switch ((char) cp) {
		case '&': ressz += 5; break;
		case '<': case '>': ressz += 4; break;
		case '"':
		    if (qq) { ressz += 6; } else { ++ressz; }
		    break;
		default:
		    ++ressz;
	    }
	} else if (cp <= 0x3FFFD) {
	    ressz += u32to8_size (cp);
	} else {
	    ressz += 3 + to_string (cp).size();
	}
    }
    return ressz;
}

string xml2s (const XMLCh *xmlstr, bool qq)
{
    size_t ix = 0;
    string res, cps;
    uint32_t cp;
    res.reserve (xml2s_size (xmlstr, qq));
    while ((cp = nextcp (xmlstr, ix)) != 0) {
	if (cp < 32 || cp == 127) {
	    res.append ("&#" + to_string (cp) + ";");
	} else if (cp < 128) {
	    switch ((char) cp) {
		case '&': res.append ("&amp;"); break;
		case '<': res.append ("&lt;"); break;
		case '>': res.append ("&gt;"); break;
		case '"':
		    if (qq) {
			res.append ("&quot;");
		    } else {
			res.push_back ('"');
		    }
		    break;
		default:
		    res.push_back ((char) cp);
		    break;
	    }
	} else if (cp <= 0x3FFFD) {
	    res.append (u32to8 (cp));
	} else {
	    res.append ("&#" + to_string (cp) + ";");
	}
    }
    return res;
}

} /*namespace GAEB*/
