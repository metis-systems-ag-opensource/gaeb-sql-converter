/* readconf.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Reading a configuration file specific to the `GAEB Reader`.
**
*/
#ifndef READCONF_H
#define READCONF_H

#include <string>

#include "Common/config.h"

struct UserPass {
    UserPass();
    UserPass (std::string &user, std::string &pass);
    UserPass (std::string &&user, std::string &&pass);
    UserPass (const UserPass &) = delete;
    UserPass (UserPass &&x);
    ~UserPass();
    void clear();
    std::string username, password;
};

Config readconf (const std::string &confname, bool with_db = false);

UserPass read_credentials (const std::string &path);

#endif /*READCONF_H*/
