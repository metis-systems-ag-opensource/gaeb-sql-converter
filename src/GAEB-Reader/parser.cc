/* parser.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Create a GAEB parser with a hidden implementation based on 'Xerces-C'.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <cstddef>

#include <forward_list>
#include <initializer_list>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

#include <xercesc/util/XMLUni.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

#include <xercesc/dom/DOM.hpp>

#include <xercesc/validators/common/Grammar.hpp>
#include <xercesc/framework/XMLGrammarPoolImpl.hpp>

#include "parser.h"

using std::forward_list;
using std::cerr, std::cout, std::endl;
using std::initializer_list;
using std::runtime_error;
using std::string, std::operator""s, std::to_string;
using std::vector;
using namespace xercesc;

namespace GAEB {

// My special error handler. The purpose of this error handler is collecting
// the error messages occurred during the parsing in the internal structure
// of the 'Parser' object (in 'ParserImpl::errors'). Each error is collected
// with the complete location data (URI of the file, line and column numbers
// error messsage, and a flag which indicates the severity of the error being
// either on warning level, or on error level. The only other member variable
// apart from 'impl' is a flag whose value is later returned (negated) by
// 'Parser::parse()'
class error_handler: public DOMErrorHandler {
public:

    error_handler () : impl(nullptr), _failed(false) { }
    error_handler (ParserImpl *impl) : impl(impl), _failed(false) {}

    void setParserData (ParserImpl *_impl) { impl = _impl; _failed = false; }

    bool failed () const { return _failed; }

    virtual bool handleError (const xercesc::DOMError&);

private:
    ParserImpl *impl;
    bool _failed;
};

// The internal structure of the 'Parser' class. The DOMLSParser used for
// doing the real parsing is *not* stored here, because these objects are
// handled internally by the Xerces-C library, which means: A DOMLSParser
// object is always constructed "on the fly" (directly before the parsing
// starts) and released (given back to the Xerces-C library) after the
// parsing is done.
struct ParserImpl {
    // The grammar cache must be kept for the complete life time of the
    // Parser object.
    XMLGrammarPool *grammarpool;

    // The generated DOM structure must be kept at least until either a new
    // parsing takes place, or the data from this structure was further
    // processed (e.g. stored in a database).
    DOMDocument *gaeb_dom;

    // The name of the GAEB file just (being parsed) is stored, because it
    // may later be used for error reporting purposes.
    string xmlfile;
    // 'errors' is a simple linked list. Together with an iterator which
    // always points to the last element of such a list, a fifo list is
    // implemented.
    ErrorList errors;
    ErrorList::iterator *last_error;

    // A small indicator for a locked grammar pool, which helps to avoid
    // producing of Xerces-C exceptions (probably).
    bool gp_locked;
};

/* Getting a DOMLSParser object from the Xerces-C library.
**
** Parser objects are handled internally by Xerces-C, meaning that there is
** no 'new ...' or 'delete ...'. Instead, a DOMImplementation object is
** retrieved from Xerces-C which itself is then used to generate a DOMLSParser
** object. But the generation of a DOMLSParser object itself is not sufficient
** without configuring it, so this step is done in this function, too, before
** (a pointer to) the DOMLSParser object can be returned.
**
** This function – as well as all the other stuff concerning the Xerces-C
** library – is hidden inside of this module for a reason: Mixing the Xerces-C
** stuff with the stuff used in the main program makes the code very unclear,
** and (additionally) error prone, which lead to the decision to hide the
** Xerces-C stuff from the "outside world".
**
*/
static DOMLSParser * newLSParser (XMLGrammarPool *grammarpool)
{
    // Define the id of the type of the DOM implementation as static 'XMLCh *'
    // string.
    const XMLCh ls_id[] = { chLatin_L, chLatin_S, chNull };

    // Create a DOMLSParser in two steps.
    DOMImplementation *impl =
	DOMImplementationRegistry::getDOMImplementation (ls_id);

    DOMLSParser *parser = impl->createLSParser
	(DOMImplementationLS::MODE_SYNCHRONOUS, 0,
	 XMLPlatformUtils::fgMemoryManager, grammarpool);

    // Configure the new parser (using commonly used values).
    DOMConfiguration* conf = parser->getDomConfig ();
    conf->setParameter (XMLUni::fgDOMComments, false);
    conf->setParameter (XMLUni::fgDOMDatatypeNormalization, true);
    conf->setParameter (XMLUni::fgDOMEntities, false);
    conf->setParameter (XMLUni::fgDOMNamespaces, true);
    conf->setParameter (XMLUni::fgDOMElementContentWhitespace, false);

    // Enable validation for this parser (including the validation of the
    // schema definitions?)
    conf->setParameter (XMLUni::fgDOMValidate, true);
    conf->setParameter (XMLUni::fgXercesSchema, true);
    conf->setParameter (XMLUni::fgXercesSchemaFullChecking, false);

    // Use the loaded grammar during parsing.
    conf->setParameter (XMLUni::fgXercesUseCachedGrammarInParse, true);

    // Don't know what value is useful here. Currently the use of other
    // (external) schema definitions is deactivated. But it may also be
    // useful to allow external schema definitions to be used (e.g. the
    // W3C schema definitions) ...
    conf->setParameter (XMLUni::fgXercesLoadSchema, false);

#if _XERCES_VERSION >= 30100
    // Xerces-C++ 3.1.0 is the first version with working multi
    // import support.
    //
    conf->setParameter (XMLUni::fgXercesHandleMultipleImports, true);
#endif

    // The DOM structure of the parsed XML document should not be released
    // by the parser (e.g. when it is destroyed), but explicitely by the
    // "user" (e.g. when the object containing this structure is destroyed).
    conf->setParameter (XMLUni::fgXercesUserAdoptsDOMDocument, true);

    return parser;
}

Parser::Parser()
{
    MemoryManager *mm = XMLPlatformUtils::fgMemoryManager;
    impl = new ParserImpl;
    impl->grammarpool = new XMLGrammarPoolImpl (mm);
    impl->gaeb_dom = nullptr;
    impl->last_error = nullptr;
    impl->gp_locked = false;
}

Parser::~Parser()
{
    if (impl->gaeb_dom) { impl->gaeb_dom->release(); }
    delete impl->grammarpool;
    impl->errors.clear();
    if (impl->last_error) { delete impl->last_error; }
    delete impl;
}

bool Parser::addSchema (const string &xsdfile, bool lockPool)
{
    if (impl->gp_locked) {
	throw runtime_error ("Grammar pool already locked");
    }
    cout << "Adding \"" << xsdfile << "\" to the grammar cache." << endl;
    error_handler eh(impl);
    DOMLSParser *parser = newLSParser (impl->grammarpool);
    parser->getDomConfig()->setParameter (XMLUni::fgDOMErrorHandler, &eh);
    bool done = parser->loadGrammar (xsdfile.c_str(),
				     Grammar::SchemaGrammarType,
				     true);
    parser->release();

    // ...
    if (done && lockPool) {
	cout << "Locking the grammar cache." << endl;
	impl->grammarpool->lockPool();
	impl->gp_locked = true;
    }

    return done;
}

void Parser::setFile (const string &xmlfile)
{
    if (impl->gaeb_dom) { impl->gaeb_dom->release(); impl->errors.clear(); }
    impl->xmlfile = xmlfile;
}

const string Parser::file() const
{
    return impl->xmlfile;
}

const ErrorList &Parser::errors() const
{
    return impl->errors;
}

bool Parser::parse (const string &xmlfile)
{
    if (impl->xmlfile.empty() && xmlfile.empty()) {
	throw runtime_error ("Parser::parse(): No input");
    }

    cout << "Beginning to parse \"" << xmlfile << "\"." << endl;
    // Set a new 'xmlfile' iff the parameter is not the empty string
    if (! xmlfile.empty()) { impl->xmlfile = xmlfile; }

    // Do a fresh start!
    if (impl->gaeb_dom) {
	cout << "Releasing a previous loaded DOM structure." << endl;
	impl->gaeb_dom->release(); impl->gaeb_dom = nullptr;
	impl->errors.clear();
	if (impl->last_error) {
	    delete impl->last_error; impl->last_error = nullptr;
	}
    }

    // Lock the grammar pool (if not already done)
    if (! impl->gp_locked) {
	cout << "Locking the grammar cache." << endl;
	impl->grammarpool->lockPool();
	impl->gp_locked = true;
    }

    // Get a new DOMLSParser from Xerces-C
    DOMLSParser *parser = newLSParser (impl->grammarpool);

    // Declare/define an error handler object and attach it to the (internal)
    // ParserImpl object.
    error_handler eh(impl);

    // Install the error handler in the parser
    parser->getDomConfig()->setParameter (XMLUni::fgDOMErrorHandler, &eh);

    cout << "Parsing \""<<xmlfile<<"\"." << endl;
    // ...
    impl->gaeb_dom = parser->parseURI (impl->xmlfile.c_str());

    // Return the the DOMLSParser object to Xerces-C
    parser->release();

    // All done
    return ! eh.failed();
}


// This error handler collects each error message found in a list of structs
// which contain all relevant information about the error. Additionally, the
// member variable '_failed' may be set (if a true error occurs).
bool error_handler::handleError (const DOMError &e)
{
    bool warn = (e.getSeverity() == DOMError::DOM_SEVERITY_WARNING);
    if (! warn) { _failed = true; }

    DOMLocator *loc = e.getLocation();

    unsigned line = loc->getLineNumber();
    unsigned column = loc->getColumnNumber();
    char *uri = XMLString::transcode (loc->getURI());
    char *msg = XMLString::transcode (e.getMessage());
    if (impl->errors.empty()) {
	impl->errors.push_front (ParserError {uri, msg, line, column, warn});
    } else {
	if (! impl->last_error) {
	    impl->last_error =
		new ErrorList::iterator (impl->errors.begin());
	}
	*impl->last_error = impl->errors.insert_after
	    (*impl->last_error, ParserError {uri, msg, line, column, warn});
//	++(*impl->last_error);
    }

    XMLString::release (&uri);
    XMLString::release (&msg);

    return true;
}

Parser createParser (const initializer_list<string> &l)
{
    bool done = true;
    const string *failed_xsd;
    Parser p;
    for (auto xsd: l) {
	if (xsd.empty()) { continue; }
	if (! p.addSchema (xsd)) { done = false; failed_xsd = &xsd; break; }
    }
    if (! done) {
	throw runtime_error ("Failed to load '" + *failed_xsd + "'");
    }
    return p;
}

Parser createParser (const vector<string> &v)
{
    bool done = true;
    const string *failed_xsd;
    Parser p;
    for (auto xsd: v) {
	if (xsd.empty()) { continue; }
	if (! p.addSchema (xsd)) { done = false; failed_xsd = &xsd; break; }
    }
    if (! done) {
	throw runtime_error ("Failed to load '" + *failed_xsd + "'");
    }
    return p;
}

GAEBDocument *Parser::get_dom (bool copy) const
{
    GAEBDocument *res = nullptr;
    if (impl->gaeb_dom) {
	if (copy) {
	    res = (GAEBDocument *) impl->gaeb_dom->cloneNode (true);
	} else {
	    res = (GAEBDocument *) impl->gaeb_dom; impl->gaeb_dom = nullptr;
	}
    }
    return res;
}

} /*namespace GAEB*/
