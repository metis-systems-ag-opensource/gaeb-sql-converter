/* readconf.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Reading a configuration file specific to the `GAEB Reader`.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <fstream>
#include <set>
#include <stdexcept>
#include <filesystem>
#include <utility>

#include "Common/confsp.h"
#include "Common/flipflop.h"
#include "Common/sutil.h"

#include "readconf.h"

using std::ifstream;
using std::set;
using std::exception, std::runtime_error;
using std::string, std::operator""s, std::to_string;

namespace fs = std::filesystem;

UserPass::UserPass (string &user, string &pass)
  : username(std::move (user)), password(std::move (pass))
{ }

UserPass::UserPass (string &&user, string &&pass)
  : username(std::move (user)), password(std::move (pass))
{ }

UserPass::UserPass (UserPass &&x)
  : username(std::move (x.username)), password(std::move (x.password))
{ }

UserPass::~UserPass()
{
    clear();
}

void UserPass::clear()
{
    for (unsigned ix = 0; ix < username.size(); ++ix) { username[ix] = 0; }
    for (unsigned ix = 0; ix < password.size(); ++ix) { password[ix] = 0; }
}

static string to_string (const set<string> &xs, string delim = ", ")
{
    flipflop<false> need_delim;
    string res;

    // Calculate the memory required for the result
    size_t xssz = xs.size();
    size_t ressz = xssz == 0 ? 0 : (xssz - 1) * delim.size();
    for (auto &x: xs) { ressz += x.size(); }

    // Reserve enough memory in `res` for holding the result. (This step
    // reduces memory consumption and/or time for allocating memory during
    // the following concatenation operations.)
    res.reserve (ressz + 1);

    // Concatenate the list elements with `delim` between them
    for (auto &x: xs) {
	if (need_delim) { res += delim; }
	res += x;
    }
    return res;
}

static void check_cfgisdir (const Config &cfg, const string &itemname)
{
    string item = cfg[itemname];
    if (! fs::exists (item)) {
	throw runtime_error ("The configuration value of " + itemname +
			     " doesn't point to anything in the filesystem");
    }
    if (! fs::is_directory (item)) {
	throw runtime_error ("The configuration value of " + itemname +
			     " doesn't point to a directory");
    }
}

static void check_mysql (const Config &cfgdata)
{
    set<string> mysql_req { "DBSERVER", "DBPORT", "DBAUTH" };
    string user, pass;
    auto [has_missing, missing] = cfgdata.check_missing (mysql_req);
    if (has_missing) {
	throw runtime_error
	    ("Missing database configuration items: " + to_string (missing));
    }
    size_t ix;
    auto &dbport = cfgdata["DBPORT"];
    unsigned long pn;
    try {
	pn = stoul (dbport, &ix);
    } catch (exception &e) {
	throw runtime_error ("Invalid port number: " + dbport);
    }
    if (ix == 0 || ix < dbport.size() || pn >= 65536) {
	throw runtime_error ("Invalid portnumber: " + dbport);
    }
    (void) read_credentials (cfgdata["DBAUTH"]);
}

UserPass read_credentials (const string &path)
{
    string line1, line2;
    ifstream cred (path);
    if (! cred.is_open()) {
	throw runtime_error
	    ("Read attempt of the authentication credentials failed.");
    }
    getline (cred, line1);
    trim (line1);
    if (line1.empty()) {
	bool ateof = cred.eof();
	cred.close();
	throw runtime_error
	    (ateof ? "Empty authentication credentials file"
		   : "Invalid authentication credentials - no username");
    }
    getline (cred, line2);
    trim (line2);
    if (line2.empty()) {
	cred.close();
	throw runtime_error
	    ("Invalid authentication credentials - no password");
    }
    cred.close();
    return UserPass (line1, line2);
}

static void check_sqlite (const Config &cfgdata)
{
    set<string> sqlite_req { "DBFILE" };
    auto [has_missing, missing] = cfgdata.check_missing (sqlite_req);
    if (has_missing) {
	throw runtime_error
	    ("Missing database configuration item: " + to_string (missing));
    }
    if (! fs::exists (cfgdata["DBFILE"])) {
	throw runtime_error
	    ("SQLite database file not found: " + cfgdata["DBFILE"]);
    }
}

static bool check_database (const Config &cfgdata)
{
    if (cfgdata.has ("DBTYPE")) {
	auto dbtype = lowercase (cfgdata["DBTYPE"]);
	if (dbtype == "mysql" || dbtype == "mariadb") {
	    check_mysql (cfgdata); return true;
	}
	if (dbtype == "sqlite" || dbtype == "sqlite3") {
	    check_sqlite (cfgdata); return false;
	}
	throw runtime_error
	    ("Unsupported database type: " + cfgdata["DBTYPE"]);
    }
    check_mysql (cfgdata); return true;
}

Config readconf (const string &confname, bool with_db)
{
    set<string> mandatory_items { "XSDBASE", "XSD", "LIB", "LIB5X",
				  "INSMETA" };
    auto [path, found] = findFile (confname);
    if (! found) {
	throw runtime_error
	    ("No configuration named \"" + confname + "\" found.");
    }
    Config cfg;
    cfg.load (path);
    auto [has_missing, missing_items] = cfg.check_missing (mandatory_items);
    if (has_missing) {
	throw runtime_error
	    ("Missing configuration items: " + to_string (missing_items));
    }
    check_cfgisdir (cfg, "XSDBASE");
    check_cfgisdir (cfg, "INSMETA");
    if (cfg.has ("XSDCACHE")) { check_cfgisdir (cfg, "XSDCACHE"); }
    if (with_db) {
	bool is_mysql = check_database (cfg);
	if (! is_mysql) {
	    throw runtime_error
		("SQLite type databases are currently unsupported.");
	}
    }
    return cfg;
}
