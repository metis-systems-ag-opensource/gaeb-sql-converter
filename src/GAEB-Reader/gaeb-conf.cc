/* gaeb-conf.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Display (parts of) the configuration file of 'gaeb-reader' and 'gaebsd'.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <cstring>
#include <iostream>
#include <map>
#include <stdexcept>
#include <string>
#include <vector>

#include "Common/confsp.h"
#include "Common/config.h"
#include "Common/flipflop.h"
#include "Common/sutil.h"
#include "Common/sysutils.h"

// My special mechanism(s) for either writing the generated SQL statements to
// a file (or to the standard output), or for using these SQL statements to
// store the data directly in a database.

//namespace fs = std::filesystem;
//using std::ofstream;
using std::cerr, std::cout, std::endl, std::flush;
//using std::ostream;
using std::exception, std::runtime_error;
using std::string, std::operator""s, std::to_string;
//using std::pair;

[[noreturn]] void usage()
{
    cerr << "Usage: " << appname() << " -h|--help" << endl <<
	    "       " << appname() << " `tag` -p|--path|--pathname" << endl <<
	    "       " << appname() << " `tag` -l|--list|--list-items" << endl <<
	    "       " << appname() << " `tag` `item-name`..." << endl <<
	    "       " << appname() << " `tag` --has `item-name`" << endl <<
	    endl <<
	    "Options/Arguments:" << endl <<
	    "  -h (alt: --help)" << endl <<
	    "    Write this usage message to the standard error stream and"
	    " terminate." << endl <<
	    "  --has `item-name`" << endl <<
	    "    Returns true (0) if the item `item-name` exists, and false"
	    " (1), otherwise." << endl <<
	    "  -p (alt: --path, --pathname)" << endl <<
	    "    Print the pathname of the configuration file." << endl <<
	    "  -l (alt: --list, --list-items)" << endl <<
	    "    List all available configuration items." << endl <<
	    "  `item-name`..." << endl <<
	    "    Print the values of the selected items `item-name`..." <<
	    "  `tag`" << endl <<
	    "    Select the configuration file of either 'gaeb-sd'"
	    " (`tag` == 'sd')" << endl <<
	    "    or 'gaeb-reader' (`tag` == 'rd')" << endl <<
	    endl;
    exit (0);
}

[[noreturn]] void usage (const string &msg)
{
    if (msg.empty()) { usage(); }
    cerr << appname() << ": " << msg << endl;
    exit (64);
}

/* Compare two C-strings (letter case agnostic) and return 'true' if both
** string match, and 'false', otherwise.
*/
static
bool cseq (const char *l, const char *r)
{
    return strcasecmp (l, r) == 0;
}

using csvec = std::vector<const char *>;
using lcmap = std::map<string, string>;

/* `csin` checks it an C-string matches any element in a list (std::vector)
** of C-strings.
*/
static
bool csin (const char *p, const csvec &sv)
{
    size_t svsz = sv.size();
    for (size_t ix = 0; ix < svsz; ++ix) {
	if (cseq (p, sv[ix])) { return true; }
    }
    return false;
}

/* `gaeb-conf` should be agnostic for all items, meaning:
**   - No mandatory item checking,
**   - No checks for the existence of any pathnames.
** In other words: Read only the configuration file (if is exists) and
** return the corresponding `Config` object.
*/
static
Config readconf (const string &confname)
{
    auto [path, found] = findFile (confname);
    if (! found) {
	throw runtime_error
	    ("No configuration named \"" + confname + "\" found.");
    }
    Config cfg;
    cfg.load (path);
    return cfg;
}

int main (int argc, char *argv[])
{

    // I want 'bool' values to be written as 'true' / 'false' on 'cerr'!
    cerr.setf (std::ios::boolalpha);

    int optx = 1;

    // Map which associates a `tag` with a configuration name.
    const std::map<string, string> tag2conf {
	{ "rd", "gaeb-reader" },
	{ "sd", "gaebsd" },
    };

    if (optx >= argc) { usage ("Missing argument(s)"); }

    // `-h` (or `--help`) is checked before any other argument.
    if (csin (argv[optx], csvec ({ "-h", "--help" }))) { usage(); }

    // Get the `tag` argument and determine the configuration name from it.
    const char *tag = argv[optx++];
    auto it = tag2conf.find (tag);
    if (it == tag2conf.end()) {
	usage ("Invalid `tag`. Try '" + appname() + " -h' for help, please!");
    }
    const string &confname = it->second;

    // Need at least one more argument!
    if (optx >= argc) { usage ("Missing argument(s)"); }

    Config cfg;
    try {
	cfg = readconf (confname);
    } catch (exception &e) {
	cerr << appname() << ": Reading `" << tag <<
		"` configuration failed - " << e.what() << endl;
	return 1;
    }

    if (csin (argv[optx], csvec ({ "-p", "--path", "--pathname" }))) {
	cout << cfg.filename() << endl; return 0;
    } else if (csin (argv[optx], csvec ({ "-l", "--list", "--list-items" }))) {
	auto cfitems = cfg.item_names();
	flipflop<false> needcomma;
	for (auto &ci: cfitems) {
	    if (needcomma) { cout << " "; }
	    cout << ci;
	}
	cout << endl; return 0;
    } else if (csin (argv[optx], csvec ({ "--has" }))) {
	++optx;
	if (optx >= argc) { usage ("'--has' requires an argument."); }
	string item = argv[optx];
	for (auto &it: cfg.item_names()) {
	    if (lccmp (it, item) == 0) { return 0; }
	}
	return 1;
    } else {
	int ec = 0;
	lcmap lc2item;
	for (auto &it: cfg.item_names()) {
	    lc2item.emplace (lowercase (it), it);
	}
	if (optx + 1 >= argc) {
	    const char *item_cs = argv[optx];
	    try {
		string item = lc2item.at (lowercase (item_cs));
		cout << cfg[item] << endl;
	    } catch (exception &e) {
		// This is a special case. If an item does not exist, no error
		// message is issued. Only the error cunter is increased. This
		// allows for a simultaneous variable setting and error
		// checking in a shell (like the Bash), like in:
		//   if varname="$(gaeb-conf `tag` item)"; then
		//     ...
		++ec;
	    }
	} else {
	    for (int ix = optx; ix < argc; ++ix) {
		const char *item_cs = argv[ix];
		try {
		    string item = lc2item.at (lowercase (item_cs));
		    cout << item << " " << cfg[item] << endl;
		} catch (exception &e) {
		    cerr << appname() << ": '" << item_cs <<
			    "' no such item." << endl;
		    ++ec;
		}
	    }
	}
	return (ec > 0 ? 1 : 0);
    }
}
