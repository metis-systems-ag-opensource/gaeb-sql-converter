/* parser.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Interface part of the 'parser.{cc,h}' module. Here, the Parser class is
** defined, but its implementation (member data and functions) is hidden.
** This allows for a separation of the ugly implementation details ('Xerces_C')
** from the implementation of the main program, which mostly uses standard C++
** features and some of the modules already used in the 'ifc-server' project.
*/
#ifndef PARSER_H
#define PARSER_H

#include <forward_list>
#include <initializer_list>
#include <string>
#include <vector>

namespace GAEB {

struct ParserImpl;

struct ParserError {
    std::string uri, msg;
    unsigned line, column;
    bool warn;
};

using ErrorList = std::forward_list<ParserError>;

class GAEBDocument;

class Parser {
public:
    Parser();
    ~Parser();

    // Add an XSD file.
    bool addSchema (const std::string &xsdfile, bool lockPool = false);

    void setFile (const std::string &xmlfile);

    const std::string file() const;

    const ErrorList &errors() const;

    // parse a given XML file
    bool parse (const std::string &xmlfile = "");

    GAEBDocument *get_dom (bool copy = false) const;

private:
    ParserImpl *impl;
};

Parser createParser (const std::initializer_list<std::string> &l);

Parser createParser (const std::vector<std::string> &v);

} /*namespace GAEB*/

#endif /*PARSER_H*/
