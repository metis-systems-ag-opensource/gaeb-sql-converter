/* sw-test.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Test program for 'swriter.{cc,h}'
**
*/

#include <iostream>

#include "Common/sutil.h"
#include "swriter.h"

using namespace GAEB;

int main()
{
    SWriter sw = SWriter (std::cout);
    Writer &w = sw;

    w.str ("Hello") . str (" ") . str ("World") . commit();

    return 0;
}
