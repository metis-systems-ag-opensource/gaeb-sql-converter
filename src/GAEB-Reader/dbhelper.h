/* dbhelper.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** A small helper class for collecting data as (name, value) pairs and then
** generating an INSERT statement from the collected data.
**
*/
#ifndef DBHELPER_H
#define DBHELPER_H

#include <iostream>
#include <string>
#include <utility>
#include <vector>

namespace GAEB {

// A small helper class which helps to make the intention of generating an
// INSERT-statement from two lists of values (names, values) a bit more clear.
struct InsStmt {
    InsStmt (const std::string &tblname, size_t prealloc = 64);
    std::pair<std::string &, std::string &> operator[] (unsigned ix);
    void clear();
    InsStmt &addColumn (const std::string &name,
			const std::string &value,
			char t);
    InsStmt &addColumn (const std::string &name, long index);
    std::string generate() const;
    operator std::string() const;
    size_t size() const;
private:
    std::string tblname;
    std::vector<std::string> colnames;
    std::vector<std::string> colvalues;
};

std::string db_trans (const std::string &s);

std::ostream &operator<< (std::ostream &out, const InsStmt &im);

} /*namespace GAEB*/

#endif /*DBHELPER_H*/
