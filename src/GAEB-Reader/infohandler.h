/* infohandler.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Simple action rutines for catching specific data from a GAEB file.
**
*/

#include <string>

#include <xercesc/sax2/Attributes.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>

namespace GAEB {

XERCES_CPP_NAMESPACE_USE

class InfoHandlerImpl;

class InfoHandler : public DefaultHandler {
public:
    InfoHandler();
    ~InfoHandler();

    // SAX ContentHandler interface functions. The functions not declared
    // here are inherited from the 'DefaultHandler', where they are declared
    // as `virtual` (but **not** *pure* `virtual`).
    void startElement (const XMLCh *const uri,
		       const XMLCh *const localname,
		       const XMLCh *const qname,
		       const Attributes &attrs);
    void endElement (const XMLCh *const uri,
		     const XMLCh *const localname,
		     const XMLCh *const qualname);
    void characters (const XMLCh *text, XMLSize_t textlen);
    void ignorableWhitespace (const XMLCh* const chars,
			      const XMLSize_t length);
    void startDocument();

    // SAX ErrorHandler interface functions.
    void warning(const SAXParseException& exc);
    void error(const SAXParseException& exc);
    void fatalError(const SAXParseException& exc);
    void resetErrors();
    void reset();

    bool have_version() const;
    const std::string &version() const;

    bool have_versiondate() const;
    const std::string &versiondate() const;

    bool have_type() const;
    const std::string &type() const;

    bool have_prjname() const;
    const std::string &prjname() const;

    bool have_date() const;
    const std::string &date() const;

    bool have_time() const;
    const std::string &time() const;

private:
    InfoHandlerImpl *impl;
}; /*InfoHandler*/

} /*namespace GAEB*/
