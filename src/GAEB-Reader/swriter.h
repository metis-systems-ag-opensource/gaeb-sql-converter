/* swriter.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** `std::ostream` implementation of the "interface" `Writer` (interface part).
**
*/
#ifndef SWRITER_H
#define SWRITER_H

#include <ostream>
#include <string>

#include "writer.h"

namespace GAEB {

class SWriter : public Writer {
public:
    SWriter (std::ostream &outs);
    SWriter (const std::string &file);
    SWriter (const SWriter &) = delete;
    SWriter (SWriter &&src);
    ~SWriter();
    SWriter &dset (const std::string &data);
    std::string get_id();
    SWriter &str (const std::string &msg);
    SWriter &commit();
    SWriter &operator= (const Writer &) = delete;
    SWriter &operator= (Writer &&src);
private:
    void close();
    std::string buf, filename;
    std::ostream *_out;
    bool id_set;
};

} /*namespace GAEB*/

#endif /*SWRITER_H*/
