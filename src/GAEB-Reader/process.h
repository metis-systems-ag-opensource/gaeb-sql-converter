/* process.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Interface part of 'process.{cc,h}'.
** (Processing a valid GAEB DOM structure.)
**
*/
#ifndef PROCESS_H
#define PROCESS_H

#include <ostream>

#include "getinfo.h"
#include "insmeta.h"
#include "writer.h"

namespace GAEB {

class GAEBDocument;

void process_gaeb (GAEBDocument *gaeb, const Info &info, InsMeta &meta,
		   Writer &out);

} /*namespace GAEB*/

#endif /*PROCESS_H*/
