/* getinfo.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Implementation part.
** (Get the version and type of the schema to be used from the GAEB document
**  in a first pass)
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <fstream>
#include <stdexcept>

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>

#include <xercesc/util/OutOfMemoryException.hpp>

#include "tostdstring.h"
#include "infohandler.h"

#include "getinfo.h"

using std::ifstream, std::endl, std::flush;
using std::runtime_error;
using std::string, std::operator""s, std::to_string;

namespace GAEB {

Info getinfo (const char *filename)
{
    SAX2XMLReader *parser = XMLReaderFactory::createXMLReader();

    parser->setFeature (XMLUni::fgSAX2CoreNameSpaces, true);
    parser->setFeature (XMLUni::fgXercesSchema, true);
    parser->setFeature (XMLUni::fgXercesHandleMultipleImports, true);
    parser->setFeature (XMLUni::fgXercesSchema, false);
    parser->setFeature (XMLUni::fgXercesSchemaFullChecking, false);
    parser->setFeature (XMLUni::fgXercesIdentityConstraintChecking, true);
    parser->setFeature (XMLUni::fgSAX2CoreNameSpacePrefixes, false);

    parser->setFeature (XMLUni::fgSAX2CoreValidation, false);

    InfoHandler info;
    parser->setContentHandler (&info);
    parser->setErrorHandler (&info);

    try {
	parser->parse (filename);

    } catch (const OutOfMemoryException &e) {
	throw runtime_error ("Out of memory exception caught.");
    } catch (const XMLException &e) {
	string msg = "Parse error in '"s + filename + "': " +
		     to_string (e.getMessage());
	throw runtime_error (msg);
    } catch (...) {
	throw runtime_error
	    ("Unexpected problem found while processing '"s + filename + "'");
    }
    if (! info.have_version()
    ||  ! info.have_versiondate()
    ||  ! info.have_type()) {
	string msg = "The information for selecting the XSD specifications is"
		     " incomplete. Please check your GAEB file.";
	throw runtime_error (msg);
    }
    return Info { info.version(), info.versiondate(), info.type(),
		  info.prjname(), info.date(), info.time()
		};
}

} /*namespace GAEB*/
