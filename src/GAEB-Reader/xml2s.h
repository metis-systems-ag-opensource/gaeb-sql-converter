/* xml2s.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Interface part of 'xml2s.{cc,h}'
** (Convert an XML string (Xerces-C 'XMLCh *') into a 'std::string' (including
**  the conversion of special characters into XML entities).)
**
*/
#ifndef XML2S_H
#define XML2S_H

#include <string>

#include <xercesc/util/XercesDefs.hpp>

namespace GAEB {

std::string xml2s (const XMLCh *xmlstr, bool qq = true);

} /*namespace GAEB*/

#endif /*XML2S_H*/
