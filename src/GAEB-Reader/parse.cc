/* parse.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Parse a GAEB file into a DOM structure, using a list of given XSD files
** for validation.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <cerrno>
#include <cstdint>
#include <iostream>
#include <stdexcept>
#include <vector>

#include "Common/sutil.h"

#include "parser.h"
#include "xsdpath.h"

#include "parse.h"

namespace GAEB {

using std::cerr, std::cout, std::flush, std::endl;
using std::exception, std::runtime_error;
using std::string;
using std::vector;

// A little bit more clear what is meant ...

enum ErrLevel : bool { el_errors = false, el_warnings = true };

static void print_errors  (const ErrorList &errors,
			   ErrLevel errorlevel,
			   const string indent = "")
{
    for (auto err: errors) {
	if (err.warn != errorlevel) { continue; }
	cerr << indent <<
		"Line " << err.line <<
		", column " << err.column <<
		": " << err.msg <<
		endl;
    }
}

GAEBDocument *parse_file (const Config &cfg,
			  const Info &vtinfo,
			  const string &gaeb_file)
{

    // Pathname of the required XSD files
    string xsdlib, xsdlib5x, xsdda;
    int errnum;

    // Constructing the pathnames of the required XSD files. This is done by
    // invoking 'xsdpath()' (from 'xsdpath.{cc,h}'). Only the pathname (as well
    // as the existance) of the 'GAEB_DA_XML_<NUM>...' file is mandatory.
    tie (xsdda, errnum) = xsdpath (cfg, vtinfo);
    if (errnum) { throw runtime_error (xsdda); }

    // The XSD library files' pathnames are constructed only if the
    // corresponding XSD files exist. The only situation which leads to an
    // exception here is if the files pointed to (by the resolved template;
    // see 'gaeb-reader.conf') points to something other than a regular file.
    // The non-existance of a XSD library file on the other hand is no error,
    // because some XSD files (like the "Raumbuch" XSD) don't refer any extra
    // library XSD files.
    tie (xsdlib, errnum) = xsdpath (cfg, vtinfo, "lib");
    if (errnum) {
	if (errnum == EINVAL) { throw runtime_error (xsdlib); }
	xsdlib = "";
    }
    // In newer versions of (some of) the "Kosten and Kalkulation" XSDs, there
    // is a second XSD library file, whose pathname is loconstructed here.
    tie (xsdlib5x, errnum) = xsdpath (cfg, vtinfo, "lib5x");
    if (errnum) {
	if (errnum == EINVAL) { throw runtime_error (xsdlib5x); }
	// See above, please!
	xsdlib5x = "";
    }

    // Create a parser which uses the given schema definitions.
    Parser gaeb_parser = createParser ({ xsdda, xsdlib, xsdlib5x });

    /* Parse the file into a DOM structure. This is done within a
    ** 'try'/'catch' block, because 'Parser::parse()' throws an exception
    ** on FATAL errors (which ave nothing to do with the Xerces-C library).
    */
    try {
	cout << "Loading (parsing/validating) \"" << gaeb_file << "\"" << endl;
	bool ok = gaeb_parser.parse (gaeb_file);
	if (! ok) {
	    cerr << "Parsing failed. Errors:" << endl;
	    print_errors (gaeb_parser.errors(), el_errors, "  ");
	} else {
	    auto &errlist = gaeb_parser.errors();
	    if (! errlist.empty()) {
		// Caught some warnings.
		cerr << "Parsing completed with warnings:" << endl;
		print_errors (errlist, el_warnings, "  ");
	    } else {
		cout << "Parsing complete, no errors." << endl;
	    }
	}
    } catch (exception &e) {
	/*...*/
	cerr << "Caught an exception during parsing. This is a FATAL error." <<
		endl << "Message: " << e.what() << endl;
	throw;
    }

    return gaeb_parser.get_dom();
}

} /*namespace GAEB*/
