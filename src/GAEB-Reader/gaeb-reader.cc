/* gaeb-reader.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Parser which returns the DOM tree as an ADT which then will be
** traversed in another module.
**
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <filesystem>
#include <fstream>
#include <iostream>
#include <ostream>
#include <stdexcept>
#include <string>
#include <utility>

#include "Common/dbif.h"
#include "Common/pathcat.h"
#include "Common/sysutils.h"
#include "finit.h"
#include "getinfo.h"
#include "parse.h"
#include "process.h"
#include "readconf.h"
#include "readmeta.h"

// My special mechanism(s) for either writing the generated SQL statements to
// a file (or to the standard output), or for using these SQL statements to
// store the data directly in a database.
#include "writer.h"
#include "dbwriter.h"
#include "swriter.h"

namespace fs = std::filesystem;
using std::ofstream;
using std::cerr, std::cout, std::endl, std::flush;
using std::ostream;
using std::exception, std::runtime_error;
using std::string, std::operator""s, std::to_string;
using std::pair;

[[noreturn]] void usage()
{
    cerr << "Usage: " << appname() << " [--to-db] GAEB-file [outfile]" << endl;
    exit (0);
}

[[noreturn]] void usage (const string &msg)
{
    if (msg.empty()) { usage(); }
    cerr << appname() << ": " << msg << endl;
    exit (64);
}

int main (int argc, char *argv[])
{
    GAEB::Writer *w = nullptr;
    GAEB::GAEBDocument *gaeb_dom = nullptr;

    // I want 'bool' values to be written as 'true' / 'false' on 'cerr'!
    cerr.setf (std::ios::boolalpha);

    if (argc < 2) { usage(); }

    int optx = 1; bool to_db = false;
    if (argv[optx] == "--to-db"s) { to_db = true; ++optx; }

    if (optx >= argc) { usage ("Missing argument(s)"); }

    Config cfg;
    try {
	cfg = readconf ("gaeb-reader", to_db);
    } catch (exception &e) {
	cerr << appname() << ": Reading configuration failed - " <<
		e.what() << endl;
	return 1;
    }

    const char *gaebfile = argv[optx++];
    const char *outfile = (optx < argc ? argv[optx] : nullptr);

    if (! to_db && outfile && fs::exists (outfile)) {
	cerr << appname() << ": File \"" << outfile <<
		"\" already exists." << endl;
	GAEB::finalise();
	return 2;
    }

    /* Initialise all parts of the 'GAEB Reader' which require initialisation
    ** – such as the 'Xerces-C' library.
    */
    GAEB::initialise();

    cout << "Pre-parsing: Getting GAEB file meta information." << endl;
    // This first step (pass) is necessary, because the GAEB phase is unknown
    // (yet), but a complete parsing (including verification) can only be done
    // with the XML-Schema definitions for this GAEB file knwon (Xerces-C
    // doesn't implement a mechanism for verifying an already DOMified
    // document). The GAEB phase is specified in the GAEB-file itself, as
    // content of the '<DP>' element (below the '<Award>' element).
    // Additionally, the schema version used is the content of the '<Version>'
    // element (below the '<GAEBInfo>' element).
    GAEB::Info vtinfo;
    try {
	vtinfo = GAEB::getinfo (gaebfile);
	cout << "The version is:      " << vtinfo.version << endl;
	cout << "The version date is: " << vtinfo.versiondate << endl;
	cout << "The type is:         " << vtinfo.type << endl;
	cout << "Project name:        " << vtinfo.prjname << endl;
	cout << "File date:           " << vtinfo.date << endl;
	cout << "File time:           " << vtinfo.time << endl;
    } catch (exception &e) {
	cerr << appname() << ": FATAL ERROR! " << e.what() << endl;
	GAEB::finalise();
	return 127;
    }

    cout << "Constructing insertion meta pathname ..." << endl;
    // Constructing the pathname of the insertion metadata file.
    string metafile = cfg["INSMETA"] /
		      (vtinfo.version + "_" + vtinfo.versiondate) /
		      ("x" + vtinfo.type + ".is");
    cout << "Result: " << metafile << endl;

    cout << "Reading insertion metadata..." << endl;
    // Parsing 'metafile', returning an extended version of the insertion
    // metadata in 'insmeta'.
    GAEB::InsMeta insmeta;
    try {
	/* Reading the insertion metadata */
	auto [im, im_ok] = GAEB::readmeta (metafile);
	if (! im_ok) {
	    throw runtime_error
		("Errors found in metadata file \"" + metafile + "\".");
	}
	insmeta = std::move (im);
    } catch (exception &e) {
	cerr << appname() << ": " << e.what() << endl;
	GAEB::finalise();
	return 1;
    }

    cout << "Parsing '" << gaebfile << "' ..." << endl;
    // Parsing the GAEB file, returning an opaque structure (via a pointer).
    gaeb_dom = GAEB::parse_file (cfg, vtinfo, gaebfile);

    ofstream out;
    if (to_db) {
	try {
	    w = new GAEB::DBWriter (cfg["DBSERVER"], stoi (cfg["DBPORT"]),
				    cfg["DBAUTH"]);
	} catch (exception &e) {
	    cerr << appname() << ": " << e.what() << endl;
	    GAEB::finalise(); return 2;
	}
    } else if (outfile) {
	cout << "Writing data to output file: " << outfile << endl;
	try {
	    w = new GAEB::SWriter (outfile);
	} catch (exception &e) {
	    cerr << appname() << ": " << e.what() << endl;
	    GAEB::finalise(); return 2;
	}
    } else {
	cerr << "Writing data to the standard output channel" << endl;
	w = new GAEB::SWriter (cout);
    }

    cout << "Processing GAEB DOM structure ..." << endl;
    // Processing the DOM output from the parser, using 'insmeta' to create
    // INSERT statements.
    try {
	GAEB::process_gaeb (gaeb_dom, vtinfo, insmeta, *w);
    } catch (exception &e) {
	cerr << appname() << ": Processing GAEB failed - " << e.what() << endl;
	delete w; GAEB::finalise(); return 1;
    }

    if (to_db) {
	cout << "Closing database connection" << endl;
    } else if (outfile) {
	cout << "Closing file \"" << outfile << "\"" << endl;
    }
    delete w; GAEB::finalise();

    return 0;
}
