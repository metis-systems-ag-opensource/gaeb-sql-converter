/* gaeb-info.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Determine the GAEB version, version date, and phase from a list of GAEB
** files and write them (together with the pathnames of the corresponding
** GAEB files) to the standard output, using the format
**
**   <GAEB version>_<GAEB version date>/<GAEB phase>\t<GAEB file pathname>
**
** This helper program may help in ordering GAEB files by their versions and
** phase-numbers.
*/

#include "debug.h"

#ifdef DEBUG
// '#include' and '#define' statements or declarations concerning debugging
#endif /*DEBUG*/

#include <iostream>
#include <stdexcept>
#include <string>
#include <tuple>
#include <utility>
#include <xercesc/util/PlatformUtils.hpp>

#include "progname.h"
#include "finit.h"
#include "getinfo.h"

using std::cerr, std::cout, std::endl, std::flush;
using std::exception;
using std::string, std::operator""s, std::to_string;
using std::tie;
//using namespace xercesc;

int main (int argc, char *argv[])
{
    progname prog (*argv);

    if (argc < 2) {
	cout << "Usage: " << (string) prog << " GAEB-file" << endl;
	return 0;
    }

    const char *filename = argv[1];

    GAEB::initialise();

    GAEB::Info vtinfo;
    try {
	vtinfo = GAEB::getinfo (filename);
    } catch (exception &e) {
	cerr << "Errors found:" << endl << e.what() << endl;
	GAEB::finalise();
	return 1;
    }
    GAEB::finalise();
    cout << vtinfo.version << '_' << vtinfo.versiondate << '/' <<
	    vtinfo.type << '\t' << filename << endl;
    return 0;
}
