/* swriter.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** `std::ostream` implementation off the "interface" `Writer`
** (implementation part)
**
*/

#include <stdexcept>
#include <fstream>
#include <iostream>
#include <utility>

#include "Common/sutil.h"

#include "swriter.h"

namespace GAEB {

using std::runtime_error;
using std::flush;
using std::ofstream;
using std::string;

SWriter::SWriter (const string &file)
  : buf(), filename(file), _out(nullptr), id_set(false)
{
    ofstream *of = new ofstream (filename);
    if (! of->is_open()) {
	throw runtime_error ("Opening \"" + file + "\" failed.");
    }
    _out = of;
}

SWriter::SWriter (std::ostream &outs)
  : buf(), filename(), _out(&outs), id_set(false)
{
    buf.reserve (8192);
}

SWriter::SWriter (SWriter &&src)
  : buf(std::move (src.buf)), _out(src._out), id_set(src.id_set)
{
    src._out = nullptr; src.id_set = false;
}

SWriter::~SWriter()
{
    close();
}

SWriter &SWriter::dset (const string &data)
{
    return str ("USE ") . str (data) . commit();
}

string SWriter::get_id()
{
    if (! id_set) {
	str ("SET @id = LAST_INSERT_ID()") . commit();
    }
    return "@id";
}

SWriter &SWriter::str (const string &msg)
{
    if (! _out) { throw runtime_error ("SWriter closed"); }
    buf += msg;
    return *this;
}

SWriter &SWriter::commit()
{
    if (! _out) { throw runtime_error ("SWriter closed"); }
    if (! buf.empty()) {
	size_t lnws = buf.find_last_not_of (" \t" EOL);
	if (lnws != string::npos) {
	    (*_out) << buf;
	    if (buf[lnws] != ';') { (*_out) << ";" EOL; }
	    _out->flush();
	}
	buf.clear();
    }
    return *this;
}

SWriter &SWriter::operator= (Writer &&src)
{
    SWriter &s = dynamic_cast<SWriter &>(src);
    buf = std::move (s.buf);
    _out = s._out; s._out = nullptr;
    id_set = s.id_set; s.id_set = false;
    return *this;
}

void SWriter::close()
{
    if (_out) {
	commit();
	if (! filename.empty()) { delete _out; }
	_out = nullptr;
	id_set = false;
    }
}

} /*namespace GAEB*/
