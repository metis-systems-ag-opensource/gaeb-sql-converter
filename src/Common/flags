#! /bin/sh
# config
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@metis-ag.com
# Copyright: (c) 2020, Metis AG
# License: MIT License
#
# Small shell script which returns extra includes and/or libs required by
# programs which use '-lcommon'

PROG="${0##*/}"

ext_packages="libmariadb"

show_cflags=false
show_libs=false
show_Lflags=false

if [ $# -lt 1 ]; then
    cat <<-EOT
	Usage: $PROG --cflags
	       $PROG --libs|--lflags|--libs-only-L
	       $PROG --cflags --libs|--lflags|--libs-only-L
	EOT
    exit 0
fi
    
while [ $# -gt 0 ]; do
    opt="$1"; shift
    case "$opt" in
	--cflags)      show_cflags=true ;;
	--libs)        show_libs=true ;;
	--libs-only-L|--lflags) show_Lflags=true ;;
	*) echo 1>&2 "${PROG}: Invalid argument '$opt'"; exit 64 ;;
    esac
done

if $show_libs && $show_Lflags; then show_Lflags=false; fi

#if ! $show_cflags && ! $show_libs; then
#    echo "${PROG}: You must specify at least one of '--cflags', '--libs'"
#    exit 64
#fi

cflags=
libs=
if $show_cflags; then
    for pkg in $ext_packages; do
	cflags="$cflags${cflags:+ }$(pkg-config $pkg --cflags)"
    done
fi
if $show_Lflags; then
    for pkg in $ext_packages; do
	libs="$libs${libs:+ }$(pkg-config $pkg --libs-only-L)"
    done
elif $show_libs; then
    for pkg in $ext_packages; do
	libs="$libs${libs:+ }$(pkg-config $pkg --libs)"
    done
fi

echo "$cflags${cflags:+ }$libs"
