/* flipflop.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Small 'flipflop' template type ...
**
*/
#ifndef FLIPFLOP_H
#define FLIPFLOP_H

template<bool F>
class flipflop {
public:
    flipflop() : v(F) { }
    operator bool() { bool x = v; v = ! F; return x; }
    void reset() { v = F; }
private:
    bool v;
};

#endif /*FLIPFLOP_H*/
