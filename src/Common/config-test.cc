/* config-test.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Small test program for 'config.{cc,h}'
**
*/

#include <iostream>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>

#include "config.h"
#include "flipflop.h"
#include "sysutils.h"

using Items = std::vector<std::string>;
using ItemSet = std::set<std::string>;

using std::exception;
using std::cerr, std::cout, std::endl, std::flush;
using std::string, std::operator""s, std::to_string;

int main (int argc, char *argv[])
{
    string prog = appname();
    Config cfg;
    Items itlist;
    ItemSet items_got;

    if (argc < 2) {
	cout << "Usage: " << prog << " <configuration file>" << endl;
	return 0;
    }
    char *cfgfile = argv[1];

    itlist.reserve (16);
    for (int ix = 2; ix < argc; ++ix) {
	char *arg = argv[ix];
	auto [it, done] = items_got.insert (arg);
	if (done) { itlist.push_back (arg); }
    }
    itlist.shrink_to_fit();

    try {
	cfg.load (cfgfile);
    } catch (exception &e) {
	cerr << prog << ": " << e.what() << endl;
	return 1;
    }

    if (itlist.empty()) {
	auto &cfd = cfg.data();
	cout << "Dumping all elements:" << endl;
	for (auto &[k, v]: cfd) {
	    cout << "  " << k << " = " << v << endl;
	}
    } else {
	flipflop<false> needcomma;
	cout << "Dumping [";
	for (string &it: itlist) {
	    if (needcomma) { cout << ", "; }
	    cout << it;
	}
	cout << "]" << endl;
	for (string &it: itlist) {
	    if (cfg.has (it)) {
		cout << "  " << it << " = " << cfg.at (it) << endl;
	    } else {
		cout << "  " << it << ": No such item" << endl;
	    }
	}
    }

    return 0;
}
