/*! \file sutil.h
**  Interface part of the module `sutil.{cc,h}`.
**
**  This module exports some utility functions, macros and types for strings
**  (`std::string` and `const char *`).
*/
/* $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'sutil.cc'
** (Small 'std::string' and 'char *' utilities ...)
**
*/
#ifndef SUTIL_H
#define SUTIL_H

#include <map>
#include <string>
#if __cplusplus >= 201703L
#include <string_view>
#endif

// Defining some macros for the standard *end of line* character sequences.
#define LF "\12"	//!< A standard line feed (ASCII)
#define CR "\15"	//!< A standard carriage return (ASCII)
#define CRLF "\15\12"	//!< The classical CR/LF combination

/*! Defining the macro `EOL` as the *end of line` sequence of the operating
**  system the programs using this module are compiled for.
*/
#if defined(__WIN32__) || defined(__win64__)
// The Windows `EOL` is the traditional *carriage return*, followed by
// *line feed*
#  define EOL CRLF
#else
// All other OS (especially Unix and Linux) use a single *line feed* as `EOL`.
#  define EOL LF
#endif

/*! `is_prefix(probe, string)` tests if `probe` is a prefix of `string`,
** returning `true` in this case and `false`, otherwise. If the third parameter
** is given as `true`, `probe` must be a true prefix of `string`, meaning:
** an equality of `probe` and `string` leads to a `false` result.
*/
bool is_prefix (const std::string &p, const std::string &s,
		bool true_prefix = false, bool nocase = true);

/*! `is_suffix(probe, string)` tests if `probe` is a suffix of `string`,
** returning `true` in this case and `false`, otherwise. If the third parameter
** is given as `true`, `probe` must be a true suffix of `string`, meaning:
** an equality of `probe` and `string` leads to a `false` result.
*/
bool is_suffix (const std::string &p, const std::string &s,
		bool true_suffix = false, bool nocase = true);

/*! `uppercase(string)` returns a copy of the `std::string` argument `string`
**  with all lower case letters (**ASCII only**) converted to upper case.
*/
std::string uppercase (const std::string &s);

/*! `makeupper(s)` converts all lower case letters (**ASCII only**) to
**  upper case.
*/
void makeupper (std::string &s);

/*! `lowercase(string)` returns a copy of the `std::string` argument `string`
**  with all upper case letters (**ASCII only**) converted to lower case.
*/
std::string lowercase (const std::string &s);

/*! `makelower(s)` converts all upper case letters (**ASCII only**) to
**  lower case.
*/
void makelower (std::string &s);

/*! `ucfirst(string)` returns a copy of `string` where the first letter of
**  each word is converted to upper case and the remaining characters are
**  converted to lower case (again: **ASCII only**. A word begins with either
**  a letter or a digit and continues with a sequence of letters and/or digits.
**  Any other character (including `'_'`) terminates a word.
*/
std::string ucfirst (const std::string &s);

/*! `lccmp(l, r)` compares the two C strings (`const char *`) `l` and `r` in a
**  letter case independent manner (**ASCII only**), returning a negative value
**  if `l` comes lexically before `r`, `0` if both strings are equal, and a
**  positive value if `l` comes lexically after `r`.
**
**  \remark
**  I once wrote this function, because the function `strcasecmp()` (which does
**  the same) was not always available, and this function was always easy to
**  implement. Even up today, i'm using one or another variant of this function
**  in programs i'm writing.
*/
int lccmp (const char *l, const char *r, size_t len = 0);

/*! `lccmp(l, r)` compares the two `std::string` values `l` and `r` in a letter
**  case independent manner (**ASCII only**).
**
**  \remark
**  This is another variant of `lccmp()` which is implemented in pure C++
**  (using `std::string` instead of `const char *`).
*/
int lccmp (const std::string &l, const std::string &r, size_t len = 0);

/*! `Substitutes` is a simple type alias which is used in `tmpl_subst()` (see
**  below). It is mainly used for declaring a variable containing the
**  replacement values for placeholders in the simple template mechanism which
**  is used by `tmpl_subst()`.
*/
using Substitutes = std::map<std::string, std::string>;

#if __cplusplus >= 201703L
/*! `tmpl_subst(s, substitutes, keep_unknown)` creates a copy of `s` where all
**  placeholders in the format `%`*name* or `%{`*name*`}` which have a
**  representation (`name`) in `substitutes` with the corresponding values.
**  Depending on the third argument (`keep_unknown`) all placeholders not
**  represented in `substitutes` are either left unchanged (`true`, the
**  default) or removed (`false`). Incomplete placeholders (`%{tag`, `%` alone)
**  are left unchanged. Any double occurrence of `%` is converted to a single
**  `%` in the result.
**
**  \remark
**  This variant uses the `std::string_view` (from C++17 and above) as first
**  parameter, which helps to avoid the time and memory consuming conversion
**  of C strings into `std::string`.
*/
std::string tmpl_subst (const std::string_view &s, Substitutes &svals,
			bool keep_unknown = true);
#else
/*! `tmpl_subst(s, substitutes, keep_unknown)` creates a copy of `s` where all
**  placeholders in the format `%`*name* or `%{`*name*`}` which have a
**  representation (`name`) in `substitutes` with the corresponding values.
**  Depending on the third argument (`keep_unknown`) all placeholders not
**  represented in `substitutes` are either left unchanged (`true`, the
**  default) or removed (`false`). Incomplete placeholders (`%{tag`, `%` alone)
**  are left unchanged. Any double occurrence of `%` is converted to a single
**  `%` in the result.
**
**  \remark
**  This variant uses the `std::string` (used for C++ versions below C++17) as
**  first parameter and can be when the version C++ compiler used is older than
**  C++17.
*/
std::string tmpl_subst (const std::string &s, Substitutes &svals,
			bool keep_unknown = true);
#endif

/*! `u2hex(v, field_width, upper_case)` converts an unsigned integer value `v`
**  (`unsigned long`) into a hexadecimal string representation (`std::string`).
**  If the `field_width` is greater than the width of the converted `v`, the
**  result is left-padded with `0` characters. If `upper_case` is given as
**  `true` (default), the hexadecimal digits for the digit values 10 to 15 are
**  represented by the upper case letters `A` to `F` in the result; otherwise,
**  the lower case letters `a` to `f` are used.
*/
std::string u2hex (unsigned long v, unsigned int fw = 1, bool upper = true);

/*! `i2hex(v, field_width, upper_case)` converts a signed integer value `v`
**  (`long`) into a hexadecimal string representation (`std::string`).
**  If the `field_width` is greater than the width of the converted `v`, the
**  result is left-padded with `0` characters. If `upper_case` is given as
**  `true` (default), the hexadecimal digits for the digit values 10 to 15 are
**  represented by the upper case letters `A` to `F` in the result; otherwise,
**  the lower case letters `a` to `f` are used.
*/
std::string i2hex (long v, unsigned int fw = 1, bool upper = true);

/*! `trim(s, trim_chars)` removes all leading characters until the first one
**  not found in `trim_chars` and all trailing characters (after the last one
**  not foundin `trim_chars`) from the `std::string` variable `s`.
*/
void trim (std::string &s, const std::string &chars);

/*! `trim(s, trim_chars)` creates a copy of `s` (`std::string`) with all
**  leading characters until the first character not found in `trim_chars` and
**  all trailing characters after the last character not found in `trim_chars`
**  removed.
*/
std::string trim (const std::string &in, const std::string &chars = " ");

/*! `isws(string, index)` checks for the character at position `index`
**  in `string` being either a blank (' ') or a HT ('\\t') character.
**  (`isws` ==> `is_white_space`).
*/
bool isws (const std::string &s, size_t ix);

/*! `nows(string, index)` checks for the character at position `index`
**  in `string` beine any other than blank (' ') or HT ('\\t').
**  (`nows` ==> 'no_white_space')
*/
bool nows (const std::string &s, size_t ix);

/*! `eos(string, index)` checks for the `index` pointing behind the end
**  of the `string`.
**  (`eos` ==> `end_of_string`)
*/
bool eos (const std::string &s, size_t ix);

#endif /*SUTIL_H*/
