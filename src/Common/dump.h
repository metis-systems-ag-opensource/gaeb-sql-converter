/*! \file dump.h
**
** \brief Definition of a function which generates a hexdump as a (probably
** very long) string.
**
** This module is used solely for debugging purposes. It remains here, because
** the neither `xpp` nor `stp` are complete yet.
*/
/* $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Interface file of 'dump.cc'
** (Small function for dumping something at a given address)
**
*/
#ifndef DUMP_H
#define DUMP_H

#include <cstddef>
#include <string>

/*! `dump(p, s, w)` constructs a hexadecimal and ASCII dump of the memory
**  beginning at the address `p`, and of the size `s`. Each "line" in the
**  constructed dump contains the representation of at most `w` bytes.
**
**  @returns the constructed hexadecimal/ASCII dump as a `std::string` value.
**  returns this value. The optional argument `w` specifies the number of
**  bytes being hex-dumped per line.
*/
std::string dump (void *ptr, size_t n, int w = 8);

#endif /*DUMP_H*/
