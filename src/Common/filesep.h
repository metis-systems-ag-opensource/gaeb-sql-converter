/* filesep.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
**
** Need the pathname element separator ('\' in Windows, '/' elsewhere)
** somewhere.
**
*/
#ifndef FILESEP_H
#define FILESEP_H

#if defined(__WIN32__) || defined(__WIN64__)
#  define PATHSEP '\\'
#else
#  define PATHSEP '/'
#endif

#endif /*FILESEP_H*/
