/* dbif.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Primitive database interface (based on the MySQL/MariaDB C interface).
**
*/

#include <mysql.h>
#include <stdexcept>

#include "dbif.h"

using std::runtime_error;
using std::optional;
using std::string, std::to_string;

#define NoString optional<string>()

struct DBH_Int {
    MYSQL *dbh; MYSQL *dbh1;
    string host, username, password;
    optional<string> database;
    int port;
};

struct DBQ_Int {
    MYSQL *dbh;		// MySQL/MariaDB database handle
    MYSQL_RES *qres;		// Result handle of the query
    MYSQL_ROW qrrow;		// Current result row (or nullptr)
    unsigned int cc;		// Column count of the result row(s)
    bool eoq;			// End Of Query result
};

//class DBQ {
//public:
DBQ::DBQ (DBH_Int *di)
  : qi(new DBQ_Int ({(di ? di->dbh1 : nullptr), nullptr, nullptr, 0, false}))
{
    MYSQL *mysql = (di ? di->dbh1 : nullptr);
    if (mysql) {
	if (! (qi->qres = mysql_use_result (mysql))) {
	    throw runtime_error ("DBQ::DBQ(): Result access failed.");
	}
	qi->cc = mysql_field_count (mysql);
    }
}

DBQ::~DBQ() { end(); free (qi); qi = nullptr; }

// Retrieve the next row from the query result. Return 'true' if there was a
// next row and 'false' otherwise.
bool DBQ::next()
{
    if (qi->eoq) { return false; }
    qi->eoq = (qi->qrrow = mysql_fetch_row (qi->qres)) == nullptr;
    return ! qi->eoq;
}

// Access a single value of the current result row of the current query. If no
// row was retrieved yet, this function invokes 'DBQ::next()' for doing this.
// There is no need to declare the result as 'const' here, because it it always
// constructed as a _copy_ from (probably constant) values within the object.
// Additionally, it is not clear (yet) if DBQ values should be marked 'const'
// anywhere (even as function parameters), because even this 'operator[]()' is
// probably modifying a DBQ value.
// The result of 'operator[]()' _must_ be an 'optional<string>' value, because
// the column value selected from the database may contain no value (NULL).
optional<string> DBQ::operator[] (unsigned ix)
{
    if (! qi->qrrow && ! next()) {
	throw runtime_error ("DBQ::[]: No more data.");
    }
    if (ix < 0 || ix >= qi->cc) {
	throw runtime_error ("DBQ::[]: Index out of range.");
    }
    const char *x = qi->qrrow[ix];
    return (x ? optional<string> (string (x)) : optional<string>());
}

// Any query result has a fixed number of columns, which is returned through
// 'DBQ::columns()'.
unsigned int DBQ::columns() { return qi->cc; }

// Copy the content of a locally created query result (using 'DBH::query()')
// to the current query handle. This allows for using a variable of the type
// DBQ to be used (e.g.) as argument to a function.
DBQ &DBQ::operator= (const DBQ &x)
{
    end();	// End this query (if open) before assigning a new one
    qi->dbh = x.qi->dbh;
    qi->qres = x.qi->qres;
    qi->qrrow = x.qi->qrrow;
    qi->cc = x.qi->cc;
    qi->eoq = x.qi->eoq;
    return *this;
}

// Move the content of a locally created query result (using 'DBH::query()')
// to the current query handle. This allows for using a variable of the type
// DBQ to be used more than once.
DBQ &DBQ::operator= (DBQ &&x)
{
    end();	// End this query (if open) before assigning a new one
    qi->dbh = x.qi->dbh; x.qi->dbh = nullptr;
    qi->qres = x.qi->qres; x.qi->qres = nullptr;
    qi->qrrow = x.qi->qrrow; x.qi->qrrow = nullptr;
    qi->cc = x.qi->cc; x.qi->cc = 0;
    qi->eoq = x.qi->eoq; x.qi->eoq = false;
    return *this;
}

// End an open query and clean up the query handle, making it available for
// new query.
void DBQ::end()
{
    if (qi) {
	if (qi->qres) { mysql_free_result (qi->qres); qi->qres = nullptr; }
	qi->dbh = nullptr; qi->qrrow = nullptr; qi->cc = 0; qi->eoq = false;
    }
}

//private:
//    MYSQL *dbh;		// MySQL/MariaDB database handle
//    MYSQL_RES *qres;		// Result handle of the query
//    MYSQL_ROW qrrow;		// Current result row (or nullptr)
//    unsigned int cc;		// Column count of the result row(s)
//    bool eoq;		// End Of Query result
//} /*DBQ*/;

//class DBH {
//public:

DBH::DBH()
  : di(new DBH_Int ({ mysql_init (nullptr), nullptr, string(), string(),
		      string(), NoString, 0 }))
{ }

DBH::DBH (const string &host, int port, const optional<string> &db,
	  const string &user, const string &password)
  : di(new DBH_Int ({ mysql_init (nullptr), nullptr, host, user, password,
		      optional<string>(db), port }))
{ }

DBH::DBH (const DBH &x)
  : di(new DBH_Int ({ mysql_init (x.di->dbh), x.di->dbh1, x.di->host,
		      x.di->username, x.di->password, x.di->database,
		      x.di->port }))
{ }

DBH::~DBH()
{
    mysql_close (di->dbh);
    free (di); di = nullptr;
}

void DBH::connect()
{
    auto &_dbh1 = di->dbh1;
    if (! _dbh1) {
	_dbh1 = mysql_real_connect (
	    di->dbh,
	    di->host.c_str(),
	    di->username.c_str(),
	    di->password.c_str(),
	    (di->database ? di->database->c_str() : nullptr),
	    di->port,
	    nullptr,
	    CLIENT_MULTI_RESULTS
	);
	if (! _dbh1) {
	    throw runtime_error
		(string ("DBH::connect(): ") + mysql_error (di->dbh));
	}
    }
}

bool DBH::selectdb (const optional<string> &db)
{
    auto &_dbh1 = di->dbh1;
    int rc = mysql_select_db (_dbh1, (db ? db->c_str() : nullptr));
    bool rf = rc == 0;
    if (rf) { di->database = db; }
    return rf;
}

const std::optional<std::string> &DBH::currentdb()
{
    return di->database;
}

int DBH::nrquery (const string &q)
{
    auto &_dbh1 = di->dbh1;
    if (! _dbh1) {
	throw runtime_error ("DB::nrquery(): Not connected");
    }
    int qrc = mysql_real_query (_dbh1, q.c_str(), q.size());
    return qrc;
}

DBQ DBH::query (const string &q)
{
    auto &_dbh1 = di->dbh1;
    if (! _dbh1) {
	throw runtime_error ("DB::query(): Not connected");
    }
    int qrc = mysql_real_query (_dbh1, q.c_str(), q.size());
    if (qrc) {
	throw runtime_error ("DB::query(): Couldn't get result - " + errmsg());
    }

    return DBQ (di);
}

uint64_t DBH::insert_id () { return mysql_insert_id (di->dbh1); }

bool DBH::autocommit (bool acon) { return mysql_autocommit(di->dbh1, acon); }

bool DBH::commit() { return mysql_commit (di->dbh1) != 0; }

bool DBH::rollback() { return mysql_rollback(di->dbh1) != 0; }

bool DBH::begin() { return nrquery ("START TRANSACTION") != 0; }

int DBH::errcode() { return mysql_errno (di->dbh1); }

string DBH::errmsg() { return mysql_error (di->dbh1); }

string DBH::quote (const string &s)
{
    auto &_dbh1 = di->dbh1;
    size_t ssize = s.size();
    char *qcs = new char[ssize * 2 + 1];
    size_t qcslen = mysql_real_escape_string (_dbh1, qcs, s.c_str(), ssize);
    string qs (qcs, qcslen);
    delete[] qcs;
    return qs;
}
//private:
//    MYSQL *dbh, *_dbh1;
//    string _host, _database, _username, _password;
//    unsigned int _port;
//};
