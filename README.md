# GAEB Reader

## Kurz gesagt:

EIne Installationsanleitung befindet sich unter `doc/INSTALL.md`

## Über *GAEB*
Das Wort *GAEB* ist eigentlich eine Abkürzung und steht für _**G**meinsamer
**A**usschuss **E**lektronik im **B**auwesen_. Dieser Ausschuss hat die
Aufgabe, die Rationalisierung im Bauwesen mittels elektronischer

Datenverarbeitung zu fördern. Dieser Ausschuss besteht bereits seit 1966. Er
ist seit dem 1. Januar 2005 als Hauptausschuss im Deutschen Vergabe- und
Vertragsausschuss für Bauleistungen (DVA) eingegliedert.

Aufgabe des *GAEB* ist die Erstellung und Überarbeitung
- des Standardleistungsbuches Bau (*STLB-Bau*) mit standardisierten Texten zur
  Beschreibung von Bauleistungen für Neubau, Instandhaltung und Sanierung,
- des Standardlkeistungsbuches mit standardisierten Texten zur Beschreibung von
  Bauleistungen für Zeitvertragsarbeiten (*STLB-BauZ*),
- von Regelwerken für den elektronischen **D**atenaustausch und den **A**ufbau
  des Leistungsverzeichnisses (*GAEB DA*). Es stellt mit dem „Aufbau
  Leistungsverzeichnis“ und dem „dv-technischen Schema XML“ die standardisierte
  Schnittstelle zum Austausch der fachlichen Informationen zwischen den am Bau
  Beteiligten zur Verfügung. Nach Aussage der Studie "Qualitätssicherung des
  *GAEB*-Datenaustausches für die Durchführung von Bauaufgaben öffentlicher
  Auftraggeber" wurden im Jahre 2006 ein Bauvolumen von etwa 80 Mrd. Euro
  mittels *GAEB*-Datenaustausch transferiert.
- von *V*erfahrens*b*eschreibungen für die elektronische Mengen- und
  Bauabrechnung (*GAEB-VB*). Darin werden Regelungen für die Abrechnung von
  Bauleistungen sowie geometrische Lösungen für typische Abrechnungsaufgaben
  erarbeitet, aktualisiert und harmonisiert. Ziel ist, mit den gleichen
  Ausgangsdaten an verschiedenen Stellen unabhängig voneinander die gleichen
  Ergebnisse zu erreichen.

Ziel des GAEB ist eine effiziente *Ausschreibung*, *Vergabe* und *Abrechnung*
(AVA) von Bauleistungen zu ermöglichen. Hierbei muss auch die Konformität mit
den technischen Regelwerken des *Deutschen Institutes für Normung e.V.* (DIN)
und der *Vergabe- und Vertragsordnung für Bauleistungen* (VOB) beachtet werden.

(Quelle:
[Wikipedia(de)](https://de.wikipedia.org/wiki/Gemeinsamer_Ausschuss_Elektronik_im_Bauwesen))


Der *GAEB* hat Formate für den Austausch von Daten in den verschiedenen Phasen
des *AVA*-Prozesses definiert, und zwar
- ursprünglich in einer Tabellenartigen Textform (*GAEB DA 90*),
- später in Form einer Baum-Struktur, wobei ein eigenes Schema entwickelt
  wurde (*GAEB DA 2000*), und
- zuletzt in Form von Daten in Form von XML-Dokumenten, für die entsprechende
  Beschreibungen als XML-Schemadateien hinterlegt sind (*GAEB DA XML*).

Da öffentliche Bauaufträge gemäß der Konventionen des *GAEB* vergeben werden,
ist eine elektronische Verarbeitung dieser XML-Dateien für Firmen, die sich
dem Bau, der Sanierung und Instandhaltung von Gebäuden im öffentlichen Auftrag
befassen, sehr wichtig.


## Zum *GAEB-Reader*-Projekt

Das *GAEB Reader*-Projekt befasst sich mit dem Analysieren und Erfassen von
GAEB-Dateien in passenden (relationalen) Datenbanken. Ähnlich wie schon zuvor
– beim *IFC-Server*-Projekt – unterteilt sich das Projekt wieder in zwei
Teilbereiche:

1. Dem einlesen von GAEB-Dateien, wozu, da es sich um XML-Dateien handelt,
   eine Prüfung bezogen auf die XML-Schemadateien des *GAEB*, und danach der
   Übertragung der gelesenen Daten in eine (MySQL/MariaDB)-Datenbank.
2. Dem Generieren der Struktur der jeweiligen relationalen Datenbanken, sowie
   von Meta-Informationen, die zur Übertragung der Daten eingelesener
   *GAEB*-Dokumente notwendig sind.

In diesem Projekt befinden sich im Ordner `src/GAEB-Reader` die C++-Quellen des
Programmes `gaeb-reader`, das für den ersten Teilbereich zuständig ist, und im
Ordner `src/GAEBsd` entsprechend die C++-Quellen des *GAEB* Schemaparsers
`gaebsd`, der für den zweiten Teilbereich zuständig ist.

Zusätzlich befinden sich im Ordner `src/GAEB-Reader` noch die Quellen zweier
Hilfprogramme:
- `gaeb-info`, das Versionsinformation zu einem *GAEB*-Dokument ausgibt, und
- `gaeb-conf`, mit dem Teile der Konfiguration des Programmes `gaeb-reader`
  ausgelesen werden können.

Als Grundlage (zum Parsieren der XML-Dateien, seien es GAEB-Dokumente oder
GAEB-Schemadateien, dient die `Xerces C`-Bibliothek der *Apache Software
Foundation*.

Um bei der Weiterverarbeitung der eingelesenen Daten nicht auf die seltsam
(und auch unpassend) anmutenden Datenstrukturen von `Xerces C` direkt zugreifen
zu müssen, wurde eine Trennung zwischen dem XML-Parser und den Programmteilen,
die sich mit der Weitervearbeitung dieser Daten befassen, eine Trennung
eingeführt. Unter anderem befinden sich im Hauptprogramm keine direkten
Verweise auf die `Xerces C`-Bibliothek. Die im Hauptprogramm notwendigen
Arbeitsschritte zur Initialisierung und Finalisierung werden durch Funktionen
im Modul `finit.{cc,h}` erledigt, die die internen Strukturen der `Xerces C`-
Bibliotheken verbergen. Auch die Weiterverarbeitung der Daten geschieht mit
den konventionellen Zeichenketten der C++-Standardbibliothek, nachdem die
`Xerces C`-Zeichenketten in diese umgewandelt wurden.


### Aufbau unf Funktionsweise des GAEB-Schemaparsers (`gaebsd`)

`gaebsd` ist ein *SAX2*-Parser ohne vollständige Validierung. Was an
Validierung notwendig ist wird in dem Teil vorgenommen, der die Empfangenen
Daten weiterverarbeit, bzw. auch in den nachfolgenden Phasen. Da *GAEB*-XSDs
(Schemadateien) mittels `<redefine>`-Element andere Schema-Dateien aufrufen
(bzw. einbinden), ist der Schema-Parser rekursiv ausgelegt.

Die Weiterverarbeitung der Daten erfolgt während des Parsierens
(`sp-action.{cc,h}`, `fixtfets.{cc,h}`), nach dem Parsieren (`resolve.{cc,h}`),
sowie als letzte Vorbereitung auf die Code-Generierung (`optimise.{cc,h}`,
`nodis.{cc,h}`), bevor das Datenbank-Schema ind die Metadaten generiert werden.

Die Generierung der SQL-Anweisung für das Datenbank-Schema und die vom
`gaeb-reader` später verwendeten Meta-Informationen werden beide im gleichen
Modul (`ogen.{cc,h}`) generiert, gesteuert lediglich durch eine eigene, vom
Interface `OStrings` (in `ostrings.h` definiert) abgeleitete Klasse zur
Generierung der jeweils benötigten Zeichenketten für die verschiedenen
Bestandteile eines solchen INSERT-Statements. Diese Klassen sind in `dbgen.cc`
sowie in `mdgen.cc` definiert, und werden nicht exportiert, da für die
Generierung benötigten Objekte dieser Klassen direkt dort angelegt, und dann
nur noch an die Routine `gen_output()` (definiert in `ogen.{cc,h}`), zusammen
mit den während der Parsierung der Schemadatei(en) gesammelten Daten übergeben
werden. Exportiert werden von `dbgen.{cc,h}` und `mdgen.{cc,h}` lediglich
jeweils eine einzighe Routine, die den gesamten Vorgang versteckt.


### Aufbau und Funktionsweise des GADB-Readers

Der GAEB-Reader benötigt für die Validierung Informationen haben, die in der
GAEB-Datei selbst stehen. Das bedeutet, dass die Parsierung einer GAEB-Datei
immer in zwei Schritten erfolgen muss:
1. Eine einfache Parsierung ohne Validierung, um der Datei die für die
   Validierung benötigten Daten (unter Anderem die Versionsnummer des
   GAEB-Schemas, mit dem diese Datei analysiert werdeb,
2. Eine Parsierung mit Validierung gemäß der durch Versionsnummer und -datum
   bestimmten GAEB-Schemadatei(en), wobei dann eine DOM-Struktur im Speicher
   entsteht. Diese so generierte DOM-Struktur wird dann an das Modul
   'process.{cc,h}' zur Weiterverarbeitung übergeben. Dieses Modul generiert
   dann, mit Hilfe der von `gaebsd` generierten Daten INSERT-Anweisungen, die
   entweder sofort ausgeführt werden, oder in einer Datei oder die
   Standardausgabe geschrieben werden.


### Weitere Programme

#### `gaeb-info`

`gaeb-info` liefert Daten wie die (GAEB-)Version einer GAEB-Datei in der
Standardausgabe ab. Sie verwendet den Teil des `gaeb-readers`, der diese
Informationen ermittelt.

#### `gaeb-conf`

`gaeb-conf` gibt eizelne Elemente der Konfiguration des
`gaeb-reader`-Programmes in der Standardausgabe ab. Es dient im Wesentlichen
dem Zweck, diese Konfigurationsdaten in Scrip-Programmen verwenden zu können.

#### `gen-sql.sh`

Dieses Hilfsprogramm dient dem Zweck, alle Datenbank-Schemata und alle
INSERT-Metadaten für alle GAEB-Phasen un allen verfügbaren GAEB-Versionen
in einem Durchgang zu generieren.


## Abschließende Bemerkungen / Einschränkungen

Leider ist auch der GAEB-Schemaparser nicht ganz vollständig. Zur Zeit können
DB-Schemata für die Phase `84Z` (das sind wohl Zeitarbeitsverträge) nicht
generiert werden.

Eigentlich ist eine relationale Datenbank nicht ganz so gut geeignet, um als
Ziel eines GAEB-Dokumentes zu dienen, das wurde während der Implementierung
auch klar. Um die Ergebnisse vin `gaebsr` und `gaeb-reader` verwenden zu
können, müssen geeignete Datenbank-Abfragen (Joins oder auch "stored
procedures") erst noch entwickelt werden,
