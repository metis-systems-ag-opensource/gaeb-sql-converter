#! /bin/bash
# install.sh
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@metis-ag.com
# Copyright: (c) 2021, Metis AG
# License: MIT License
#
# Small installation helper script
#
# Synopsis:
#
#   install.sh <prefix> [gaeb]
#   install.sh local
#   install.sh	# w.o. arguments for help

PROG="$(basename "$0")"
PPATH="$(realpath -s "$(dirname "$0")")"
BASE="${PROG%.sh}"
BPATH="${PPATH%/butils}"

eecho() {
    echo 1>&2 ${1+"$@"}
}

abort() {
    local ec="$1"
    [[ "$ec" =~ ^[1-9][0-9]*$ ]] && shift || ec=1
    eecho "${PROG}:" ${1+"$@"}
    exit $ec
}

get_cmds() {
    local cmd cmdpath failed scmd sl ok
    if [ $# -lt 1 ]; then
	abort 2 "get_cmds() invoked without arguments"
    fi
    failed=()
    for cmd in "$@"; do
	if [ "${cmd%|*}" != "$cmd" ]; then
	    IFS='|' sl=($cmd); ok=false
	    for scmd in "${sl[@]}"; do
		if cmdpath="$(type -p "$scmd")"; then
		    [ -n "$cmdpath" ] || cmdpath="$scmd"  ## shell-internals
		    eval "${scmd}_cmd=\"$cmdpath\""; ok=true
		fi
	    done
	    $ok || failed+=("$cmd")
	elif cmdpath="$(type -p "$cmd")"; then
	    [ -n "$cmdpath" ] || cmdpath="$cmd"		  ## shell-internals
	    eval "${cmd}_cmd=\"$cmdpath\""
	else
	    failed+=("$cmd")
	fi
    done
    if [ ${#failed[@]} -gt 0 ]; then
	abort 3 "Some required external programs were not found:" \
		"${failed[@]}"
    fi
}

usage() {
    if [ $# -gt 0 ]; then
	echo 1>&2 "${PROG}:" "$@"
	exit 64
    fi
    cat <<-EOT
Usage: $PROG <install-prefix> [gaeb]
       $PROG local [gaeb]
       $PROG  # w.o. arguments for this usage message

Arguments:
  <install-prefix>
    Installing the programs and data globally. <install-prefix> names the
    base directory for the complete installation (e.g. '/usr', '/usr/local',
    '/opt', and so on).
  gaeb
    When given, this script tries to download and install the GAEB XML Schema
    files instead of installing the program(s).
  local
    Install the programs locally, meaning: in the HOME directory of the user
    who invoked this script.

Invoking this script without arguments leads to this usage message being
printed.
EOT
    exit 0
}

## Creating a wrapper script at the specified position
gen_script() {
    local out="$1"
    echo -n "Generating the wrapper script ..."
    cat >"$out" <<-'EOT'
	#! /bin/sh
	#
	# Small wrapper script for the programs in the directory which also
	# contains this wrapper script. This script is used instead of direct
	# symbolic links, because some of these programs may depend on the
	# path of the directory they reside in. This script guarantees that
	# these programs are executed with the correct pathnames.
	#
	# ATTENTION! This is an automatically created script file.
	
	PROG="$(basename "$0")"
	PPATH="$(dirname "$0")"
	
	WRPROG="$(readlink "$0")"
	WRNAME="$(basename "$WRPROG")"
	WRPATH="$(dirname "$WRPROG")"
	
	if [ "$PROG" = "$WRNAME" ]; then
	    echo 1>&2 "${PROG}: Invalid reference to this wrapper script."
	    exit 64
	fi
	
	RPATH="$WRPATH/$PROG"
	if [ ! -f "$RPATH" -o ! -x "$RPATH" ]; then
	    echo 1>&2 "${PROG}: Sorry, but '$PROG' doesn't reside here or" \
		      "isn't executable."
	    exit 64
	fi
	
	exec "$RPATH" ${1+"$@"}
	EOT
    chmod a+x "$out"
    echo " done."
}

## Create a symbolic link if it doesn't already exist.
## If the link target already exists and
##  a) is no symbolic link, or
##  b) is a symbolic link which doesn't point to the file to be symlink'd
## issue an error.
##
check_mklink() {
    local src="$1" dst="$2" rp
    if [ -e "$dst" ]; then
	if [ ! -h "$dst" ]; then
	    eecho "${PROG}: >$dst< already exists and is no symbolic link."
	    return 1
	fi
	rp="$(readlink "$dst")"
	if [ "$rp" != "$src" ]; then
	    eecho "${PROG}: >$dst< already exists and points to somewhere else."
	    return 1
	fi
	return 0
    else
	ln -sf "$src" "$dst"
    fi
}
    

## Download and install GAEB XML Schema files from ZIP-archives which are
## probably stored at the 'https://www.gaeb.de' website.
##
install_gaeb_zips() {
    local cf="$1" dst="$2" td="$3"
    local dlcmd IFS_save dfdir dlcnt failed_urls
    local -A failed_dirs

    if [ -n "$wget_cmd" ]; then
	dlcmd=("$wget_cmd" -O "$td/gaeb-xsds.zip" -q)
    else
	dlcmd=("$curl_cmd" -o "$td/gaeb-xsds.zip" -s)
    fi

    IFS_save="$IFS"; IFS='|'; dlcnt=0
    failed_dirs=(); failed_urls=()
    echo -n "Installing data "
    while read dir url; do
	dfdir="$dst/$dir"
	test -z "${failed_dirs[$dir]}" || continue
	if ! mkdir 2>/dev/null -p "$dfdir"; then
	    echo -n "-"
	    failed_dirs[$dir]="$dir"
	    continue
	fi
	if ! "${dlcmd[@]}" "$url"; then
	    echo -n "!"
	    failed_urls+=("$url")
	else
	    if ! $unzip_cmd -joq -d "$dfdir" "$td/gaeb-xsds.zip"; then
		#rm -rf "$dfdir"
		echo -n "?"
		failed_urls+=("$url")
	    else
		echo -n "."
		dlcnt=$((dlcnt + 1))
	    fi
	fi
	rm -f "$td/gaeb-xsds.zip"
    done < "$cf"; echo " done."
    IFS="$IFS_save"

    # Error output (directory creation failures)
    if [ ${#failed_dirs[@]} -gt 0 ]; then
	eecho "The following directories couldn't be created:"
	for x in "${failed_dirs[@]}"; do eecho " $x"; done
    fi

    # Error output (download failures)
    if [ ${#failed_urls[@]} -gt 0 ]; then
	eecho "Failed to download data from the following URLs:"
	for x in "${failed_urls[@]}"; do eecho " $x"; done
    fi

    test $dlcnt -eq 0 && return 1 || return 0
}

## Download and install GAEB XML Schema files directly from the
## 'https://www.gaeb.de' website.
##
install_gaeb_xsds() {
    local cf="$1" dst="$2" td="$3"
    local dlcmd dlprg IFS_save dfdir dlcnt failed_urls x
    local -A failed_dirs

    if [ -n "$wget_cmd" ]; then
	dlcmd=("$wget_cmd" -q)
    else
	dlcmd=("$curl_cmd" -s --remote-name)
    fi

    IFS_save="$IFS"; IFS='|'; dlcnt=0
    failed_dirs=(); failed_urls=()
    echo -n "Installing data "
    while read dir url; do
	dfdir="$dst/$dir"
	test -z "${failed_dirs[$dfdir]}" || continue
	if ! mkdir 2>/dev/null -p "$dfdir"; then
	    echo -n "-"
	    failed_dirs[$dfdir]="$dfdir"; continue
	fi
	if (cd "$dfdir"; "${dlcmd[@]}" "$url"); then
	    echo -n "."; dlcnt=$((dlcnt + 1))
	else
	    echo -n "!"
	    failed_urls+=("$url")
	fi
    done < "$cf"; echo " done."
    IFS="$IFS_save"

    # Error output (directory creation failures)
    if [ ${#failed_dirs[@]} -gt 0 ]; then
	eecho "The following directories couldn't be created:"
	for x in "${failed_dirs[@]}"; do eecho " $x"; done
    fi

    # Error output (download failures)
    if [ ${#failed_urls[@]} -gt 0 ]; then
	eecho "Failed to download data from the following URLs:"
	for x in "${failed_urls[@]}"; do eecho " $x"; done
    fi

    # This function succeeds if any file could be successfully downloaded
    # and installed.
    test $dlcnt -eq 0 && return 1 || return 0
}

## Check for the existance of the following commands and store the full
## pathname of each of them in a variable with the name '<command name>_cmd'.
## Required commands are: 'unzip' and either 'wget' or 'curl'.
get_cmds unzip 'wget|curl'

test $# -gt 0 || usage

## Prepare the pathname variables for the installation
##
pfx="$1"; shift
if [ "$pfx" = local ]; then
    instdir="$HOME/lib/gaeb-sql-converter"; bindir="$HOME/bin"
    #cfgdir="$HOME/.config/gaeb"; cfgsfx=".conf"
else
    if [ "${pfx#/}" = "$pfx" ]; then
	usage "<install-prefix> must be an absolute pathname."
    fi
    if [ -e "$pfx" ]; then
	if [ ! -d "$pfx" ]; then
	    usage "<install-prfix> must be a directory."
	fi
    elif ! mkdir -p "$pfx"; then
	abort "Attempt to create >$pfx< failed."
    fi
    instdir="$pfx/lib/gaeb-sql-converter"; bindir="$pfx/bin"
    #cfgdir="/etc/gaeb"; cfgsfx=
fi

mkdir -p "$instdir" || abort "Attempt to create >$instdir< failed."

echo "Installing in >$instdir<"

if [ $# -gt 0 ]; then
    test "$1" = gaeb || usage ">$1< - invalid argument."
    mkdir "$instdir/gaeb-xsd"
    instcnt=0
    TMPDIR="${TEMP:-${TMP:-/tmp}}/$BASE-$$"
    mkdir "$TMPDIR" || abort 1 "Attempt to create download directory failed."
    if [ -f "$BPATH/conf/xsd-zipfiles" ]; then
	echo "Installing GAEB XML Schema files from ZIP archives."
	install_gaeb_zips "$BPATH/conf/xsd-zipfiles" "$instdir/gaeb-xsd" \
			  "$TMPDIR" \
	    && instcnt=$((instcnt + 1))
    fi
    if [ -f "$BPATH/conf/xsd-schemafiles" ]; then
	echo
	echo "Installing GAEB XML Schema files directly."
	install_gaeb_xsds "$BPATH/conf/xsd-schemafiles" "$instdir/gaeb-xsd" \
			  "$TMPDIR" \
	    && instcnt=$((instcnt + 1))
    fi
    if [ $instcnt -eq 0 ]; then
	abort 1 "No files were downloaded."
    fi
else

    # Create the required directories if they do not already exist.
    mkdir -p "$bindir" || abort "Attempt to create >$bindir< failed."

    # Generate the local 'bin' directory within the '$instdir' tree.
    mkdir -p "$instdir/bin"

    # Generate the wrapper script
    gen_script "$instdir/bin/run.sh"

    # Install the programs in '$instdir/bin' and create symbolic links to
    # the wrapper script with the names of the programs in '$bindir'
    ec=0; ic=0; echo
    for pp in "$BPATH/bin"/*; do
	test -x "$pp" || continue
	p="${pp##*/}"
	echo "Installing '$p'"
	install -s -m 0511 "$pp" "$instdir/bin/$p"
	bp="$bindir/$p"
	echo "Creating symlink to wrapper script"
	if ! check_mklink "$instdir/bin/run.sh" "$bp"; then
	    ec=$((ec + 1)); continue
	fi
	ic=$((ic + 1))
    done

    if [ $ic -lt 1 ]; then
	eecho "${PROG}: No programs were installed. Did you create them?"
    fi

    # Installing the 'gen-sql.sh' script from the 'butils' directory. It
    # depends on 'gaebsd' being in the same directory as itself, so invoking
    # it through the wrapper script is _mandatory_ here.

    echo
    echo "Installing 'gen-gaeb-sql-sh'"
    install "$BPATH/butils/gen-sql.sh" "$instdir/bin/gen-gaeb-sql.sh"
    echo "Creating symlink to wrapper script"
    if ! check_mklink "$instdir/bin/run.sh" "$bindir/gen-gaeb-sql.sh"; then
	ec=$((ec + 1))
    fi

    # Installing the default configuration files. Due to the configuration
    echo "Installing default configuration files"
    mkdir -p "$instdir/conf"
    for cfp in "$BPATH/conf"/*.conf; do
	cfn="${cfp##*/}"
	install -m 0644 "$cfp" "$instdir/conf/$cfn"
    done

    # Installing the default authentication data. These are always wrong for
    # a fresh installation, but are nonetheless required for 'gaeb-conf' to
    # work.
    mkdir -p "$instdir/auth"
    install -m 0644 "$BPATH/auth/credentials" "$instdir/auth/credentials"

    # The default directory holding the SQL schema and INSERT-meta files.
    # The creation of this directory is required for 'gaeb-conf' to work.
    mkdir -p "$instdir/im"

    if [ $ec -gt 0 ]; then
	abort 1 "Terminating with failurte status due to previous errors."
    fi
fi

# Force a 'success' exit code
exit 0
