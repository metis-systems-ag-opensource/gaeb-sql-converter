#! /bin/bash
# bpath.sh
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@metis-ag.com
# Copyright: (c) 2021, Metis AG
# License: MIT License
#
# Writing the pathname of a file (relative to a given direcory) to the
# standard output stream

PROG="${0##*/}"

eecho() {
    echo 1>&2 ${1+"$@"}
}

usage() {
    if [ $# -gt 0 ]; then eecho "${PROG}:" "$@"; exit 64; fi
    echo "Usage: $PROG directory [relative-to]"
    exit 0
}

if [ $# -lt 1 ]; then usage; fi
DIR="$1"; shift
REL=; if [ $# -gt 0 ]; then REL="$1"; shift; fi

## If we have 'realpath' (should be in the 'coreutils') then all is fine,
## because all we have to do is executing 'realpath' with some arguments.
##
if realpath=$(type -p realpath); then
    if [ -n "$REL" ]; then
	exec $realpath -s --relative-to="$REL" "$DIR"
    else
	exec $realpath -s "$DIR"
    fi
fi

## On the other hand, if we don't have 'realpath', the situation is somewhat
## more complex. The following function definitions use some "name" parameters
## (indirections), which allows for the modification of a variable from outside
## of the corresponding functions by argument. This frees us from the necessity
## to use fixed global variable names inside of these functions.
## ("name" parameters are declared with 'local -n var=<value>')


## This function splits a given pathname into its components and stores these
## components as an array into the variable whose name is given as the second
## argument.
##
split_path() {
    if [ $# -lt 2 ]; then
	eecho "split_path(): Missing arguments!"; return 64
    fi
    local -n res="$2"
    local fields x IFS_save
    IFS_save="$IFS"; IFS=/
    fields=()
    for x in $1; do
	test -n "$x" || continue
	fields+=("$x")
    done
    IFS="$IFS_save"
    if [ "${1#/}" != "$1" ]; then fields=('' "${fields[@]}"); fi
    res=("${fields[@]}")
    #eval "$res="'("${fields[@]}")'
}

## Perform a 'shift' operation, but not on the parameter array '$@', but
## on the array variable whose name is given as the (only) argument to this
## function.
##
vshift() {
    if [ $# -lt 1 ]; then
	eecho "vshift(): Missing arguments!"; return 64
    fi
    local -n v="$1"
    shift
    if [ "${v@a}" != a ]; then
	eecho "vshift(): '$1' is no array!"; return 64
    fi
    set -- "${v[@]}"
    shift
    v=("$@")
#    unset v[0]
}

## Performs a "pop" operation on the array variable whose name is the (only)
## argument of this function, meaning: Removes the last element of this
## array variable.
##
vpop() {
    if [ $# -lt 1 ]; then
	eecho "vpop(): Missing arguments!"; return 64
    fi
    local -n v="$1"
    local x
    if [ "${v@a}" != a ]; then
	eecho "vpop(): '$1' is no array!"; return 64
    fi
    x=$((${#v[@]} - 1))
    unset v[$x]
}

## Joins a list of words (third argument onward) with a delimiter given as
## second argument into a variable whose name is the first argument.
vjoin() {
    if [ $# -lt 3 ]; then
	eecho "vjoin(): Missing arguments!"; return 64
    fi
    local -n v="$1"
    local del="$2" res="$3"
    shift 3
    while [ $# -gt 0 ]; do
	res+="$del$1"; shift
    done
    v="$res"
}

## Normalises the pathname in the variable whose name is the (only) argument
## of this function.
##
normalise_path() {
    local -n path="$1"
    local dv nd
    if [ "${path#/}" = "$path" ]; then
	path="$(pwd)/$path"
	split_path "$path" dv
	nd=("${dv[0]}"); vshift dv
	while [ ${#dv[@]} -gt 0 ]; do
	    case "${dv[0]}" in
		.)  ;;
		..) test ${#nd[@]} -lt 2 || vpop nd ;;
		*)  nd+=("${dv[0]}") ;;
	    esac
	    vshift dv
	done
	vjoin "${!path}" / "${nd[@]}"
    fi
}

# Normalise 'DIR' and make it an absolute path
normalise_path DIR

if [ -n "$REL" ]; then
    # Normalise 'REL' and make it an absolute pathname
    normalise_path REL

    # Truncate the content of 'REL' from the beginning of the content of
    # 'DIR' and print the result.
    echo "${DIR#$REL/}"
else
    # Print the content of the normalised 'DIR'.
    echo "$DIR"
fi
