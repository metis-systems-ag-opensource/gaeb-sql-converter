#! /bin/bash
# gen-sql.sh
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@metis-ag.com
# Copyright: (c) 2021, Metis AG
# License: MIT License
#
# Generate all SQL schemas and INSERTION meta files in one go

PROG="$(basename "$0")"
BASE="${PROG%.sh}"
PPATH="$(realpath "$(dirname "$0")")"
BASEP="$(dirname "$PPATH")"
BINPATH="$BASEP/bin"
TMPDIR="${TEMP:-${TMP:-/tmp}}"
LOGPATH="$TMPDIR/${BASE}-$$.logs"
GETCONF="${BINPATH}/gaeb-conf"

eecho() {
    echo 1>&2 ${1+"$@"}
}

usage() {
    if [ $# -gt 0 ]; then eecho "${PROG}:" "$@"; exit 64; fi
    echo "Usage: $PROG [-f] XSD-dir output-dir"
    echo "       $PROG -h"
    exit 0
}

usage1() {
    echo "Usage: $PROG [-f] output-dir"
    echo "       $PROG -h"
    exit 0
}

usage2() {
    echo "Usage: $PROG [-f] XSD-dir"
    echo "       $PROG -h"
    exit 0
}

usage3() {
    echo "Usage: $PROG [-f]"
    echo "       $PROG -h"
    exit 0
}

tmkdir() {
    local opt path
    if [ "x$1" = x-p ]; then opt="$1"; shift; fi
    if [ $# -lt 1 ]; then eecho "tmkdir(): Missing arguments."; return 64; fi
    path="$1"; shift
    if ! [ -d "$path" ]; then
	if ! mkdir &> /dev/null $opt "$path"; then
	    eecho "tmkdir(): Failed to create directory ${path@Q}."
	    return 73
	fi
    elif ! [ -d "$path" ]; then
	eecho "tmkdir(): ${path@Q} is no directory."
	return 70
    fi
}
    

check_cmds() {
    local cmd missing path
    missing=()
    for cmd in "$@"; do
	if path=$(type -p "$cmd"); then
	    eval "$(basename "$cmd" | tr ' ' '_')_cmd=\"${path:-$cmd}\""
	else
	    missing+=("$cmd")
	fi
    done
    if [ ${#missing} -gt 0 ]; then
	eecho "${PROG}: Missing required commands (${missing[@]})"
	exit 69
    fi
}

shopt -s nullglob

check_cmds test "$BINPATH/gaebsd"

GAEB_PHASES=(31 50 50.1 50.2 51 51.1 51.2 52 61 80 81 82 83 83Z 84 84P 84Z 85 \
	     86 86ZE 86ZR 87 89 89B 93 94 96 97 98 99)

force=false
if xsddir="$("$GETCONF" sd xsdbase)"; then
    if outdir="$("$GETCONF" sd insmeta)"; then
	if [ "x$1" = x-h ]; then usage3; fi
	if [ "x$1" = x-f ]; then force=true; shift; fi
    else
	if [ "x$1" = x-h ]; then usage1; fi
	if [ "x$1" = x-f ]; then force=true; shift; fi
	test $# -gt 0 || usage "Missing 'output-dir' argument."
	outdir="$1"; shift
    fi
elif outdir="$("$GETCONF" sd insmeta)"; then
    if [ "x$1" = x-h ]; then usage1; fi
    if [ "x$1" = x-f ]; then force=true; shift; fi
    test $# -gt 0 || usage "Missing 'XSD-dir' argument."
    xsddir="$1"; shift
else
    test $# -gt 0 || usage
    if [ "x$1" = x-f ]; then force=true; shift; fi
    test $# -gt 1 || usage "Missing argument(s)."

    xsddir="$1"
    outdir="$2"
    shift 2
fi

test -d "$xsddir" || { eecho "${PROG}: ${xsddir@Q} is no directory"; exit 1; }

tmkdir "$LOGPATH" || exit $?

gvd=();
for sd in "$xsddir"/*; do
    test -d "$sd" || continue
    dn="${sd##*/}"
    if [[ "$dn" =~ [3-9]\.[0-9]+_[2-9][0-9]{3}-(0[1-9]|1[012]) ]]; then
	gvd+=("$sd")
    fi
done

if [ ${#gvd[@]} -lt 1 ]; then
    eecho "${PROG}: No directories matching GAEB versions found in ${xsddir@Q}"
    exit 1
fi

tmkdir -p "$outdir" || exit $?

rvd=(); ovd=()
for d in "${gvd[@]}"; do
    gv="${d#$xsddir/}"
    od="$outdir/$gv"
    tmkdir "$od" || continue
    rvd+=("$d"); ovd+=("$od")
done

nv=$((${#rvd[@]} - 1))
eval "xl=({0..$nv})"
ec=0
FAILED=()
for x in "${xl[@]}"; do
    xssd="${rvd[$x]}"; ovsd="${ovd[$x]}"
    xsv="${xssd#$xsddir/}"
    echo -n "Processing GAEB version ${xsv@Q}:"
    XSDs=(); OUTs=()
    for phase in "${GAEB_PHASES[@]}"; do
	xsd=
	for x in "$xssd"/*/*_${phase}_*.xsd "$xssd"/*_${phase}_*.xsd; do
	    if [ -n "$x" ]; then xsd="$x"; break; fi
	done
	test -n "$xsd" || continue
	out="$ovsd/x${phase}"; log="$LOGPATH/$phase.log"
	echo -n " $phase"
	if $force; then rm -f "$out".{is,sql}; fi
	if $gaebsd_cmd $xsd $out &>>"$log"; then
	    XSDs+=("$xsd"); OUTS+=("$ovsd/x${phase}")
	else
	    FAILED+=("$(realpath -s --relative-base="$BASEP" "$xsd")")
	    echo -n '!'
	    rm -f "$out".{is,sql}
	    ec=$((ec + 1))
	fi
    done
    echo
    sleep 1
done

if [ $ec -gt 0 ]; then
    eecho "$PROG: Some (or all) if the SQL/INSERT-meta files failed:"
    for f in "${FAILED[@]}"; do
	eecho "   $f"
    done
    eecho "  You can find the output concerning the creation of all files in"
    eecho "  \"$LOGPATH\"."
else
    : rm -rf "$LOGPATH"
fi
