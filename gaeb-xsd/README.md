# GAEB XSD files

The `GAEB XSD` files are not part of this software. These files can be
downloaded from:

  [GAEB-Datenaustausch Downloads](https://www.gaeb.de/de/service/downloads/gaeb-datenaustausch/)

They are available in packages (as ZIP files), containing the `XML Schema`
files for the different theme groups (like e.g. *Leistungsverzeichnis*,
*Handel*, etc.).

Once downloaded, you should unpack them into folders like
*gaeb-version*\_*gaeb-version-date*/*group* below a folder with a name like
this one (the folder which contains this focument), e.g.
```
$ mkdir 3.3_2021-05
$ unzip -d 3.3_2021-05/Leistungsverzeichnis 2021-05_Leistungsverzeichnis.zip
```
(provided that the ZIP-archive `2021-05_Leistungsverzeichnis.zip` is part
of the version 'GAEB DA XML 3.3 | 2021-05'.

Currently available (DATE: 2021-11-24) are:
  - [GAEB DA XML 3.3 | 2021-05](https://www.gaeb.de/de/service/downloads/gaeb-datenaustausch/#1605874164512-4ab864a4-5dfa)
  - [GAEB DA XML 3.3 | 2020-06 (BETA!)](https://www.gaeb.de/de/service/downloads/gaeb-datenaustausch/#1624449768284-03189d95-f2a2)
  - [GAEB DA XML 3.3 | 2019-11 (BETA!)](https://www.gaeb.de/de/service/downloads/gaeb-datenaustausch/#1571392333319-67f00373-f200)
  - [GAEB DA XML 3.3 | 2010-05](https://www.gaeb.de/de/service/downloads/gaeb-datenaustausch/#1580479738785-b7d33de1-6ffc)
  - [GAEB DA XML 3.2 | 2013-10](https://www.gaeb.de/de/service/downloads/gaeb-datenaustausch/#dlj2ZSUAZBdUHRl)
  - [GAEB DA XML 3.2 | 2013-09 (BETA!)](https://www.gaeb.de/de/service/downloads/gaeb-datenaustausch/#1558535343180-03f65f68-e182)
  - [GAEB DA XML 3.2 | 2012-06 (BETA!)](https://www.gaeb.de/de/service/downloads/gaeb-datenaustausch/#1558535364463-ba200704-5a1b)

The small installation helper `butils/install.sh` tries to download and install
the corresponding GAEB files, but it is not guaranteed that the download
locations remain the forever same.
