# Makefile
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@metis-ag.com
# Copyright: (c) 2020, Metis AG
# 
#
# Makefile for 'gaeb-reader'

MAKEFLAGS := --no-print-directory

PREFIX = /usr/local

BASH = $(shell which bash)

%:
	@$(MAKE) -C src $@

all:
	@$(MAKE) -C src $@

install: install_prereq pinstall
	@echo "Downloading and installing the GAEB XML Schema files"
	@$(BASH) ./butils/install.sh "$(DESTDIR)$(PREFIX)" gaeb

pinstall:
	@echo "Installing Programs"
	@$(BASH) ./butils/install.sh "$(DESTDIR)$(PREFIX)"

install_local local_install: install_prereq pinstall_local
	@echo "Downloading and installing the GAEB XML Schema files"
	@$(BASH) ./butils/install.sh local gaeb

pinstall_local:
	@echo "Installing Programs"
	@$(BASH) ./butils/install.sh local

install_prereq: bin/gaebsd bin/gaeb-reader bin/gaeb-info bin/gaeb-conf

#clean cleanall:
#	$(MAKE) -C src $@

.PHONY: install_prereq
